#pragma once
#include <cstdint>


using u16 = uint16_t;
using u8 = uint8_t;
using u32 = uint32_t;

namespace Utils
{
	template<class T>
	constexpr bool isBitOn(const size_t value, const T bit) noexcept { return value & (1 << static_cast<u8>(bit)); };
	template<class T> 
	constexpr size_t setBit(const size_t value, const T bit) noexcept { return value | (1 << static_cast<u8>(bit)); };
	template<class T>
	constexpr size_t resetBit(const size_t value, const T bit) noexcept { return value & ~(1 << static_cast<u8>(bit)); };
	template<class T>
	constexpr size_t setBit(const size_t value, const T bit, const bool isOn) noexcept { return isOn ? setBit(value, bit) : resetBit(value, bit); };
	
	constexpr u16 join(const u8 high, const u8 low) noexcept { return (static_cast<u16>(high) << 8) | low; };
	constexpr bool isInRange(const size_t value, const size_t min, const size_t max) noexcept { return value >= min && value <= max; };
}