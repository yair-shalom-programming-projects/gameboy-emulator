#pragma once

#include <exception>

class InvalidInstructionException : public std::exception
{
public:
	InvalidInstructionException() : std::exception("invalid instruction called") {};
};	