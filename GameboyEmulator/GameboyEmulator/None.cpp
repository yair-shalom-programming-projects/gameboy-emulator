#include "None.h"
#include "Address.h"

using namespace Address::Memory;

//? Constructor ?//


/* -- C'tor -- */
None::None() :
	IMBC()
{
}



//? Methods ?//


/* -- handle writing to specific address in None MBC (Rom only) -- */
bool None::write(const u16 address, const u8 data)
{

	// ReadOnlyMemory area
	return address >= ROM_BANK_0_START && address <= ROM_BANK_N_END;

}

/* -- handle reading to specific address in None MBC (Rom only) -- */
bool None::read(const u16 address, u8& data) const
{

	// ROM is in _cartridge in the same address
	if (address >= ROM_BANK_0_START && address <= ROM_BANK_N_END)
	{
		data = _cartridge[address];
		return true;
	}

	return false;

}
