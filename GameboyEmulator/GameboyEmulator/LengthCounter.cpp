#include "LengthCounter.h"

LengthCounter::LengthCounter(const u16 length, const bool isEnabled)
{
	setLengthCounter(length , isEnabled);
}

void LengthCounter::setLengthCounter(const u16 length, const bool isEnabled)
{
	_lengthTimer = length;
	setEnabled(isEnabled);
}

void LengthCounter::setEnabled(bool isEnabled)
{
	_isEnabled = isEnabled;
}

bool LengthCounter::clock()
{
	if (_isEnabled) {
		decrementTimer();
		return _lengthTimer;
	}
	return true;
}

// length is the length that the channels maxes (64/256)
void LengthCounter::triggerEvent(const u16 length , const u8 frameSequencer)
{
    // if the length timer isn't zero then do nothing on trigger
    if (_lengthTimer) {
        return;
    }

    _lengthTimer = length;// set the new length
    if (!isNextFrameClockLengthClock(frameSequencer) && _isEnabled) {
        decrementTimer(); // if the next frame sequencer clock is not length and the channel is enabled decrement the timer once
    }
}

void LengthCounter::decrementTimer()
{
    if (!_lengthTimer) {
        return;
    }
    --_lengthTimer;
}
bool LengthCounter::isNextFrameClockLengthClock(const u8 frameSequencer)
{
    return frameSequencer % 2;
}

// returns if the the channel should be disabled
bool LengthCounter::checkObscureLengthClock(const u8 value, const u8 frameSequencer)
{
    // if the next frame sequencer clock doesn't clock length and the channel is not enabled
    // and the the length is now enabled and the length timer is not zero then decrement the length timer
    // if the new length timer is zero and channel is now triggered then disable channel
    if (
        !isNextFrameClockLengthClock(frameSequencer) &&
        !_isEnabled && 
        value & Const::APU::FLAG_CHANNEL_LENGTH_FLAG &&
        _lengthTimer
        )
    {

        decrementTimer();
        if (
            !_lengthTimer &&
            !(value & Const::APU::TRIGGER_EVENT_FLAG)
            ) {
            return true;
        }
    }
    return false;
}

std::istream& operator>>(std::istream& is, LengthCounter& lengthCounter)
{
    return is >> lengthCounter._lengthTimer 
              >> lengthCounter._isEnabled;
}

std::ostream& operator<<(std::ostream& os, const LengthCounter& lengthCounter)
{
    return os << lengthCounter._lengthTimer << '\n' 
              << lengthCounter._isEnabled;
}
