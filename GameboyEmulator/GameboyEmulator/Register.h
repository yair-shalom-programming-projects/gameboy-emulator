#pragma once
#include <bitset>
#include "Utils.h"
#include <iostream>

class Register final
{

public:
	using Byte = std::bitset<8>;

	// Constructors
	Register() noexcept : _value(0) {};
	Register(const Byte value) noexcept : _value(value) {};
	Register(const u8 other) noexcept : _value(other) {};

	// Methods
	u16 add16(Register& other, const u16 value = 1) noexcept;
	u16 subtract16(Register& other, const u16 value = 1) noexcept;
	void set16(Register& other, const u16 value) noexcept;
	u16 join(const Register& other) noexcept;

	u8 val() const noexcept { return static_cast<u8>(_value.to_ulong()); };


	// operator overloads
	operator u8() const { return val(); };

	auto operator[](const u8 index) { return _value[index]; };
	constexpr bool operator[](const u8 index) const { return _value[index]; };

	u8 operator+=(const Register& other) noexcept { _value = val() + other; return *this; };
	u8 operator-=(const Register& other) noexcept { _value = val() - other; return *this; };
	u8 operator++() noexcept { _value = val() + 1; return *this; };
	u8 operator--() noexcept { _value = val() - 1; return *this; };

	u8 operator&=(const Register& other) noexcept { _value &= other._value; return *this; };
	u8 operator|=(const Register& other) noexcept { _value |= other._value; return *this; };
	u8 operator^=(const Register& other) noexcept { _value ^= other._value; return *this; };
	u8 operator<<=(const u8 other) noexcept { _value <<= other; return *this; };
	u8 operator>>=(const u8 other) noexcept { _value >>= other; return *this; };

private:
	
	// Fields
	Byte _value;
};

