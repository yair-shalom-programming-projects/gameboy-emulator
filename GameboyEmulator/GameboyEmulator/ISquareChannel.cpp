#include "ISquareChannel.h"

ISquareChannel::ISquareChannel(Memory& memory, u16 NRx0, u16 NRx1, u16 NRx2, u16 NRx3, u16 NRx4):
	IChannelWithEnvelope(
        memory,
        NRx0,
        NRx1,
        NRx2,
        NRx3,
        NRx4
    ),
    _sampleIndex(0)
{
}

void ISquareChannel::clock(int cycles)
{
    _cyclesCount += cycles;
    if (_cyclesCount < _cyclesSampleUpdate) return;
    
    _sampleIndex = ++_sampleIndex % 8;
    _cyclesCount -= _cyclesSampleUpdate;
    
}

void ISquareChannel::triggerEvent(const u8 frameSequencer)
{

    // when a square channel is triggered, it's lower 2-bits are not modified!
    _frequency = _frequency & Const::APU::TWO_BITS_ON | (getFrequency() & ~Const::APU::TWO_BITS_ON );
    _cyclesSampleUpdate = (2048 - _frequency) * 2;
    IChannelWithEnvelope::triggerEvent(frameSequencer);
}

float ISquareChannel::getSample()
{
    if (!_isDacOn) {
        return 0;
    }
    u8 dutyValue = Const::APU::AUDIO_WAVE_PATTERN_ARRAY[_selectedDuty][_sampleIndex];
    return dutyValue ?  +_envelope.getVolume() : -_envelope.getVolume(); 

}

std::istream& ISquareChannel::setState(std::istream& is)
{
    _sampleIndex = is.get();
    _selectedDuty = is.get();
    is >> _frequency;
    return IChannelWithEnvelope::setState(is);
}

std::ostream& ISquareChannel::getState(std::ostream& os) const
{
    os.put(_sampleIndex);
    os.put(_selectedDuty);
    os << _frequency << '\n';
    return IChannelWithEnvelope::getState(os);
}

void ISquareChannel::reset()
{
    _frequency = 0;
    resetDuty();
    IChannelWithEnvelope::reset();
}

void ISquareChannel::resetDuty()
{
    _selectedDuty = 0;
    _sampleIndex = 0;
}

void ISquareChannel::setDuty()
{
    _selectedDuty = (_memory[_NRx1] & Const::APU::FLAG_CHANNEL_DUTY_FLAG) >> 6;// bits 7-8 of NRx1
}

void ISquareChannel::setFrequency()
{
    _frequency = getFrequency();
    _cyclesSampleUpdate = (2048 - _frequency) * 4;
}
