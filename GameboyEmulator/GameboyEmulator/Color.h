#pragma once
#include "Utils.h"
#include <iostream>
#include <SDL.h>

enum class ColorsType : u8 { REGULAR, ORIGINAL };

struct Color final
{
	// Fields
	u8 r = 0, g = 0, b = 0;

	// Operator Overload
	const bool operator==(const Color& other);
	Color& operator=(const Color& other);
	operator u32() const;
	operator SDL_Color() const;

	friend std::istream& operator>>(std::istream& is, Color& color);
	friend std::ostream& operator<<(std::ostream& os, const Color& color);

};