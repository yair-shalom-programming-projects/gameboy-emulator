#pragma once
#include <utility>

template<class T>
class Singleton
{

public:

	// Constructors
	Singleton(const Singleton&) = delete;
	Singleton& operator=(const Singleton) = delete;

	// Methods
	static T& getInstance()
	{
		static T i{ Singleton_t() };
		return i;
	}

protected:

	// Constructors
	Singleton() = default;
	struct Singleton_t {};

};