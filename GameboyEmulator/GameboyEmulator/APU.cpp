#include "APU.h"
#include "Address.h"
#include <iostream>
#include <vector>
#include <SDL.h>



APU::APU(Singleton_t):
    _memory(Memory::getInstance()),
    _squareChannel1(Memory::getInstance()),
    _squareChannel2(Memory::getInstance()),
    _noiseChannel(Memory::getInstance()),
    _waveChannel(Memory::getInstance())
{
    SDL_Init(SDL_INIT_AUDIO);
    SDL_AudioSpec desiredSpec;
    desiredSpec.freq = Const::APU::AUDIO_FREQUENCY;
    desiredSpec.format = AUDIO_F32;
    desiredSpec.channels = 2;
    desiredSpec.samples = Const::APU::AUDIO_NUM_SAMPLES;
    desiredSpec.callback = NULL;
    desiredSpec.userdata = this;

    SDL_AudioSpec obtainedSpec;

    audioDevice = SDL_OpenAudioDevice(nullptr, 0, &desiredSpec, &obtainedSpec, 0);
    SDL_PauseAudioDevice(audioDevice, 0);
}

void APU::update(int cycles)
{
    if (_isEnabled)
    {
        cycleFrameSequencer(cycles);
        cycleChannels(cycles);
        cycleSamples(cycles);
    }
}

void APU::reset()
{
    _cyclesCount = 0;
    _cyclesFrameSequencerCount = 0;
    _sampleCounter = 0;
    _frameSequencerCounter = 0;
    _sampleIndex = 0;
    _volumeLeft = 1;
    _volumeRight = 1;
    channelsOutput[8]; // TODO: Reset this array
    _isEnabled = true;
    resetChannels();
}

bool APU::write(const u16 address, u8 value)
{
    if (isAddressDivWrite(address)) 
    {
        checkDivClockFs(_memory[address] , 0);
        return false;
    }
    if (!isAddressAPU(address))
    {
        return false;
    }

    bool isSquareChannelsLength = false;

    if(!_isEnabled)
    {
        if(address == Address::APU::NRx52 && (value & Const::APU::FLAG_APU_POWER))
        {
            _memory[address] = value;
            std::cout << "Powering on" << std::endl;
            _isEnabled = true;
            _frameSequencerCounter = 7; // resetting the frame sequencer so the next clock will be 0
            resetDuties(); // reseting duties
            return true;
        }
        else if(isAddressLengthCounter(address)) 
        {
            // change only the length if its powered down in square one/two
            // duties are not affected from write when its powered down
            if (isSquareChannelsLength = isAddressLengthCounterSquare(address); isSquareChannelsLength) 
            {
                _memory[address] =
                    (value & ~Const::APU::LAST_TWO_BITS_ON) | (_memory[address] & Const::APU::LAST_TWO_BITS_ON);
            }
        }
        else 
        {
            return true;
        }
    }

    if (isAddressWaveRam(address) && _waveChannel.isRunning())
    {
        _waveChannel.setWaveRam(address, value);
        return true;
    }

    if (!isSquareChannelsLength) 
    {
        _memory[address] = value;
    }

    switch (address)
    {
    // Update Volumes
    case Address::APU::NRx50:
        _volumeLeft = ((value >> 4) & Const::APU::FLAG_OUTPUT_VOLUME);
        _volumeRight = (value & Const::APU::FLAG_OUTPUT_VOLUME);
        break;
    
    // Udpated Channles Output Left/Right
    case Address::APU::NRx51:
        for (auto i = 0; i < 8; ++i)
        {
            channelsOutput[i] = value & Const::APU::FLAGS_FOR_OUTPUT[i];
        }
        break;

    // Power Off
    case Address::APU::NRx52:
        if (!(value & Const::APU::FLAG_APU_POWER) && _isEnabled) 
        {
            std::cout << "Powering off" << std::endl;
            for (int i = Address::APU::START; i < Address::APU::NRx52; ++i) 
            {
                _memory[i] = 0;
            }
            _squareChannel1.stop();
            _squareChannel2.stop();
            _waveChannel.stop();
            _noiseChannel.stop();
            _isEnabled = false;
        }
        break;

    case Address::APU::SquareChannel1::NRx0:
        _squareChannel1.setSweep();
        break;
    case Address::APU::SquareChannel1::NRx1:
        _squareChannel1.setDuty();
        _squareChannel1.setLengthCounter();
        break;
    case Address::APU::SquareChannel1::NRx2:
        _squareChannel1.setDacPower(value);
        _squareChannel1.setEnvelope();
        break;
    case Address::APU::SquareChannel1::NRx3:
        _squareChannel1.setFrequency();
        break;
    case Address::APU::SquareChannel1::NRx4:
        _squareChannel1.checkObscureLengthClock(value, _frameSequencerCounter);
        _squareChannel1.setFrequency();
        _squareChannel1.setEnabledFlagLengthCounter();
        if (value & Const::APU::TRIGGER_EVENT_FLAG) 
        {
            _squareChannel1.triggerEvent(_frameSequencerCounter);
        }
        break;

    case Address::APU::SquareChannel2::NRx1:
        _squareChannel2.setDuty();
        _squareChannel2.setLengthCounter();
        break;
    case Address::APU::SquareChannel2::NRx2:
        _squareChannel2.setDacPower(value);
        _squareChannel2.setEnvelope();
        break;
    case Address::APU::SquareChannel2::NRx3:
        _squareChannel2.setFrequency();
        break;
    case Address::APU::SquareChannel2::NRx4:
        // finish trigger event
        _squareChannel2.checkObscureLengthClock(value, _frameSequencerCounter);
        _squareChannel2.setFrequency();
        _squareChannel2.setEnabledFlagLengthCounter();
        if (value & Const::APU::TRIGGER_EVENT_FLAG)
        {
            _squareChannel2.triggerEvent(_frameSequencerCounter);
        }
        break;

    case Address::APU::WaveChannel::NRx0:
        _waveChannel.setDacPower(value);
        break;
    case Address::APU::WaveChannel::NRx1:
        _waveChannel.setLengthCounter();
        break;
    case Address::APU::WaveChannel::NRx2:
        _waveChannel.readLevel();
        break;
    case Address::APU::WaveChannel::NRx3:
        _waveChannel.setFrequency();
        break;
    case Address::APU::WaveChannel::NRx4:
        // finish trigger event
        _waveChannel.checkObscureLengthClock(value, _frameSequencerCounter);
        _waveChannel.setFrequency();
        _waveChannel.setEnabledFlagLengthCounter();
        if (value & Const::APU::TRIGGER_EVENT_FLAG)
        {
            _waveChannel.triggerEvent(_frameSequencerCounter);
        }
        break;

    case Address::APU::NoiseChannel::NRx1:
        _noiseChannel.setLengthCounter();
        break;
    case Address::APU::NoiseChannel::NRx2:
        _noiseChannel.setDacPower(value);
        _noiseChannel.setEnvelope();
        break;
    case Address::APU::NoiseChannel::NRx3:
        _noiseChannel.setNoiseData();
        break;
    case Address::APU::NoiseChannel::NRx4:
        _noiseChannel.checkObscureLengthClock(value, _frameSequencerCounter);
        _noiseChannel.setEnabledFlagLengthCounter();
        // finish trigger event
        if (value & Const::APU::TRIGGER_EVENT_FLAG) 
        {
            _noiseChannel.triggerEvent(_frameSequencerCounter);
        }
        break;
    }
    return true;

}

bool APU::read(const u16 address , u8& data)
{

    if (!isAddressAPU(address))
    {
        return false;
    }
    
    const u8 read_mask[Address::APU::END - Address::APU::START + 1] = {
        /* NRX0  NRX1  NRX2  NRX3  NRX4 */
           0x80, 0x3F, 0x00, 0xFF, 0xBF, // NR1X
           0xFF, 0x3F, 0x00, 0xFF, 0xBF, // NR2X
           0x7F, 0xFF, 0x9F, 0xFF, 0xBF, // NR3X
           0xFF, 0xFF, 0x00, 0x00, 0xBF, // NR4X
           0x00, 0x00, 0x70, 0xFF, 0xFF, // NR5X

           0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, // Unused
           // Wave RAM
           0, /* ... */
    };

    
    if (address == Address::APU::NRx52)
    {
        data =  _isEnabled ? 0xF0 : 0X70;
        u8 toOR = 0x1;
        for (auto& channel : std::vector<IChannel*>{
                                    dynamic_cast<IChannel*>(&_squareChannel1),
                                    dynamic_cast<IChannel*>(&_squareChannel2),
                                    dynamic_cast<IChannel*>(&_waveChannel),
                                    dynamic_cast<IChannel*>(&_noiseChannel)
                                    })
        {
            // the 4 first bits on NR52 represent if the channels are on
            if (channel->isRunning()) {
                data |= toOR;
            }
            toOR *= 2;
        }
        return true;
    }
    if (isAddressWaveRam(address) && _waveChannel.isRunning()) 
    {
        data = _waveChannel.getWaveRam(address);
        std::cout << "Reading Wave Ram: " << std::to_string(data) << "\n";
        return true;
    }

    data = _memory[address] | (read_mask[address - Address::APU::START]);
    return true;
}


bool APU::isAddressDivWrite(const u16 address)
{
    return address == Address::Timer::DIVIDER_REGISTER;
}

void APU::checkDivClockFs(const u8 oldDiv, const u8 newDiv)
{
    // in case of bit 5 was on and now off, clock the frame sequencer
    if (Utils::isBitOn<int>(oldDiv, 5) && !Utils::isBitOn<int>(oldDiv, 5)) 
    {
        clockFrameSequencer();
    }
}

void APU::resetDuties()
{
    _squareChannel1.resetDuty();
    _squareChannel2.resetDuty();
}

void APU::resetChannels()
{
    const auto channels = std::vector<IChannel*>
    {
                            dynamic_cast<IChannel*>(&_squareChannel1),
                            dynamic_cast<IChannel*>(&_squareChannel2),
                            dynamic_cast<IChannel*>(&_waveChannel),
                            dynamic_cast<IChannel*>(&_noiseChannel)
    };
    for (auto* channel : channels) 
    {
        channel->reset();
    }
}

void APU::cycleFrameSequencer(int cycles)
{
    // frame sequencer is clocked by 512hz timer
    _cyclesFrameSequencerCount += cycles;
    if (_cyclesFrameSequencerCount >= Const::APU::CYCLES_512Hz) 
    {
        clockFrameSequencer();
        _cyclesFrameSequencerCount -= Const::APU::CYCLES_512Hz;
    }
}

void APU::clockFrameSequencer()
{
// clocking:
//    Step   Length Ctr  Vol Env     Sweep
//        -------------------------------------- -
//        0      Clock - -
//        1 - --
//        2      Clock - Clock
//        3 - --
//        4      Clock - -
//        5 - --
//        6      Clock - Clock
//        7 - Clock -
//        -------------------------------------- -
//        Rate   256 Hz      64 Hz       128 Hz

    _frameSequencerCounter = ++_frameSequencerCounter % 8;
    clockLength();
    clockEnvelope();
    clockSweep();
}

void APU::clockLength()
{
    switch (_frameSequencerCounter)
    {
    case 0:
    case 2:
    case 4:
    case 6:
        _squareChannel1.clockLengthCounter();
        _squareChannel2.clockLengthCounter();
        _waveChannel.clockLengthCounter();
        _noiseChannel.clockLengthCounter();
    }
}

void APU::clockEnvelope()
{
    switch (_frameSequencerCounter) 
    {
    case Const::APU::ENVELOP_STEP_NUMBER:
        _squareChannel1.clockEnvelope();
        _squareChannel2.clockEnvelope();
        _noiseChannel.clockEnvelope();
    }
}

void APU::clockSweep()
{
    switch (_frameSequencerCounter) 
    {
    case 2:
    case 6:
        _squareChannel1.sweepTick();
    }
}

void APU::cycleChannels(int cycles)
{
    _squareChannel1.clock(cycles);
    _squareChannel2.clock(cycles);
    _waveChannel.clock(cycles);
    _noiseChannel.clock(cycles);
}

void APU::cycleSamples(int cycles)
{

    _cyclesCount += cycles * 2;
    if (_cyclesCount >= Const::APU::AUDIO_CYCLES_UNTIL_SAMPLE_COLLECTION) 
    {
        _cyclesCount -= Const::APU::AUDIO_CYCLES_UNTIL_SAMPLE_COLLECTION;

        // Collect sample
        float mixedLeftChannel = 0, mixedRightChannel = 0;
        float audioVolume = 1.0;
        int leftVol = _volumeLeft - 2;
        int rightVol = _volumeRight - 2;
        float aux;
        const std::vector<IChannel*> channels = {
                dynamic_cast<IChannel*>(&_squareChannel1),
                dynamic_cast<IChannel*>(&_squareChannel2),
                dynamic_cast<IChannel*>(&_waveChannel)
            //    dynamic_cast<IChannel*>(&_noiseChannel)
        };

        for (int i = 0; i < Const::APU::NUMBER_OF_CHANNELS - 1; ++i) 
        {
            aux = channels[i]->getSample();

            if (channelsOutput[i * 2]) 
            {
                SDL_MixAudioFormat(reinterpret_cast<u8*>(&mixedLeftChannel), reinterpret_cast<u8*>(&aux), AUDIO_F32,
                    sizeof(float), leftVol);
            }

            if (channelsOutput[i * 2 + 1]) 
            {
                SDL_MixAudioFormat(reinterpret_cast<u8*>(&mixedRightChannel), reinterpret_cast<u8*>(&aux), AUDIO_F32,
                    sizeof(float), rightVol);
            }

        }
        _audioBuffer[_sampleIndex++] = mixedLeftChannel * audioVolume;
        _audioBuffer[_sampleIndex++] = mixedRightChannel * audioVolume;
    }

    if (_sampleIndex >= Const::APU::AUDIO_NUM_SAMPLES)
    {
        _sampleIndex -= Const::APU::AUDIO_NUM_SAMPLES;

        SDL_QueueAudio(audioDevice, _audioBuffer, Const::APU::AUDIO_NUM_SAMPLES * sizeof(float));
    }

    
}

bool APU::isAddressAPU(const u16 address)
{
    return address >= Address::APU::START &&
           address <= Address::APU::END;
}

bool APU::isAddressLengthCounter(const u16 address)
{
    return address == Address::APU::SquareChannel1::NRx1 ||
           address == Address::APU::SquareChannel2::NRx1 ||
           address == Address::APU::WaveChannel::NRx1    ||
           address == Address::APU::NoiseChannel::NRx1;
}

bool APU::isAddressLengthCounterSquare(const u16 address)
{
    return address == Address::APU::SquareChannel1::NRx1 ||
           address == Address::APU::SquareChannel2::NRx1;
}

bool APU::isAddressWaveRam(const u16 address)
{
    return address >= Address::APU::WAVE_RAM_START && 
           address <= Address::APU::WAVE_RAM_END;
}


std::ostream& operator<<(std::ostream& os, const APU& APU) 
{
    os.put(APU._frameSequencerCounter);

    for (const float& sample : APU._audioBuffer)
        os << sample << '\n';

    return os << APU._cyclesFrameSequencerCount << '\n'
              << APU._cyclesCount << '\n'
              << APU._sampleIndex << '\n'
              << APU._squareChannel1
              << APU._squareChannel2
              << APU._waveChannel
              << APU._noiseChannel;
};

std::istream& operator>>(std::istream& is, APU& APU) 
{
    APU._frameSequencerCounter = is.get();

    for (float& sample : APU._audioBuffer)
        is >> sample;

    return is >> APU._cyclesFrameSequencerCount
              >> APU._cyclesCount
              >> APU._sampleIndex
              >> APU._squareChannel1
              >> APU._squareChannel2
              >> APU._waveChannel
              >> APU._noiseChannel;
}