#include "Cartridge.h"
#include <fstream>
#include <algorithm>
#include "None.h"
#include "MBC2.h"
#include "MBC5.h"
#include "Address.h"
#include "Constants.h"
#include "UnsupportedGameException.h"
#include "FileNotFoundException.h"
#include "InvalidAddressException.h"
#include "InvalidBanksCountException.h"

using namespace Address::Cartridge;
using namespace Const::Cartridge;
using std::make_unique;


//? Constructors ?//

/* -- C'tor -- */
Cartridge::Cartridge(Singleton_t)
{
}


//? Operators Overloads ?//

/* -- overload to index operator -- */
u8& Cartridge::operator[](const size_t& address)
{
    return _cartridge[address];
}

/* -- overload to index operator to const cartridge -- */
u8 Cartridge::operator[](const size_t& address) const
{
    return _cartridge[address];
}


//? Methods ?//

/* -- get number of ROM banks (16KB each) -- */
u16 Cartridge::getROMBanksCount() const
{
    u16 banks = std::pow(2, _cartridge[ROM_TYPE] + 1);

    if (banks < MIN_ROM_BANKS_COUNT || banks > MAX_ROM_BANKS_COUNT)
        throw InvalidROMBanksCountException();
    
    return banks;
}

/* -- get number of RAM banks (8KB each) -- */
u16 Cartridge::getRAMBanksCount() const 
{
    switch (_cartridge[RAM_TYPE])
    {
        case 0x00: return 0;
        case 0x02: return 1;
        case 0x03: return 4;
        case 0x04: return 16;
        case 0x05: return 8;
        default: throw InvalidRAMBanksCountException();
    }
}

/* -- get Memory Bank Controller -- */
void Cartridge::getMBC(std::unique_ptr<IMBC>& MBC) const
{
    switch (_cartridge[MBC_TYPE])
    {
        case 0:					   MBC = make_unique<None>(); break;
        case 1: case 2: case 3:    MBC = make_unique<MBC1>(); break;
        case 5: case 6:		 	   MBC = make_unique<MBC2>(); break;
        case 25: case 26: case 27: 
        case 28: case 29: case 30: MBC = make_unique<MBC5>(); break;
        default:                   throw UnsupportedGameException();
    }
}

void Cartridge::reset()
{
    _cartridge.clear();
}


/* -- load cartridge from file to _cartridge -- */
void Cartridge::loadCartridge(const std::string_view filePath)
{
    reset();
    
    std::ifstream file(filePath.data(), std::ios::binary);

    if (!file.is_open()) 
        throw FileNotFoundException();

    while (!file.eof())
        _cartridge.push_back(file.get());

}

std::ostream& operator<<(std::ostream& os, const Cartridge& cartridge)
{

    os << cartridge._cartridge.size();

    for (const auto& byte : cartridge._cartridge)
        os.put(byte);
    
    return os;
}

std::istream& operator>>(std::istream& is, Cartridge& cartridge)
{

    size_t size = 0;
    is >> size;
    
    for (size_t i = 0; i < size; ++i)
        cartridge._cartridge[i] = is.get();
    
    return is;
}
