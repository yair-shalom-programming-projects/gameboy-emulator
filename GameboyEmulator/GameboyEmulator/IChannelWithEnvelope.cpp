#include "IChannelWithEnvelope.h"

IChannelWithEnvelope::IChannelWithEnvelope(Memory& memory, u16 NRx0, u16 NRx1, u16 NRx2, u16 NRx3, u16 NRx4):
	IChannel(
		memory,
		NRx0,
		NRx1,
		NRx2,
		NRx3,
		NRx4,
		LengthCounter(
		Const::APU::AUDIO_DEFAULT_LENGTH - (memory[NRx1] & Const::APU::FLAG_CHANNEL_LENGTH_DATA),
		memory[NRx4] & Const::APU::FLAG_CHANNEL_LENGTH_FLAG
		)),
	_envelope(VolumeEnvelope(
		(_memory[NRx2] & Const::APU::FLAG_CHANNEL_ENVELOPE_VOLUME) >> 4,
		_memory[NRx2] & Const::APU::FLAG_CHANNEL_ENVELOPLE_PERIOD,
		_memory[NRx2] & Const::APU::FLAG_CHANNEL_ENVELOPE_AMPLIFY
	))
{
}

void IChannelWithEnvelope::setEnvelope()
{
	_envelope.setVolumeEnvelope(
		(_memory[_NRx2] & Const::APU::FLAG_CHANNEL_ENVELOPE_VOLUME) >> 4, // 5-8 bits of NRx2 are the value of the starting volume
		_memory[_NRx2] & Const::APU::FLAG_CHANNEL_ENVELOPLE_PERIOD, // 1-3 bits of NRx2 are the envelope period timer
		_memory[_NRx2] & Const::APU::FLAG_CHANNEL_ENVELOPE_AMPLIFY // 4th bit of NRx2 is whether the envelope will amplify or decrease the volume
	);
}


void IChannelWithEnvelope::triggerEvent(const u8 frameSequencer)
{
	// isRunnig is true when triggered
	_isRunning = true;
	_lengthCounter.triggerEvent(Const::APU::AUDIO_DEFAULT_LENGTH, frameSequencer);
	_envelope.triggerEvent(
		_memory[_NRx2] & Const::APU::FLAG_CHANNEL_ENVELOPE_VOLUME,
		frameSequencer
		);
	IChannel::triggerEvent();
}

void IChannelWithEnvelope::setDacPower(const int value)
{
	// if the first 5 bits are off then the dac should be turned off and so the channel
	if (value & Const::APU::DAC_POWER_FLAG) {
		startDac();
	}
	else {
		stop();
	}
}


void IChannelWithEnvelope::clockEnvelope()
{
	_envelope.clock();
}




void IChannelWithEnvelope::reset()
{
	_envelope.setVolumeEnvelope(0, 0, 0);
	IChannel::reset();
}

void IChannelWithEnvelope::setLengthCounter()
{
	// default value for this channel - (first 6 bits of NRx1 are the new value of length) = new length timer value
	// the 7th bit of NRx4 represents the power status of the length
	_lengthCounter.setLengthCounter(
		Const::APU::AUDIO_DEFAULT_LENGTH - (_memory[_NRx1] & Const::APU::FLAG_CHANNEL_LENGTH_DATA),
		_memory[_NRx4] & Const::APU::FLAG_CHANNEL_LENGTH_FLAG
	);
}

std::istream& IChannelWithEnvelope::setState(std::istream& is)
{
	is >> _envelope;
	return IChannel::setState(is);
}

std::ostream& IChannelWithEnvelope::getState(std::ostream& os) const
{
	os << _envelope;
	return IChannel::getState(os);
}

