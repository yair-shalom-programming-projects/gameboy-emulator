#pragma once
#include "IMBC.h"
#include <array>
#include <fstream>

class MBC1 : public IMBC
{

public:

	// Constructors
	MBC1();
	virtual ~MBC1() noexcept = default;

	// Method
	virtual bool write(const u16 address, const u8 data) override;
	virtual bool read(const u16 address, u8& data) const override;

	virtual std::istream& setState(std::istream& is) override;
	virtual std::ostream& getState(std::ostream& os) const override;

protected:

	enum class Mode : u8 { ROM, RAM };

	// Methods
	virtual void handleBanking(const u16 address, const u8 data) noexcept;
	virtual void setRAMEnabled(const u8 data) noexcept;
	
	virtual void setBank1(const u8 data) noexcept;
	virtual void setBank2(const u8 data) noexcept;

	virtual u8 getROMBank(const u16 address) const noexcept;
	virtual u8 getRAMBank() const noexcept;

	virtual void reset();


	bool isMode(const Mode mode) const { return _mode == mode; };

	// Fields
	const u16 _ROMBanksCount;
	const u16 _RAMBanksCount;

	std::array<std::array<u8, 0x2000>, 4> _RAMBanks;
	
	Mode _mode;
	
	u8 _bank1;
	u8 _bank2;
	
	bool _RAMEnabled;

};

