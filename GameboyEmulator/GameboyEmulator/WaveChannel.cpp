#include "WaveChannel.h"
#include "Address.h"
#include <iostream>

WaveChannel::WaveChannel(Memory& memory) :
    IChannel(
        memory,
        Address::APU::WaveChannel::NRx0,
        Address::APU::WaveChannel::NRx1,
        Address::APU::WaveChannel::NRx2,
        Address::APU::WaveChannel::NRx3,
        Address::APU::WaveChannel::NRx4,
        LengthCounter(
            Const::APU::AUDIO_WAVE_LENGTH - memory[Address::APU::WaveChannel::NRx1] ,
            memory[Address::APU::WaveChannel::NRx4] & Const::APU::FLAG_CHANNEL_LENGTH_FLAG)
    )
{
}


void WaveChannel::clock(int cycles)
{

    if (_cyclesCount <= 4) {
        if (_cyclesCount == 4) {
            _waveRecentlyRead = true; // wave was recently read
        }

        _cyclesCount += (2048 - getFrequency()) * 2 - 4;
        _sampleIndex = (_sampleIndex + 1) & 31; // update sample increase
        setWaveByte(); // set the new wave byte
    }
    else {
        _cyclesCount -= cycles; // cycles decrement in 4s
        _waveRecentlyRead = false; // if it wasn't read now then it wasn't read recently
    }
}

void WaveChannel::setDacPower(const int value)
{
    // if bit 8th is on for NRx0 then dac is on
    if (value & Const::APU::DAC_POWER_FLAG_WAVE) {
        startDac();
    }
    else {
        stop();
    }
}

void WaveChannel::setFrequency()
{
    _cyclesSampleUpdate = (2048 - getFrequency()) * 2;
}

void WaveChannel::readLevel()
{
    _outputLevel = (Const::APU::AudioLevel)((_memory[_NRx2] & Const::APU::FLAG_CHANNEL_LEVEL_FLAG) >> 5); // the level of the channel is the bits 6-8 in NRx2
}

u8 WaveChannel::getWaveRam(const u16 address)
{
    return _waveRecentlyRead ? _waveByte : 0xFF; // only if it was recently read you read the current wave byte otherwise you return 0xFF
}

void WaveChannel::setWaveRam(const u16 address, const u8 value)
{
    if (_waveRecentlyRead) { // writing to wave ram only occurs when the wave is recently read
        _memory[Address::APU::WAVE_RAM_START + (_sampleIndex / 2)] = value;
    }
}

void WaveChannel::setWaveByte()
{
    _waveByte = _memory[Address::APU::WAVE_RAM_START + (_sampleIndex / 2)];
}

void WaveChannel::triggerEvent(const u8 frameSequencer)
{
    /*
        Triggering the wave channel on the DMG while it reads a sample byte will alter the first four bytes of wave RAM.
        If the channel was reading one of the first four bytes, 
        the only first byte will be rewritten with the byte being read. 
        If the channel was reading one of the later 12 bytes, 
        the first FOUR bytes of wave RAM will be rewritten with the four aligned bytes that the read was from
        (bytes 4-7, 8-11, or 12-15); 
        for example if it were reading byte 9 when it was retriggered, 
        the first four bytes would be rewritten with the contents of bytes 8-11.
    */
    if (_isRunning && _isDacOn && _cyclesCount <= 2) { 
        unsigned int bytePos = (_sampleIndex + 1) / 2;
        if (bytePos < 4 ){
            _memory[Address::APU::WAVE_RAM_START] = _memory[Address::APU::WAVE_RAM_START + bytePos];
        }
        else {
            unsigned int src = bytePos & 0xC;
            for (int i = 0; i < 4; ++i) {
                _memory[Address::APU::WAVE_RAM_START + i] = _memory[Address::APU::WAVE_RAM_START + src + i];
            }
        }
    }

    _isRunning = true;
    _cyclesCount = (2048 - getFrequency()) * 2 + 6;
    _sampleIndex = 0;
    setWaveByte();
    _lengthCounter.triggerEvent(Const::APU::AUDIO_WAVE_LENGTH , frameSequencer);
    IChannel::triggerEvent();
}

float WaveChannel::getSample()
{
    if (!_isDacOn ) {
        return 0;
    }
    // Each byte in the Wave RAM holds two 2 4-bit samples
    u8 sample = (_sampleIndex & 1) ? _waveByte & 0xF : _waveByte >> 4;;
    sample >>= Const::APU::WAVE_CHANNEL_SHIFT_BY_VOLUME[static_cast<u8>(_outputLevel)];
    return sample;
}

std::istream& WaveChannel::setState(std::istream& is)
{
    _waveByte = is.get();
    u8 outputLevel = is.get();

    _outputLevel = static_cast<Const::APU::AudioLevel>(outputLevel);
    
    is >> _sampleIndex
       >> _waveRecentlyRead;

    return IChannel::setState(is);
}

std::ostream& WaveChannel::getState(std::ostream& os) const
{
    os.put(_waveByte);
    os.put(static_cast<u8>(_outputLevel));

    os << _sampleIndex << '\n'
       << _waveRecentlyRead;

    return IChannel::getState(os);
}

void WaveChannel::reset()
{
    _sampleIndex = 0;
    _waveByte = 0;
    _outputLevel = Const::APU::AudioLevel::MUTE;
}

void WaveChannel::setEnabled()
{
    _isRunning = _memory[Address::APU::NRx52] & Const::APU::POWER_FLAG_WAVE_CHANNEL;
}


void WaveChannel::setLengthCounter()
{
    // default value for this channel - (all 8 bits of NRx1 are the new value of length) = new length timer value
    // the 7th bit of NRx4 represents the power status of the length
    _lengthCounter.setLengthCounter(
        Const::APU::AUDIO_WAVE_LENGTH - _memory[_NRx1],
        _memory[_NRx4] & Const::APU::FLAG_CHANNEL_LENGTH_FLAG
    );
}

std::istream& operator>>(std::istream& is, WaveChannel& waveChannel)
{
    return waveChannel.setState(is);
}
std::ostream& operator<<(std::ostream& os, const WaveChannel& waveChannel)
{
    return waveChannel.getState(os);
}
