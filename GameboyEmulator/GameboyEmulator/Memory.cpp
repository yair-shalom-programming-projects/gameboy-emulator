#include "Memory.h"
#include "Address.h"
#include "Joypad.h"
#include "APU.h"
#include "GPU.h"
#include "Timers.h"
#include <iostream>


using namespace Address::Memory;
using namespace Utils;


//? Constructors ?//

Memory::Memory(Singleton_t) :
	_cartridge(Cartridge::getInstance())
{
}



//? Methods ?//

/* 
	-- read a byte from memory -- 
	* input: address to read from, is this a read inc/dec operation? (for OAM bug)
	* output: data in the given address
*/
u8 Memory::read(const u16 address, const bool isIncOrDec)
{

	if (isIncOrDec)
		doOAMBug(address, OAM_BUG_TYPE::READ_DURING_INC_DEC);


	// check for special behavior when reading from specific addresses in the MBC
	if (u8 data = 0; _MBC->read(address, data))
	{
		return data;
	}
	if (u8 data = 0; APU::getInstance().read(address , data))
	{
		return data;
	}

	// do OAM bug
	else if (address >= OAM_BUG_START && address <= OAM_BUG_END && GPU::isMode(Mode::OAM))
	{
		doOAMBug(address, OAM_BUG_TYPE::READ);
		return 0xFF;
	}

	// get desired joypad state (directions/buttons)
	else if (address == Address::JOYPAD)
	{
		return Joypad::getInstance();
	}

	else if (isUnmappedAddress(address))
	{ 
		return 0xFF;
	}

	else
	{
		return _memory[address];
	}

}

/*
	-- read a word from memory --
	* input: address to read from,
			 is increase or not - necessary for implementing OAM bug
	* output: data in the given address
*/
u16 Memory::read16(const u16 address, const bool isIncOrDec)
{
	// 16bits stores reversed (2F 99 -> 992F)
	return join(read(address + 1, isIncOrDec), read(address, isIncOrDec));
}

/* -- reads directly from _memory buffer -- */
u16 Memory::rawRead16(const u16 address) const
{
	return join(_memory[address + 1], _memory[address]);
}

/* -- reset memory to the initial state -- */
void Memory::reset()
{
	resetMemory();
	_cartridge.getMBC(_MBC);
}

/* -- load cartridge to memory -- */
void Memory::loadCartridge(const std::string_view filePath)
{
	_cartridge.loadCartridge(filePath);
}


/* -- handle writing a byte to memory -- */
void Memory::write(const u16 address, const u8 data)
{

	static Timers& _timers = Timers::getInstance();

	// do DMA transfer
	if (address == Address::DMA_TRANSFER)
	{
		DMATransfer(data);
	}


	// check for special behavior when writing to specific addresses in the MBC
	if (_MBC->write(address, data));

	// check for special behavior when writing to specific addresses in the MBC
	if (APU::getInstance().write(address, data));

	// writing to Echo RAM writes to working RAM too
	else if (address >= ECHO_START && address < ECHO_END)
	{
		_memory[address] = data;
		_memory[address - 0x2000] = data;
	}

	// do OAM bug
	else if (address >= OAM_BUG_START && address <= OAM_BUG_END && GPU::isMode(Mode::OAM))
	{
		doOAMBug(address, OAM_BUG_TYPE::WRITE);
	}

	// writing to Timer Divider resets the Internal Counter (Divider is the MSB)
	else if (address == Address::Timer::DIVIDER_REGISTER)
	{
		rawWrite16(Address::Timer::INTERNAL_COUNTER, 0);
	}

	// changing Timer Control affects Timer overflow immediately, bits 3-7 unused
	else if (address == Address::Timer::CONTROL)
	{
		_memory[address] = data | 0b11111000;
		_timers.update(0);
		_timers.update(0);
	}

	// timer cannot be written 1 M-cycle after overflow
	else if (address == Address::Timer::TIMER && _timers.isCycleAfterOverflow())
	{
	}

	// if writing to current scanline, resets it 
	else if (address == Address::LCD::LY)
	{
		_memory[address] = 0;
	}

	// unused addresses
	else if (address >= 0xFF4C && address <= 0xFF7F)
	{
	}

	else if (writeToRegistersWithUnusedBits(address, data))
	{
	}

	else
	{
		_memory[address] = data;
	}

}

/* -- write 16bits to memory -- */
void Memory::write16(const u16 address, const u16 data)
{

	// 16bits stores reversed (992F -> 2F99)
	write(address, data);
	write(address + 1, data >> 8);

}

/* -- write directly to buffer -- */
void Memory::rawWrite16(const u16 address, const u16 data)
{
	_memory[address] = data;
	_memory[address + 1] = data >> 8;
}



//? Private Methods ?//

/* -- reset _memory to his initial state -- */
void Memory::resetMemory() noexcept
{
	_memory.fill(0);
	_memory[0xFF00] = 0xFF;  // Joypad
	_memory[0xFF03] = 0xCC;  // Internal Counter
	_memory[0xFF04] = 0xAB;  // Divider Register
	_memory[0xFF05] = 0x00;  // TIMA
	_memory[0xFF06] = 0x00;  // TMA
	_memory[0xFF07] = 0x00;  // TAC
	_memory[0xFF10] = 0x80;  // NR10
	_memory[0xFF11] = 0xBF;  // NR11
	_memory[0xFF12] = 0xF3;  // NR12
	_memory[0xFF14] = 0xBF;  // NR14
	_memory[0xFF16] = 0x3F;  // NR21
	_memory[0xFF17] = 0x00;  // NR22
	_memory[0xFF19] = 0xBF;  // NR24
	_memory[0xFF1A] = 0x7F;  // NR30
	_memory[0xFF1B] = 0xFF;  // NR31
	_memory[0xFF1C] = 0x9F;  // NR32
	_memory[0xFF1E] = 0xBF;  // NR33
	_memory[0xFF20] = 0xFF;  // NR41
	_memory[0xFF21] = 0x00;  // NR42
	_memory[0xFF22] = 0x00;  // NR43
	_memory[0xFF23] = 0xBF;  // NR30
	_memory[0xFF24] = 0x77;  // NR50
	_memory[0xFF25] = 0xF3;  // NR51
	_memory[0xFF26] = 0xF1;  // NR52
	_memory[0xFF40] = 0x91;  // LCDC
	_memory[0xFF42] = 0x00;  // SCY
	_memory[0xFF43] = 0x00;  // SCX
	_memory[0xFF44] = 0x00;  // LY
	_memory[0xFF45] = 0x00;  // LYC
	_memory[0xFF47] = 0xFC;  // BGP
	_memory[0xFF48] = 0xFF;  // OBP0
	_memory[0xFF49] = 0xFF;  // OBP1
	_memory[0xFF4A] = 0x00;  // WY
	_memory[0xFF4B] = 0x00;  // WX
	_memory[0xFF0F] = 0xE0;  // IF
	_memory[0xFFFF] = 0xE0;  // IE
	resetWaveRam();
}

void Memory::resetWaveRam() noexcept
{
	for (auto i = Address::APU::WAVE_RAM_START; i < Address::APU::WAVE_RAM_END; ++i) {
		_memory[i] = Const::APU::WAVE_RAM_DEFUALT_VALUES[i - Address::APU::WAVE_RAM_START];
	}
}
	

/*
	--
	Sprite Attr Table can only be accessed in Mode::SEARCH_SPRITES_ATTR.
	DMA is a way of copying data to the sprites RAM for the given address
	--
	* input: offset to start reading from (data passed when writing)
*/
void Memory::DMATransfer(const u8 data)
{

	// data is the transfer source address divideed by 0x100 
	const u16 offsetAddress = data << 8;
	
	for (u8 i = 0; i < 0xA0; ++i)
		write(SPRITE_TABLE_START + i, read(offsetAddress + i));

}


/*
	-- handle write to register with unused bits --
	* input: memory address, data to write
	* output: is the given address is one of those registers?
*/
bool Memory::writeToRegistersWithUnusedBits(const u16 address, const u8 data)
{

	// bits 6-7 unused
	if (address == Address::JOYPAD)
	{
		_memory[address] = data | 0b11000000;
	}

	// bits 1-6 unused
	else if (address == 0xFF02)
	{
		_memory[address] = data | 0b01111110;
	}


	// bit 7 unused, bits 0-2 readonly
	else if (address == Address::LCD::STATUS)
	{
		_memory[address] |= data | 0b11111000;
	}

	// bits 0-6 unused
	else if (address == Address::APU::WaveChannel::NRx0)
	{
		_memory[address] = data | 0b01111111;
	}

	// bits 0-4, 7 unused
	else if (address == Address::APU::WaveChannel::NRx2)
	{
		_memory[address] = data | 0b10011111;
	}

	// bits 6-7 unused
	else if (address == Address::APU::NoiseChannel::NRx1)
	{
		_memory[address] = data | 0b11000000;
	}

	// bit 7 unused
	else if (address == Address::APU::SquareChannel1::NRx0)
	{
		_memory[address] = data | 0b10000000;
	}
	
	// bit 0-5 unused
	else if (address == Address::APU::NoiseChannel::NRx4)
	{
		_memory[address] = data | 0b00111111;
	}
	
	// bit 5-7 unused
	else if (address == 0xff26)
	{
		_memory[address] = data | 0b11110000;
	}

	// bits 5-7 unused
	else if (address == Address::Int::REQUEST_REGISTER)
	{
		_memory[address] = data | 0b11100000;
	}
	else if (address == Address::Int::ENABLED_REGISTER)
	{
		 // if data is zero, IE is zero according to mooneye's test
		_memory[address] = !data ? 0 : data | 0b11100000;

	}
	// address 0xFF03 used for the internal counter, originaly unmapped
	else if (address == Address::Timer::INTERNAL_COUNTER)
	{
	}

	else
	{
		return false;
	}

	return true;

}

/* -- checks if the given address is unmapped registers -- */ 
bool Memory::isUnmappedAddress(const u16 address)
{
	switch (address)
	{
	case Address::Timer::INTERNAL_COUNTER:
	case 0xFF08: case 0xFF09: case 0xFF0A:
	case 0xFF0B: case 0xFF0C: case 0xFF0D:
	case 0xFF0E: case 0xFF15: case 0xFF1F: 
	case 0xFF27: case 0xFF28: case 0xFF29:
		return true;
	}
	
	return address >= 0xFF4C && address <= 0xFF7F;
}


/* -- do OAM bug memory corruption if the given address is in a valid range -- */
void Memory::doOAMBug(const u16 address, const OAM_BUG_TYPE type)
{
	
	static const std::unordered_map<OAM_BUG_TYPE, std::function<void(Memory*)>> HANDLERS {
		{ OAM_BUG_TYPE::READ,				 &Memory::OAMBugRead			   },
		{ OAM_BUG_TYPE::WRITE,				 &Memory::OAMBugWrite			   },
		{ OAM_BUG_TYPE::READ_DURING_INC_DEC, &Memory::OAMBugReadDuringIncOrDec },
	};
	
	// OAM bug is happening only in a specific range
	if (address >= OAM_BUG_START && address <= OAM_BUG_END)
		HANDLERS.at(type)(this);

}


void Memory::OAMBugRead()
{

	const u8 currentRow = GPU::getCurrentRow();

	// the corruption will not happend if the accessed row is the first row
	if (currentRow == 0xff || currentRow == 0) return;

	const u16 currentRowAddress = OAM_BUG_START + currentRow * 8;
	const u16 precedingRowAddress = currentRowAddress - 8;


	// first word of the current row
	const u16 a = rawRead16(currentRowAddress);
	// first word of the preceding row
	const u16 b = rawRead16(precedingRowAddress);
	// third word of the preceding row
	const u16 c = rawRead16(precedingRowAddress + 4);

	// update first word in the current row
	rawWrite16(currentRowAddress, (b | (a & c)));
	rawWrite16(precedingRowAddress, (b | (a & c)));

	// copying last three word from preceding row to last three words in the current row
	for (u8 i = 2; i < 8; ++i)
		_memory[currentRowAddress + i] = _memory[precedingRowAddress + i];

}

void Memory::OAMBugWrite()
{

	const u8 currentRow = GPU::getCurrentRow();

	// the corruption will not happend if the accessed row is the first row
	if (currentRow == 0xff || currentRow == 0) return;

	const u16 currentRowAddress = OAM_BUG_START + currentRow * 8;
	const u16 precedingRowAddress = currentRowAddress - 8;


	// first word of the current row
	const u16 a = rawRead16(currentRowAddress);
	// first word of the preceding row
	const u16 b = rawRead16(precedingRowAddress);
	// third word of the preceding row
	const u16 c = rawRead16(precedingRowAddress + 4);

	// update first word in the current row
	rawWrite16(currentRowAddress, (((a ^ c) & (b ^ c)) ^ c));

	// copying last three word from preceding row to last three words in the current row
	for (u8 i = 2; i < 8; ++i)
		_memory[currentRowAddress + i] = _memory[precedingRowAddress + i];

}


/* -- do special case OAM bug when there's read during Increase/Decrease memory corruption -- */
void Memory::OAMBugReadDuringIncOrDec()
{
	const u8 currentRow = GPU::getCurrentRow();

	// the corruption will not happend if the accessed row is the first four or the last row
	if (currentRow == 0xff || currentRow < 4 || currentRow >= 19) return;

	const u16 currentRowAddress = OAM_BUG_START + currentRow * 8;
	const u16 precedingRowAddress = currentRowAddress - 8;
	const u16 twoRowsBeforeAddress = precedingRowAddress - 8;

	// first word two rows before the current row
	const u16 a = rawRead16(twoRowsBeforeAddress);
	// first word of the preceding row
	const u16 b = rawRead16(precedingRowAddress);
	// first word of the current row
	const u16 c = rawRead16(currentRowAddress);
	// third word of the preceding row
	const u16 d = rawRead16(precedingRowAddress + 4);

	// update first word in the preceding row
	rawWrite16(precedingRowAddress, (b & (a | c | d)) | (a & c & d));

	// preceding row is copied to the row before it and to the current row
	for (u8 i = 0; i < 8; ++i)
		_memory[currentRowAddress + i] = _memory[twoRowsBeforeAddress + i] = _memory[precedingRowAddress + i];

}

std::ostream& operator<<(std::ostream& os, const Memory& memory)
{
	os << memory._cartridge << memory._MBC;

	for (const auto& val : memory._memory)
		os.put(val);
	
	return os;
}

std::istream& operator>>(std::istream& is, Memory& memory)
{
	is >> memory._cartridge >> memory._MBC;

	for (auto& val : memory._memory)
		val = is.get();
	
	return is;
}
