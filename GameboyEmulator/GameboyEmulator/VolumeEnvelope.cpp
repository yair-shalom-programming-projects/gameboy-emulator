#include "VolumeEnvelope.h"


VolumeEnvelope::VolumeEnvelope(u8 period, u8 startVol, bool amplify)
{
    setVolumeEnvelope(period, startVol, amplify);
}

void VolumeEnvelope::setVolumeEnvelope(u8 period, u8 startVol, bool amplify)
{
    _period = period;
    _volume = startVol;
    _amplify = amplify;
}

void VolumeEnvelope::clock()
{
    // if the env is disabled do nothing
    if (_disable_env) {
        return;
    }
    if (!_period) {
        return; // if envelope period is zero then do nothing
    }
    --_periodTimer;

    if (_periodTimer) {
        return; // if it is not the time to change volume then continue
    }

    _periodTimer = _period;
        
    const u8 modifier = _amplify ? +1 : -1;
    const u8 new_volume = _volume + modifier;

    if (new_volume >= 0 && new_volume <= 15)
    {
        _volume = new_volume;// if the new volume is in valid range then update it 
    }
    else {
        _disable_env = true;// if the new volume is not in valid range then disable env
    }
}

void VolumeEnvelope::triggerEvent( const u8 vol, const u8 frameSequencer)
{
    _disable_env = false;// on trigger the disable env flag is false
    _volume = vol; // the current volume is reloded from the memory
    _periodTimer = _period; // timer is reloded
    // if the next frame sequence clocks the vol, then
    // the timer is reloaded + 1.
    if (isNextStepClocksVol(frameSequencer)) {
        ++_periodTimer;
    }
}

u8 VolumeEnvelope::getVolume() const
{
    return _volume;
}

bool VolumeEnvelope::isNextStepClocksVol(const u8 frameSequencer) const
{
    return frameSequencer == Const::APU::ENVELOP_STEP_NUMBER;
}

std::istream& operator>>(std::istream& is, VolumeEnvelope& volumeEnvelope)
{
    volumeEnvelope._volume = is.get();
    volumeEnvelope._period = is.get();
    volumeEnvelope._periodTimer = is.get();

    return is >> volumeEnvelope._amplify 
              >> volumeEnvelope._disable_env;
}

std::ostream& operator<<(std::ostream& os, const VolumeEnvelope& volumeEnvelope)
{
    os.put(volumeEnvelope._volume);
    os.put(volumeEnvelope._period);
    os.put(volumeEnvelope._periodTimer);

    return os << volumeEnvelope._amplify << '\n'
              << volumeEnvelope._disable_env;
}
