#pragma once
#include "Memory.h"
#include "Register.h"
#include <unordered_map>
#include <functional>
#include "Interrupts.h"
	
class Interrupts;

class CPU final : public Singleton<CPU>
{

public:

	// Constructors
	explicit CPU(Singleton_t);

	// Methods
	u8 execute();
	void status();
	void reset();

	//  stack related methods
	void pushStack(const u16 value);
	void pushStack(const u8 value);
	u16 popStack();
	u8 popStackHigh();
	u8 popStackLow();

	// Getters 
	u8   getCycles() const noexcept { return _cycles; }
	u16  getPC()	 const noexcept { return _PC;     }
	u16  getSP()	 const noexcept	{ return _SP;     }
	bool isHalted()  const noexcept { return _halted; }

	// Setters
	void setPC(const u16 newPC) noexcept;
	void doHaltBug() noexcept { _doHaltBug = true; }
	void freeHalt() noexcept { _halted = false; }

	// Operator overloads
	friend std::ostream& operator<<(std::ostream& os, const CPU& _cpu);
	friend std::istream& operator>>(std::istream& is, CPU& _cpu);

	// Fields
	bool _halted;

private:


	// Private Methods
	void initInstructions() noexcept;
	void initPrefixInstructions() noexcept;

	//	common instructions methods
	void inc8(Register& reg);
	u16 inc16(Register& high, Register& low);
	u16 inc16(u16& value);
	void dec8(Register& reg);
	u16 dec16(Register& high, Register& low);
	u16 dec16(u16& value);
	void sub8(u8 value, const bool sub_carry = false);
	void add8(Register& reg, u8 value, const bool add_carry = false);
	void add16(Register& reg1, Register& reg2, const u16 value);
	void and8(const u8 value);
	void xor8(const u8 value);
	void or8(const u8 value);
	void cp8(const u8 value);

	//  common prefix instructions methods
	void rlc(Register& reg);
	void rrc(Register& reg);
	void rl(Register& reg);
	void rr(Register& reg);
	void sla(Register& reg);
	void sra(Register& reg);
	void srl(Register& reg);
	void swap(Register& reg);

	void bit(const u8 value, const Register& reg);
	void res(const u8 value, Register& reg);
	void set(const u8 value, Register& reg);

	//	HL special prefix methods
	void doHL(void(CPU::* method)(Register&));
	void bitHL(const u8 bit);
	void setHL(const u8 bit, const bool turnOn = true);


	// multi-byte opcodes methods
	void ld_rr_d16(Register& high, Register& low);
	void ld_rr_d16(u16& registers);
	void ld_d16_r(const u16 address, const Register& reg);
	void ld_r_d8(Register& reg, const u16 address, const bool isPC = false);
	void arithmic_a(void(CPU::* method)(const u8), const u16 address, const bool isPC = false);
	void jr_cc(const bool condition);
	void ret_cc(const bool condition);
	void jp_cc(const bool condition);
	void call_cc(const bool condition);
	void ld_a_hl(const bool isInc = false);
	void ld_hl_a(const bool isInc = false);
	void inc_dec_rr(Register& high, Register& low, const bool isInc = false);
	void inc_dec_rr(u16& registers, const bool isInc = false);
	void pop(Register& high, Register& low);
	void push(const u16 value);
	void rst(const u16 newPC);

	// Utils
	enum Flag : u8 { CARRY = 4, HALF_CARRY, SUBTRACT, ZERO };
	using Instructions = std::unordered_map<u8, std::function<void()>>;


	// Fields
	Memory& _memory;
	Interrupts& _interrupts;

	Instructions _instructions;
	Instructions _prefixInstructions;

	Register _A, _F;
	Register _B, _C;
	Register _D, _E;
	Register _H, _L;

	u16	_SP, _PC;

	u8 _cycles;
	u8 _currentCycle;
	u8 _instruction;

	u16 _container;    // unsigned container
	char _sContainer; // signed container

	u8 _nextPrefix;
	bool _enableIME; // for EI instruction (enables IME next cycle)

	// Fields
	bool _doHaltBug;
};

