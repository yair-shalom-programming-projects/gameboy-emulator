#include "GUI.h"
#include "Gameboy.h"
#include "Constants.h"
#include "CannotCreateWindowException.h"
#include "SDL_ttf.h"
#include <filesystem>

using namespace Const::LCD;
using namespace Const::Color;

const std::unordered_map<ColorsType, std::array<Color, 4>> GUI::_colors = {
	{ ColorsType::REGULAR,  {    WHITE,    LIGHT_GRAY,    DARK_GRAY,    BLACK }},
	{ ColorsType::ORIGINAL, { OG_WHITE, OG_LIGHT_GRAY, OG_DARK_GRAY, OG_BLACK }}
};

GUI::GUI(Singleton_t) :
	_scale(3),
	_colorsType(ColorsType::REGULAR)
{
	if (SDL_Init(SDL_INIT_VIDEO) ||
		SDL_CreateWindowAndRenderer(getWidth(), getHeight(), SDL_WINDOW_RESIZABLE, &_window, &_renderer) ||
		!(_texture = SDL_CreateTexture(_renderer, SDL_PIXELFORMAT_ARGB8888, SDL_TEXTUREACCESS_STREAMING, WIDTH, HEIGHT)) ||
		SDL_RenderSetLogicalSize(_renderer, WIDTH, HEIGHT) ||
		SDL_RenderSetScale(_renderer, _scale, _scale))
	{
		throw CannotCreateWindowException(SDL_GetError());
	}

	// enable receive winapi event throw SDL
	SDL_EventState(SDL_SYSWMEVENT, SDL_ENABLE);

	// menu should initialize after SDL init
	_menu = &Menu::getInstance(_window);

}

GUI::~GUI() noexcept
{
	SDL_DestroyRenderer(_renderer);
	SDL_DestroyWindow(_window);
	SDL_DestroyTexture(_texture);
	SDL_Quit();
}

/*
	-- render the given frame to the GUI application --
	* input: frame buffer 
*/
void GUI::renderFrame(const Frame& frame)
{

	void* pixels = nullptr;
	int pitch = 0;

	SDL_LockTexture(_texture, NULL, &pixels, &pitch);

	// insert _screen to the display 
	for (u8 y = 0; y < HEIGHT; ++y)
	{
		u32* display = (u32*)((u8*)pixels + y * pitch);

		for (const auto& colorIndex : frame[y])
			*display++ = _colors.at(_colorsType)[colorIndex];
	}

	SDL_UnlockTexture(_texture);
	SDL_RenderClear(_renderer);
	SDL_RenderCopy(_renderer, _texture, NULL, NULL);
	SDL_RenderPresent(_renderer);

}

/* -- set GPU application title to the given title -- */
void GUI::setTitle(const std::string_view title)
{
	SDL_SetWindowTitle(_window, title.data());
}

/* -- update all GUI related events -- */
void GUI::update()
{
	SDL::updateEventQueues();

	checkedForDroppedFiles();
	_menu->handlePendingEvents();
}

void GUI::setScale(const size_t& scale)
{
	_scale = scale;
	SDL_SetWindowSize(_window, getWidth(), getHeight());
}

SDL_Surface* GUI::takeScreenshot() const
{
	SDL_Surface* surface = SDL_CreateRGBSurface(0, getWidth(), getHeight(), 32, 0x00ff0000, 0x0000ff00, 0x000000ff, 0xff000000);
	SDL_RenderReadPixels(_renderer, NULL, SDL_PIXELFORMAT_ARGB8888, surface->pixels, surface->pitch);
	
	return surface;
}

void GUI::setColorsType(const ColorsType& type)
{
	ColorsType back = (type == ColorsType::ORIGINAL ? ColorsType::REGULAR : ColorsType::ORIGINAL);
	SDL_SetRenderDrawColor(_renderer, _colors.at(back)[0].r, _colors.at(back)[0].g, _colors.at(back)[0].b, 255);
	_colorsType = type;
}

/* -- checks if the user dragged & dropped a file to load -- */
void GUI::checkedForDroppedFiles()
{
	if (isThereValidDroppedFile())
		Gameboy::getInstance().loadGame(SDL::fileDropped);

	SDL::fileDropped.clear();
}

std::string GUI::getGameFileFromUser()
{

	while (!isThereValidDroppedFile())
	{
		SDL::updateEventQueues();
		SDL_Delay(100);
	}
	return SDL::fileDropped;

}


void GUI::renderHelloScreen()
{
	TTF_Init();
	TTF_Font* Sans = TTF_OpenFont("Fonts/Sans.ttf", 100);

	SDL_Surface* designersSurface = TTF_RenderText_Solid(Sans, "Yair Shalom & Dur Topolanski", OG_BLACK);
	SDL_Surface* titleSurface = TTF_RenderText_Solid(Sans, "GameBoy Emulator", OG_BLACK);
	SDL_Surface* explanationSurface = TTF_RenderText_Solid(Sans, "Drag & Drop GameBoy File!", OG_BLACK);

	SDL_SetWindowTitle(_window, "GameBoy");
	SDL_Texture* Message1 = SDL_CreateTextureFromSurface(_renderer, designersSurface);
	SDL_Texture* Message2 = SDL_CreateTextureFromSurface(_renderer, titleSurface);
	SDL_Texture* Message3 = SDL_CreateTextureFromSurface(_renderer, explanationSurface);

	SDL_Rect Message_rect1{ 0, 20, Const::LCD::WIDTH, 14 };
	SDL_Rect Message_rect2{ 22, 40, 100, 14 };
	SDL_Rect Message_rect3{ 0, 90, Const::LCD::WIDTH, 14 };

	SDL_SetRenderDrawColor(_renderer, OG_WHITE.r, OG_WHITE.g, OG_WHITE.b, 255);
	SDL_RenderClear(_renderer);

	SDL_RenderCopy(_renderer, Message1, NULL, &Message_rect1);
	SDL_RenderCopy(_renderer, Message2, NULL, &Message_rect2);
	SDL_RenderCopy(_renderer, Message3, NULL, &Message_rect3);
	SDL_FreeSurface(designersSurface);
	SDL_FreeSurface(titleSurface);
	SDL_FreeSurface(explanationSurface);
	SDL_DestroyTexture(Message1);
	SDL_DestroyTexture(Message2);
	SDL_DestroyTexture(Message3);
	SDL_RenderPresent(_renderer);
	TTF_Quit();
}

bool GUI::isThereValidDroppedFile() const
{
	return !SDL::fileDropped.empty() && SDL::fileDropped.ends_with(Const::GAMEBOY_FILE_EXTENTION);
}
