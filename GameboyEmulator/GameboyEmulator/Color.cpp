#include "Color.h"

const bool Color::operator==(const Color& other)
{
	return r == other.r && g == other.g && other.b == other.b;
}

Color& Color::operator=(const Color& other)
{
	r = other.r;
	g = other.g;
	b = other.b;
	return *this;
}

/*
	<alpha><red><green><blue>
*/
Color::operator u32() const
{
	return (0xFF << 24) | (r << 16) | (g << 8) | (b << 0);
}

Color::operator SDL_Color() const
{
	return { r, g, b, 0xFF };
}

std::istream& operator>>(std::istream& is, Color& color)
{
	return is >> color.r >> color.g >> color.b;
}

std::ostream& operator<<(std::ostream& os, const Color& color)
{
	return os << color.r << color.g << color.b;
}
