#include "Menu.h"
#include "CannotCreateMenuException.h"
#include <SDL_syswm.h>
#include <iostream>
#include "Gameboy.h"

using std::bind;
using SDL::menubarEvents;
using std::placeholders::_1;

static Gameboy* gameboy = nullptr;

const Menu::MenuHandlers Menu::OPTION_HANDLERS = {
	&Menu::handleSaveState, &Menu::handleLoadState,
	bind(&Menu::handleChangeScale, _1, 1), bind(&Menu::handleChangeScale, _1, 2),
	bind(&Menu::handleChangeScale, _1, 3), bind(&Menu::handleChangeScale, _1, 4),
	&Menu::handlePause,	   &Menu::handleReset,
	&Menu::handleLoadGame, &Menu::handleScreenShot,
	bind(&Menu::handleChangeColorsType, _1, ColorsType::REGULAR, Option::REGULAR_COLORS),
	bind(&Menu::handleChangeColorsType, _1, ColorsType::ORIGINAL, Option::ORIGINAL_COLORS)
};

const char* Menu::STATE_SIGNATURE = "YAIRSHALOM_DURTOPOLANSKI_2022_GAMEBOY_EMULATOR";


Menu::Menu(Singleton_t, SDL_Window* window) :
	_window(window)
{
	construct();
	initWindow();
	addToWindow();
}


void Menu::construct()
{
	auto stateMenu = CreatePopupMenu();
	AppendMenu(stateMenu, MF_STRING, Option::SAVE_STATE, L"&Save State");
	AppendMenu(stateMenu, MF_STRING, Option::LOAD_STATE, L"&Load State");

	auto scaleMenu = CreatePopupMenu();
	AppendMenu(scaleMenu, MF_STRING, Option::SCALE_1X1, L"&1x1");
	AppendMenu(scaleMenu, MF_STRING, Option::SCALE_2X2, L"&2x2");
	AppendMenu(scaleMenu, MF_STRING, Option::SCALE_3X3, L"&3x3");
	AppendMenu(scaleMenu, MF_STRING, Option::SCALE_4X4, L"&4x4");

	auto optionsMenu = CreatePopupMenu();
	AppendMenu(optionsMenu, MF_STRING, Option::PAUSE,	   L"&Pause");
	AppendMenu(optionsMenu, MF_STRING, Option::RESET,	   L"&Reset");
	AppendMenu(optionsMenu, MF_STRING, Option::LOAD_GAME,  L"&Load Game");
	AppendMenu(optionsMenu, MF_STRING, Option::SCREENSHOT, L"&Screenshot");

	auto colorsTypeMenu = CreatePopupMenu();
	AppendMenu(colorsTypeMenu, MF_STRING, Option::REGULAR_COLORS,  L"&Regular");
	AppendMenu(colorsTypeMenu, MF_STRING, Option::ORIGINAL_COLORS, L"&Original");

	auto screenMenu = CreatePopupMenu();
	AppendMenu(screenMenu, MF_POPUP, (UINT_PTR)colorsTypeMenu, L"&Colors Type");

	_menu = CreateMenu();
	AppendMenu(_menu, MF_POPUP, (UINT_PTR)stateMenu,   L"&State");
	AppendMenu(_menu, MF_POPUP, (UINT_PTR)scaleMenu,   L"&Scale");
	AppendMenu(_menu, MF_POPUP, (UINT_PTR)optionsMenu, L"&Options");
	AppendMenu(_menu, MF_POPUP, (UINT_PTR)screenMenu,  L"&Screen");

	CheckMenuRadioItem(_menu, Option::REGULAR_COLORS, Option::ORIGINAL_COLORS, Option::REGULAR_COLORS, 0);
}

void Menu::addToWindow()
{
	if (!SetMenu(_winapiWindow, _menu))
		throw CannotCreateMenuException(GetLastError());
}

void Menu::initWindow()
{
	SDL_SysWMinfo info{ 0 };

	if (!SDL_GetWindowWMInfo(_window, &info))
		throw CannotCreateMenuException(GetLastError());

	_winapiWindow = info.info.win.window;
}


//? Handlers ?//


void Menu::handlePendingEvents()
{
	gameboy = &Gameboy::getInstance();

	while (menubarEvents.size())
	{
		if (size_t option = LOWORD(menubarEvents.front().syswm.msg->msg.win.wParam); option < OPTION_HANDLERS.size())
			OPTION_HANDLERS[option](this);

		menubarEvents.pop();
	}
}

bool Menu::isStateFileSignatureValid(std::ifstream& file)
{
	std::string fileSigniture;
	std::getline(file, fileSigniture);
	
	return fileSigniture == STATE_SIGNATURE;
}

void Menu::writeStateFileSignature(std::ofstream& file)
{
	file << STATE_SIGNATURE << '\n';
}

void Menu::handleSaveState()
{
	std::wstring filePath(MAX_PATH, '\0');

	OPENFILENAMEW structure
	{
		sizeof(OPENFILENAMEW), _winapiWindow, nullptr,
		L"YDGB Files\0 *.ydgb\0\0", nullptr, NULL,
		1, & filePath[0], filePath.size(), nullptr, NULL,
		nullptr, nullptr, NULL, NULL, NULL, L"ydgb", NULL,
		NULL, nullptr 
	};

	if (!GetSaveFileName(&structure)) return;

	std::ofstream file(filePath, std::ios::binary);

	if (!file.is_open()) return;

	writeStateFileSignature(file);
	file << *gameboy;

}

void Menu::handleLoadState()
{
	std::wstring filePath(MAX_PATH, '\0');

	OPENFILENAMEW structure
	{
		sizeof(OPENFILENAMEW), _winapiWindow, nullptr,
		L"YDGB Files\0 *.ydgb\0\0", nullptr, NULL,
		1, & filePath[0], filePath.size(), nullptr, NULL,
		nullptr, nullptr, NULL, NULL, NULL, L"ydgb", NULL,
		NULL, nullptr
	};

	if (!GetOpenFileName(&structure)) return;

	std::ifstream file(filePath, std::ios::binary);
	
	if (!file.is_open()) 
		return;

	if (!isStateFileSignatureValid(file))
	{
		std::cout << "Invalid state file entered!" << std::endl;
		return;
	}

	file >> *gameboy;

}
void Menu::handleChangeScale(const size_t& scale)
{
	GUI::getInstance().setScale(scale);
}

void Menu::handleLoadGame()
{
	std::string filePath(MAX_PATH, '\0');

	OPENFILENAMEA structure
	{
		sizeof(OPENFILENAMEA), _winapiWindow, nullptr,
		"GameBoy Files\0 *.gb\0\0", nullptr, NULL,
		1, &filePath[0], filePath.size(), nullptr, NULL,
		nullptr, nullptr, NULL, NULL, NULL, "gb", NULL,
		NULL, nullptr
	};

	if (!GetOpenFileNameA(&structure)) return;

	gameboy->loadGame(filePath);
}

void Menu::handlePause()
{
	SDL::waitForInput();
}

void Menu::handleReset()
{
	gameboy->reset();
}

void Menu::handleScreenShot()
{
	SDL_Surface* surface = GUI::getInstance().takeScreenshot();

	std::string file_path(MAX_PATH, '\0');
	
	OPENFILENAMEA structure
	{
		sizeof(OPENFILENAMEA), _winapiWindow, nullptr,
		"Image Files\0 *.bmp\0\0", nullptr, NULL,
		1, &file_path[0], file_path.size(), nullptr, NULL,
		nullptr, nullptr, NULL, NULL, NULL, "bpm", NULL,
		NULL, nullptr
	};

	if (!GetSaveFileNameA(&structure)) return;

	SDL_SaveBMP(surface, file_path.c_str());
	SDL_FreeSurface(surface);
}

void Menu::handleChangeColorsType(const ColorsType& type, const Option& checkOptionId)
{
	CheckMenuRadioItem(_menu, Option::REGULAR_COLORS, Option::ORIGINAL_COLORS, checkOptionId, 0);
	GUI::getInstance().setColorsType(type);
}
