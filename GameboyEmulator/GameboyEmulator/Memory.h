#pragma once
#include <array>
#include <string_view>
#include <memory>
#include "IMBC.h"
#include "Singleton.h"
#include "Constants.h"

/// <summary>
/// 
/// Memory Map:
///	
/// 	0x0000 - 0x3FFF ROM Bank 0 (readonly)		  Note - when reading/writing to this area, it happends from/to _cartridge instead of copying it to memory
///		0x4000 - 0x7FFF ROM Bank n (switchable bank)  Note - when reading/writing to this area, it happends from/to _cartridge instead of copying it to memory
///		0x8000 - 0x9FFF Video RAM
///		0xA000 - 0xBFFF RAM Bank n (switchable bank)
///		0xC000 - 0xCFFF Work RAM Bank 0 
///		0xD000 - 0xDFFF Work RAM Bank 1 
///		0xE000 - 0xFDFF Echo of 0xC000 - 0xDDFF
///		0xFE00 - 0xFE9F Sprite Attribute Table
///		0xFEA0 - 0xFEFF n/a
///		0xFF00 - 0xFF7F I/O Ports
///		0xFF80 - 0xFFFE High RAM (HRAM)
///		0xFFFF Interrupt Enable Register
/// 
/// </summary>

enum class OAM_BUG_TYPE : u8 { READ, WRITE, READ_DURING_INC_DEC };

class Memory final : public Singleton<Memory>
{

public:
	
	// Constructors
	explicit Memory(Singleton_t);

	// Methods
	void write(const u16 address, const u8 data);
	void write16(const u16 address, const u16 data);
	void rawWrite16(const u16 address, const u16 data);

	u8 read(const u16 address, const bool isIncOrDec = false);
	u16 read16(const u16 address, const bool isIncOrDec = false);
	u16 rawRead16(const u16 address) const;

	void reset();
	void loadCartridge(const std::string_view filePath);

	// OAM Bug methods
	void doOAMBug(const u16 address, const OAM_BUG_TYPE type);

	// Operator Overloads
	u8& operator[](const u16 address) noexcept { return _memory[address]; };
	u8 operator[](const u16 address) const noexcept { return _memory[address]; };

	friend std::ostream& operator<<(std::ostream& os, const Memory& memory);
	friend std::istream& operator>>(std::istream& is, Memory& memory);


private:

	// Methods
	void resetMemory() noexcept;
	void resetWaveRam() noexcept;
	void DMATransfer(const u8 data);
	
	bool writeToRegistersWithUnusedBits(const u16 address, const u8 data);
	bool isUnmappedAddress(const u16 address);

	// OAM Bug methods
	void OAMBugRead();
	void OAMBugWrite();
	void OAMBugReadDuringIncOrDec();

	// Fields
	Cartridge& _cartridge;

	std::array<u8, Const::Memory::SIZE> _memory;
	std::unique_ptr<IMBC> _MBC;
};

