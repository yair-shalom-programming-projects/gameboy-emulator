#include "Gameboy.h"
#include <SDL_main.h>
#include <iostream>

using std::cout, std::endl;

int main(int argc, char** args) 
{

	cout <<
		"\n------- GameBoy Emulator Project -------\n" <<
		"      Yair Shalom & Dur Topolanski \n" << endl;
		
	try
	{
		Gameboy::getInstance().emulate();
	}
	catch (const std::exception& e)
	{
		cout << "$ " << e.what() << endl;
	}

	return 0;

}


