#include "Interrupts.h"
#include "Utils.h"
#include "RequestedServeToInvalidInterruptException.h"
#include <iostream>

using namespace Address::Int;
using namespace Utils;


Interrupt& operator++(Interrupt& i)
{
    return (i = Interrupt{ static_cast<u8>(i) + 1 });
}


bool Interrupts::IME = false; 


/* -- C'tor -- */
Interrupts::Interrupts(Singleton_t, CPU& CPU) :
    _memory(Memory::getInstance()),
    _cpu(CPU)
{
    reset();
}

/*
    -- checks for new Interrupts request and serve them in
       priority order --
*/
void Interrupts::doInterrupts()
{

    const u8 requests = _memory.read(REQUEST_REGISTER);
    const u8 enabled  = _memory.read(ENABLED_REGISTER);


    // if halted mode just entered or exited, update behaviour
    if (_wasHalted != _cpu.isHalted())
        setBehaviour(requests, enabled);
    
    _wasHalted = _cpu.isHalted();

    switch (_behaviour)
    {
    
    // checks for enabled requests and serve the highest priority 
    case Behaviour::NORMAL:
    case Behaviour::HALT_IME:

        for (Interrupt i = Interrupt::VBLANK; IME && i <= Interrupt::JOYPAD; ++i)
        {
            if (isBitOn(requests, i) && isBitOn(enabled, i))
            {
                IME = false;
                _cpu.freeHalt();
                _servedInterrupt = i;
            }
        }
        break;
        
    // halt mode not entered and halt bug occurs
    case Behaviour::HALT_NOT_IME_ENABLED_REQUESTS:
        _cpu.doHaltBug();
        _cpu.freeHalt();
        break;

    // halt mode entered until requested interrupt enabled (there's no serve)
    case Behaviour::HALT_NOT_IME_NOT_ENABLED_REQUESTS:
        if (requests & enabled & 0b11111)
            _cpu.freeHalt();
        break;
    }

}

void Interrupts::setBehaviour(const u8 requests, const u8 enabled)
{
 
    _behaviour = _cpu.isHalted() ? IME ? Behaviour::HALT_IME :
                (requests & enabled & 0b11111) ? 
                Behaviour::HALT_NOT_IME_ENABLED_REQUESTS :
                Behaviour::HALT_NOT_IME_NOT_ENABLED_REQUESTS :
                Behaviour::NORMAL;

}


/*
    -- 
    request interupt for the given interupt id by setting the id's bit
    in request register address 
    --
    * input: interupt's id (bit)
*/
void Interrupts::request(const Interrupt& id)
{
    _memory.write(REQUEST_REGISTER, setBit(_memory.read(REQUEST_REGISTER), id));
}

/*
    -- serving an interrupt --
    The serve contains three steps.
    1. reset corrently served interrupt id bit in REQUEST_REGISTER (2 M-cycles)
    2. push old _PC to Stack (3 M-cycles) 
    * output: number of clocks the current cycle(s) took
*/
u8 Interrupts::serve()
{
    static const u8 CYCLES = 4;
    static u8 requestRegister = 0;

    if (_servedInterrupt == Interrupt::NONE)
        throw RequestedServeToInvalidInterruptException();

    switch (_currentCycle)
    {
    case 1:
        requestRegister = _memory.read(REQUEST_REGISTER);
        break;

    case 2:
        _memory.write(REQUEST_REGISTER, resetBit(requestRegister, _servedInterrupt));
        break;

    case 3:
        _memory.doOAMBug(_cpu.getSP(), OAM_BUG_TYPE::WRITE);
        break;

    case 4:
        _cpu.pushStack(static_cast<u8>(_cpu.getPC() >> 8));
        break;
    
    case 5:
        _cpu.pushStack(static_cast<u8>(_cpu.getPC()));
        _cpu.setPC(ADDRESSES.at(_servedInterrupt));

        _currentCycle = 0;
        _servedInterrupt = Interrupt::NONE;
        break;
    }
    
    ++_currentCycle;
    return CYCLES;
}

void Interrupts::reset()
{
    IME = false;

    _servedInterrupt = Interrupt::NONE;
    _behaviour = Behaviour::NORMAL;

    _currentCycle = 1;
    _wasHalted = false;
}

std::ostream& operator<<(std::ostream& os, const Interrupts& interrupts)
{
    os.put(interrupts._currentCycle);
    os.put(static_cast<char>(interrupts._servedInterrupt));
    os.put(static_cast<u8>(interrupts._behaviour));
    
    return os << interrupts.IME << '\n'
              << interrupts._wasHalted << '\n';
}

std::istream& operator>>(std::istream& is, Interrupts& interrupts)
{
    interrupts._currentCycle = is.get();
    interrupts._servedInterrupt = static_cast<Interrupt>(is.get());
    interrupts._behaviour = static_cast<Interrupts::Behaviour>(is.get());

    return is >> interrupts.IME
              >> interrupts._wasHalted;
}