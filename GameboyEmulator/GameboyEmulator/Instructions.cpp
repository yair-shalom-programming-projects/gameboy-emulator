#include "CPU.h"
#include "InvalidInstructionException.h"
#include "Address.h"
#include <iostream>
#include "GPU.h"

using namespace Utils;

/* -- Initialize the _instructions field with ALL CPU instruction set -- */
void CPU::initInstructions() noexcept
{

	_instructions = { {

		{ 0x00, [&]() {

			/*
				NOP
				1  4
				- - - -
			*/
			_cycles = 4;
		}},

		{ 0x01, [&]() {

			/*
				LD BC, d16
				3  12
				- - - -
			*/
			ld_rr_d16(_B, _C);

		}},

		{ 0x02, [&]() {

			/*
				LD (BC), A
				1  8
				- - - -
			*/
			ld_d16_r(_B.join(_C), _A);
		
		}},

		{ 0x03, [&]() {

			/*
				INC BC
				1  8
				- - - -
			*/
			inc_dec_rr(_B, _C, true);
			
		}},

		{ 0x04, [&]() {

			/*
				INC B
				1  4
				Z 0 H -
			*/
			inc8(_B);

		}},

		{ 0x05, [&]() {

			/*
				DEC B
				1  4
				Z 1 H -
			*/
			dec8(_B);

		}},

		{ 0x06, [&]() {

			/*
				LD B, d8
				2  8
				- - - -
			*/
			ld_r_d8(_B, _PC, true);

		}},

		{ 0x07, [&]() {

			/*
				RLCA
				1  4
				0 0 0 C
			*/
			_F[Flag::CARRY] = _A[7];

			_A = (_A << 1) | _A[7];

			_F[Flag::ZERO] = 0;
			_F[Flag::HALF_CARRY] = 0;
			_F[Flag::SUBTRACT] = 0;

			_cycles = 4;
		}},

		{ 0x08, [&]() {

			/*
				LD (a16), SP
				3  20
				- - - -
			*/

			switch (_currentCycle)
			{
			case 1:
				// read Instruction
				break;

			case 2:
				_container = _memory.read(_PC, true);
				break;

			case 3:
				_container = join(_memory.read(_PC + 1, true), _container);
				break;

			case 4:
				_memory.write(_container, _SP);
				break;

			case 5:
				_memory.write(_container + 1, _SP >> 8);
				_PC += 2;
				break;
			}

			_cycles = 4;
			_currentCycle = _currentCycle == 5 ? 1 : _currentCycle + 1;

		}},

		{ 0x09, [&]() {

			/*
				ADD HL, BC
				1  8
				- 0 H C
			*/
			add16(_H, _L, _B.join(_C));

		}},

		{ 0x0A, [&]() {

			/*
				LD A, (BC)
				1  8
				- - - -
			*/
			ld_r_d8(_A, _B.join(_C));
		}},

		{ 0x0B, [&]() {

			/*
				DEC BC
				1  8
				- - - -
			*/
			inc_dec_rr(_B, _C);

		}},

		{ 0x0C, [&]() {

			/*
				INC C
				1  4
				Z 0 H -
			*/
			inc8(_C);

		}},

		{ 0x0D, [&]() {

			/*
				DEC C
				1  4
				Z 1 H -
			*/
			dec8(_C);
			
		}},

		{ 0x0E, [&]() {

			/*
				LD C, d8
				2  8
				- - - -
			*/
			ld_r_d8(_C, _PC, true);

		}},

		{ 0x0F, [&]() {

			/*
				RRCA
				1  4
				0 0 0 C
			*/
			_F[Flag::CARRY] = _A[0];
			_A = (_A[0] << 7) | (_A >> 1);


			_F[Flag::ZERO] = 0;
			_F[Flag::HALF_CARRY] = 0;
			_F[Flag::SUBTRACT] = 0;

			
			_cycles = 4;
		}},

		{ 0x10, [&]() {

			/*
				STOP d8
				2  4
				- - - -
			*/
			// this is intended to switch the gameboy into 
			// VERY low power standby mode			
			++_PC;
			_cycles = 4;
		}},

		{ 0x11, [&]() {

			/*
				LD DE, d16
				3  12
				- - - -
			*/
			ld_rr_d16(_D, _E);

		}},

		{ 0x12, [&]() {

			/*
				LD (DE), A
				1  8
				- - - -
			*/
			ld_d16_r(_D.join(_E), _A);
		
		}},

		{ 0x13, [&]() {

			/*
				INC DE
				1  8
				- - - -
			*/
			inc_dec_rr(_D, _E, true);

		}},

		{ 0x14, [&]() {

			/*
				INC D
				1  4
				Z 0 H -
			*/

			inc8(_D);

		}},

		{ 0x15, [&]() {

			/*
				DEC D
				1  4
				Z 1 H -
			*/
			dec8(_D);
			
		}},

		{ 0x16, [&]() {

			/*
				LD D, d8
				2  8
				- - - -
			*/
			ld_r_d8(_D, _PC, true);
		
		}},

		{ 0x17, [&]() {

			/*
				RLA
				1  4
				0 0 0 C
			*/

			const bool A7 = _A[7];
			_A = (_A << 1) | _F[Flag::CARRY];

			_F[Flag::CARRY] = A7;
			_F[Flag::ZERO] = 0;
			_F[Flag::HALF_CARRY] = 0;
			_F[Flag::SUBTRACT] = 0;

			_cycles = 4;
		}},

		{ 0x18, [&]() {

			/*
				JR r8
				2  12
				- - - -
			*/
			jr_cc(true);

		}},

		{ 0x19, [&]() {

			/*
				ADD HL, DE
				1  8
				- 0 H C
			*/
			add16(_H, _L, _D.join(_E));
			
		}},

		{ 0x1A, [&]() {

			/*
				LD A, (DE)
				1  8
				- - - -
			*/
			ld_r_d8(_A, _D.join(_E));

		}},

		{ 0x1B, [&]() {

			/*
				DEC DE
				1  8
				- - - -
			*/
			inc_dec_rr(_D, _E);
			
		}},

		{ 0x1C, [&]() {

			/*
				INC E
				1  4
				Z 0 H -
			*/
			inc8(_E);

		}},

		{ 0x1D, [&]() {

			/*
				DEC E
				1  4
				Z 1 H -
			*/
			dec8(_E);
			
		}},

		{ 0x1E, [&]() {

			/*
				LD E, d8
				2  8
				- - - -
			*/
			ld_r_d8(_E, _PC, true);

		}},

		{ 0x1F, [&]() {

			/*
				RRA
				1  4
				0 0 0 C
			*/
			const bool A0 = _A[0];
			
			_A >>= 1;
			_A[7] = _F[Flag::CARRY];

			_F[Flag::ZERO] = 0;
			_F[Flag::SUBTRACT] = 0;
			_F[Flag::HALF_CARRY] = 0;
			_F[Flag::CARRY] = A0;

			_cycles = 4;
		}},

		{ 0x20, [&]() {

			/*
				JR NZ, r8
				2  12/8
				- - - -
			*/
			jr_cc(!_F[Flag::ZERO]);

		}},

		{ 0x21, [&]() {

			/*
				LD HL, d16
				3  12
				- - - -
			*/
			ld_rr_d16(_H, _L);

		}},

		{ 0x22, [&]() {
			/*
				LD (HL+), A
				1  8
				- - - -
			*/
			ld_hl_a(true);
		}},

		{ 0x23, [&]() {

			/*
				INC HL
				1  8
				- - - -
			*/
			inc_dec_rr(_H, _L, true);

		}},

		{ 0x24, [&]() {

			/*
				INC H
				1  4
				Z 0 H -
			*/
			inc8(_H);

		}},

		{ 0x25, [&]() {

			/*
				DEC H
				1  4
				Z 1 H -
			*/
			dec8(_H);

		}},

		{ 0x26, [&]() {

			/*
				LD H, d8
				2  8
				- - - -
			*/
			ld_r_d8(_H, _PC, true);
		
		}},

		{ 0x27, [&]() {

			/*
				DAA
				1  4
				Z - 0 C
				* explanation https://ehaskins.com/2018-01-30%20Z80%20DAA/
			*/

			u16 result = _A;

			if (_F[Flag::SUBTRACT])
			{
				if (_F[Flag::HALF_CARRY])
				{
					result -= 0x6;
				}
				if (_F[Flag::CARRY])
				{
					result -= 0x60;
				}
			}
			else
			{
				if (_F[Flag::HALF_CARRY] || (_A & 0xF) > 0x9)
				{
					result += 0x6;
				}
				if (_F[Flag::CARRY] || result > 0x9F)
				{
					result += 0x60;
					_F[Flag::CARRY] = 1;
				}
			}

			_A = static_cast<u8>(result);

			_F[Flag::ZERO] = !_A;
			_F[Flag::HALF_CARRY] = 0;

			
			_cycles = 4;

		}},

		{ 0x28, [&]() {

			/*
				JR Z, r8
				2  12/8
				- - - -
			*/
			jr_cc(_F[Flag::ZERO]);

		}},

		{ 0x29, [&]() {

			/*
				ADD HL, HL
				1  8
				- 0 H C
			*/
			add16(_H, _L, _H.join(_L));
			
		}},
	
		{ 0x2A, [&]() {

			/*
				LD A, (HL+)
				1  8
				- - - -
			*/
			ld_a_hl(true);

		}},

		{ 0x2B, [&]() {

			/*
				DEC HL
				1  8
				- - - -
			*/
			inc_dec_rr(_H, _L);
			
		}},

		{ 0x2C, [&]() {

			/*
				INC L
				1  4
				Z 0 H -
			*/
			inc8(_L);
			
		}},

		{ 0x2D, [&]() {

			/*
				DEC L
				1  4
				Z 1 H -
			*/
			dec8(_L);

		}},

		{ 0x2E, [&]() {

			/*
				LD L, d8
				2  8
				- - - -
			*/
			ld_r_d8(_L, _PC, true);

		}},


		{ 0x2F, [&]() {

			/*
				CPL
				1  4
				- 1 1 -
			*/

			_A = ~_A;

			_F[Flag::SUBTRACT] = 1;
			_F[Flag::HALF_CARRY] = 1;

			_cycles = 4;
		}},

		{ 0x30, [&]() {

			/*
				JR NC, r8
				2  12/8
				- - - -
			*/
			jr_cc(!_F[Flag::CARRY]);

		}},

		{ 0x31, [&]() {

			/*
				LD SP, d16
				3  12
				- - - -
			*/
			ld_rr_d16(_SP);
			
		}},

		{ 0x32, [&]() {
			/*
				LD (HL-), A
				1  8
				- - - -
			*/
			ld_hl_a();

		}},

		{ 0x33, [&]() {

			/*
				INC SP
				1  8
				- - - -
			*/
			inc_dec_rr(_SP, true);

		}},

		{ 0x34, [&]() {

			/*
				INC (HL)
				1  12
				Z 0 H -
			*/
			doHL(&CPU::inc8);

		}},

		{ 0x35, [&]() {

			/*
				DEC (HL)
				1  12
				Z 1 H -
			*/
			doHL(&CPU::dec8);

		}},

		{ 0x36, [&]() {

			/*
				LD (HL), d8
				2  12
				- - - -
			*/
			
			switch (_currentCycle)
			{
			case 1:
				// read Instruction
				break;

			case 2:
				_container = _memory.read(_PC, true);
				break;

			case 3:
				_memory.write(_H.join(_L), _container);
				++_PC;
				break;
			}

			_cycles = 4;
			_currentCycle = _currentCycle == 3 ? 1 : _currentCycle + 1;
		}},

		{ 0x37, [&]() {

			/*
				SCF
				1  4
				- 0 0 1
			*/

			_F[Flag::SUBTRACT] = 0;
			_F[Flag::HALF_CARRY] = 0;
			_F[Flag::CARRY] = 1;
			
			_cycles = 4;
		}},

		{ 0x38, [&]() {

			/*
				JR C, r8
				2  12/8
				- - - -
			*/
			jr_cc(_F[Flag::CARRY]);

		}},

		{ 0x39, [&]() {

			/*
				ADD HL, SP
				1  8
				- 0 H C
			*/
			add16(_H, _L, _SP);
			
		}},

		{ 0x3A, [&]() {
			/*
				LD A, (HL-)
				1  8
				- - - -
			*/
			ld_a_hl();

		}},

		{ 0x3B, [&]() {

			/*
				DEC SP
				1  8
				- - - -
			*/
			inc_dec_rr(_SP);
			
		}},

		{ 0x3C, [&]() {

			/*
				INC A
				1  4
				Z 0 H -
			*/
			inc8(_A);

		}},

		{ 0x3D, [&]() {

			/*
				DEC A
				1  4
				Z 1 H -
			*/
			dec8(_A);

		}},

		{ 0x3E, [&]() {

			/*
				LD A, d8
				2  8
				- - - - 
			*/
			ld_r_d8(_A, _PC, true);

		}},

		{ 0x3F, [&]() {

			/*
				CCF
				1  4
				- 0 0 C
			*/
			_F[Flag::SUBTRACT] = 0;
			_F[Flag::HALF_CARRY] = 0;
			_F[Flag::CARRY] = !_F[Flag::CARRY];

			_cycles = 4;
		}},

		{ 0x40, [&]() {

			/*
				LD B, B
				1  4
				- - - -
			*/
			_B = _B;
			
			_cycles = 4;
		}},

		{ 0x41, [&]() {

			/*
				LD B, C
				1  4
				- - - -
			*/
			_B = _C;
			
			_cycles = 4;
		}},

		{ 0x42, [&]() {

			/*
				LD B, D
				1  4
				- - - -
			*/
			_B = _D;
			
			_cycles = 4;
		}},

		{ 0x43, [&]() {

			/*
				LD B, E
				1  4
				- - - -
			*/
			_B = _E;

			_cycles = 4;
		}},

		{ 0x44, [&]() {

			/*
				LD B, H
				1  4
				- - - -
			*/
			_B = _H;
			
			_cycles = 4;

		}},

		{ 0x45, [&]() {

			/*
				LD B, L
				1  4
				- - - -
			*/
			_B = _L;
			
			_cycles = 4;
		}},

		{ 0x46, [&]() {

			/*
				LD B, (HL)
				1  8
				- - - -
			*/
			ld_r_d8(_B, _H.join(_L));

		}},

		{ 0x47, [&]() {

			/*
				LD B, A
				1  4
				- - - -
			*/
			_B = _A;

			_cycles = 4;
		}},

		{ 0x48, [&]() {

			/*
				LD C, B
				1  4
				- - - -
			*/
			_C = _B;

			_cycles = 4;
		}},

		{ 0x49, [&]() {

			/*
				LD C, C
				1  4
				- - - -
			*/
			_C = _C;
			
			_cycles = 4;
		}},

		{ 0x4A, [&]() {

			/*
				LD C, D
				1  4
				- - - -
			*/
			_C = _D;

			_cycles = 4;
		}},

		{ 0x4B, [&]() {

			/*
				LD C, E
				1  4
				- - - -
			*/
			_C = _E;

			_cycles = 4;
		}},

		{ 0x4C, [&]() {

			/*
				LD C, H
				1  4
				- - - -
			*/
			_C = _H;

			_cycles = 4;
		}},

		{ 0x4D, [&]() {

			/*
				LD C, L
				1  4
				- - - -
			*/
			_C = _L;

			_cycles = 4;
		}},

		{ 0x4E, [&]() {

			/*
				LD C, (HL)
				1  8
				- - - -
			*/
			ld_r_d8(_C, _H.join(_L));

		}},

		{ 0x4F, [&]() {

			/*
				LD C, A
				1  4
				- - - -
			*/
			_C = _A;
			
			_cycles = 4;
		}},

		{ 0x50, [&]() {

			/*
				LD D, B
				1  4
				- - - -
			*/
			_D = _B;
			
			_cycles = 4;
		}},

		{ 0x51, [&]() {

			/*
				LD D, C
				1  4
				- - - -
			*/
			_D = _C;
			
			_cycles = 4;
		}},

		{ 0x52, [&]() {

			/*
				LD D, D
				1  4
				- - - -
			*/
			_D = _D;

			_cycles = 4;
		}},

		{ 0x53, [&]() {

			/*
				LD D, E
				1  4
				- - - -
			*/
			_D = _E;
			
			_cycles = 4;
		}},

		{ 0x54, [&]() {

			/*
				LD D, H
				1  4
				- - - -
			*/
			_D = _H;

			_cycles = 4;
		}},

		{ 0x55, [&]() {

			/*
				LD D, L
				1  4
				- - - -
			*/
			_D = _L;
			
			_cycles = 4;
		}},

		{ 0x56, [&]() {

			/*
				LD D, (HL)
				1  8
				- - - -
			*/
			ld_r_d8(_D, _H.join(_L));
		
		}},

		{ 0x57, [&]() {

			/*
				LD D, A
				1  4
				- - - -
			*/
			_D = _A;
			
			_cycles = 4;
		}},

		{ 0x58, [&]() {

			/*
				LD E, B
				1  4
				- - - -
			*/
			_E = _B;

			_cycles = 4;
		}},

		{ 0x59, [&]() {

			/*
				LD E, C
				1  4
				- - - -
			*/
			_E = _C;

			_cycles = 4;
		}},

		{ 0x5A, [&]() {

			/*
				LD E, D
				1  4
				- - - -
			*/
			_E = _D;

			_cycles = 4;
		}},

		{ 0x5B, [&]() {

			/*
				LD E, E
				1  4
				- - - -
			*/
			_E = _E;

			_cycles = 4;
		}},

		{ 0x5C, [&]() {

			/*
				LD E, H
				1  4
				- - - -
			*/
			_E = _H;

			_cycles = 4;
		}},

		{ 0x5D, [&]() {

			/*
				LD E, L
				1  4
				- - - -
			*/
			_E = _L;
			
			_cycles = 4;
		}},

		{ 0x5E, [&]() {

			/*
				LD E, (HL)
				1  8
				- - - -
			*/
			ld_r_d8(_E, _H.join(_L));

		}},

		{ 0x5F, [&]() {

			/*
				LD E, A
				1  4
				- - - -
			*/
			_E = _A;

			_cycles = 4;
		}},

		{ 0x60, [&]() {

			/*
				LD H, B
				1  4
				- - - -
			*/
			_H = _B;

			_cycles = 4;
		}},

		{ 0x61, [&]() {

			/*
				LD H, C
				1  4
				- - - -
			*/
			_H = _C;

			_cycles = 4;
		}},

		{ 0x62, [&]() {

			/*
				LD H, D
				1  4
				- - - -
			*/
			_H = _D;

			_cycles = 4;
		}},

		{ 0x63, [&]() {

			/*
				LD H, E
				1  4
				- - - -
			*/
			_H = _E;

			_cycles = 4;
		}},

		{ 0x64, [&]() {

			/*
				LD H, H
				1  4
				- - - -
			*/
			_H = _H;

			_cycles = 4;
		}},

		{ 0x65, [&]() {

			/*
				LD H, L
				1  4
				- - - -
			*/
			_H = _L;
			
			_cycles = 4;
		}},

		{ 0x66, [&]() {

			/*
				LD H, (HL)
				1  8
				- - - -
			*/
			ld_r_d8(_H, _H.join(_L));

		}},

		{ 0x67, [&]() {

			/*
				LD H, A
				1  4
				- - - -
			*/
			_H = _A;
			
			_cycles = 4;
		}},

		{ 0x68, [&]() {

			/*
				LD L, B
				1  4
				- - - -
			*/
			_L = _B;

			_cycles = 4;
		}},

		{ 0x69, [&]() {

			/*
				LD L, C
				1  4
				- - - -
			*/
			_L = _C;

			_cycles = 4;
		}},

		{ 0x6A, [&]() {

			/*
				LD L, D
				1  4
				- - - -
			*/
			_L = _D;

			_cycles = 4;
		}},

		{ 0x6B, [&]() {

			/*
				LD L, E
				1  4
				- - - -
			*/
			_L = _E;

			_cycles = 4;
		}},

		{ 0x6C, [&]() {

			/*
				LD L, H
				1  4
				- - - -
			*/
			_L = _H;

			_cycles = 4;
		}},

		{ 0x6D, [&]() {

			/*
				LD L, L
				1  4
				- - - -
			*/
			_L = _L;

			_cycles = 4;
		}},

		{ 0x6E, [&]() {

			/*
				LD L, (HL)
				1  8
				- - - -
			*/
			ld_r_d8(_L, _H.join(_L));

		}},

		{ 0x6F, [&]() {

			/*
				LD L, A
				1  4
				- - - -
			*/
			_L = _A;
			
			_cycles = 4;
		}},

		{ 0x70, [&]() {

			/*
				LD (HL), B
				1  8
				- - - -
			*/
			ld_d16_r(_H.join(_L), _B);

		}},

		{ 0x71, [&]() {

			/*
				LD (HL), C
				1  8
				- - - -
			*/
			ld_d16_r(_H.join(_L), _C);

		}},

		{ 0x72, [&]() {

			/*
				LD (HL), D
				1  8
				- - - -
			*/
			ld_d16_r(_H.join(_L), _D);

		}},

		{ 0x73, [&]() {

			/*
				LD (HL), E
				1  8
				- - - -
			*/
			ld_d16_r(_H.join(_L), _E);

		}},

		{ 0x74, [&]() {

			/*
				LD (HL), H
				1  8
				- - - -
			*/
			ld_d16_r(_H.join(_L), _H);

		}},

		{ 0x75, [&]() {

			/*
				LD (HL), L
				1  8
				- - - -
			*/
			ld_d16_r(_H.join(_L), _L);

		}},

		{ 0x76, [&]() {

			/*
				HALT
				1  4
				- - - -
			*/
			_halted = true;

			_cycles = 4;
		}},

		{ 0x77, [&]() {

			/*
				LD (HL), A
				1  8
				- - - -
			*/
			ld_d16_r(_H.join(_L), _A);

		}},

		{ 0x78, [&]() {

			/*
				LD A, B
				1  4
				- - - -
			*/
			_A = _B;

			_cycles = 4;
		}},

		{ 0x79, [&]() {

			/*
				LD A, C
				1  4
				- - - -
			*/
			_A = _C;
			
			_cycles = 4;
		}},

		{ 0x7A, [&]() {

			/*
				LD A, D
				1  4
				- - - -
			*/
			_A = _D;

			_cycles = 4;
		}},

		{ 0x7B, [&]() {

			/*
				LD A, E
				1  4
				- - - -
			*/
			_A = _E;

			_cycles = 4;
		}},

		{ 0x7C, [&]() {

			/*
				LD A, H
				1  4
				- - - -
			*/
			_A = _H;

			_cycles = 4;
		}},

		{ 0x7D, [&]() {

			/*
				LD A, L
				1  4
				- - - -
			*/
			_A = _L;
			
			_cycles = 4;
		}},

		{ 0x7E, [&]() {

			/*
				LD A, (HL)
				1  8
				- - - -
			*/
			ld_r_d8(_A, _H.join(_L));

		}},

		{ 0x7F, [&]() {

			/*
				LD A, A
				1  4
				- - - -
			*/
			_A = _A;
			
			_cycles = 4;
		}},

		{ 0x80, [&]() {

			/*
				ADD A, B
				1  4
				Z 0 H C
			*/
			add8(_A, _B);
			
		}},

		{ 0x81, [&]() {

			/*
				ADD A, C
				1  4
				Z 0 H C
			*/
			add8(_A, _C);

		}},

		{ 0x82, [&]() {

			/*
				ADD A, D
				1  4
				Z 0 H C
			*/
			add8(_A, _D);

		}},

		{ 0x83, [&]() {

			/*
				ADD A, E
				1  4
				Z 0 H C
			*/
			add8(_A, _E);

		}},

		{ 0x84, [&]() {

			/*
				ADD A, H
				1  4
				Z 0 H C
			*/
			add8(_A, _H);
			
		}},

		{ 0x85, [&]() {

			/*
				ADD A, L
				1  4
				Z 0 H C
			*/
			add8(_A, _L);
			
		}},

		{ 0x86, [&]() {

			/*
				ADD A, (HL)
				1  8
				Z 0 H C
			*/
			switch (_currentCycle)
			{
			case 1:
				// read Instruction
				break;
			case 2:
				add8(_A, _memory.read(_H.join(_L)));
				break;
			}

			_cycles = 4;
			_currentCycle = _currentCycle == 2 ? 1 : _currentCycle + 1;

		}},

		{ 0x87, [&]() {
			
			/*
				ADD A, A
				1  4
				Z 0 H C
			*/
			add8(_A, _A);

		}},

		{ 0x88, [&]() {

			/*
				ADC A, B
				1  4
				Z 0 H C
			*/
			add8(_A, _B, true);
			
		}},

		{ 0x89, [&]() {

			/*
				ADC A, C
				1  4
				Z 0 H C
			*/
			add8(_A, _C, true);
			
		}},

		{ 0x8A, [&]() {

			/*
				ADC A, D
				1  4
				Z 0 H C
			*/
			add8(_A, _D, true);
			
		}},

		{ 0x8B, [&]() {

			/*
				ADC A, E
				1  4
				Z 0 H C
			*/
			add8(_A, _E, true);
			
		}},

		{ 0x8C, [&]() {

			/*
				ADC A, H
				1  4
				Z 0 H C
			*/
			add8(_A, _H, true);
			
		}},

		{ 0x8D, [&]() {

			/*
				ADC A, L
				1  4
				Z 0 H C
			*/
			add8(_A, _L, true);
			
		}},

		{ 0x8E, [&]() {

			/*
				ADC A, (HL)
				1  8
				Z 0 H C
			*/
			switch (_currentCycle)
			{
			case 1:
				// read Instruction
				break;
			case 2:
				add8(_A, _memory.read(_H.join(_L)), true);
				break;
			}

			_cycles = 4;
			_currentCycle = _currentCycle == 2 ? 1 : _currentCycle + 1;
		}},

		{ 0x8F, [&]() {

			/*
				ADC A, A
				1  4
				Z 0 H C
			*/
			add8(_A, _A, true);

		}},

		{ 0x90, [&]() {

			/*
				SUB B
				1  4
				Z 1 H C
			*/
			sub8(_B);
			
		}},

		{ 0x91, [&]() {

			/*
				SUB C
				1  4
				Z 1 H C
			*/
			sub8(_C);

		}},

		{ 0x92, [&]() {

			/*
				SUB D
				1  4
				Z 1 H C
			*/
			sub8(_D);
			
		}},

		{ 0x93, [&]() {

			/*
				SUB E
				1  4
				Z 1 H C
			*/
			sub8(_E);
			
		}},

		{ 0x94, [&]() {

			/*
				SUB H
				1  4
				Z 1 H C
			*/
			sub8(_H);
			
		}},

		{ 0x95, [&]() {

			/*
				SUB L
				1  4
				Z 1 H C
			*/
			sub8(_L);
			
		}},

		{ 0x96, [&]() {

			/*
				SUB (HL)
				1  8
				Z 1 H C
			*/
			switch (_currentCycle)
			{
			case 1:
				// read Instruction
				break;
			case 2:
				sub8(_memory.read(_H.join(_L)));
				break;
			}

			_cycles = 4;
			_currentCycle = _currentCycle == 2 ? 1 : _currentCycle + 1;
		}},

		{ 0x97, [&]() {

			/*
				SUB A
				1  4
				1 1 0 0
			*/
			sub8(_A);

		}},

		{ 0x98, [&]() {

			/*
				SBC A, B
				1  4
				Z 1 H C
			*/
			sub8(_B, true);

		}},

		{ 0x99, [&]() {

			/*
				SBC A, C
				1  4
				Z 1 H C
			*/
			sub8(_C, true);

		}},

		{ 0x9A, [&]() {

			/*
				SBC A, D
				1  4
				Z 1 H C
			*/
			sub8(_D, true);

		}},

		{ 0x9B, [&]() {

			/*
				SBC A, E
				1  4
				Z 1 H C
			*/
			sub8(_E, true);

		}},

		{ 0x9C, [&]() {

			/*
				SBC A, H
				1  4
				Z 1 H C
			*/
			sub8(_H, true);
			
		}},

		{ 0x9D, [&]() {

			/*
				SBC A, L
				1  4
				Z 1 H C
			*/
			sub8(_L, true);

		}},

		{ 0x9E, [&]() {

			/*
				SBC A, (HL)
				1  8
				Z 1 H C
			*/
			switch (_currentCycle)
			{
			case 1:
				// read Instruction
				break;

			case 2:
				sub8(_memory.read(_H.join(_L)), true);
				break;
			}

			_cycles = 4;
			_currentCycle = _currentCycle == 2 ? 1 : _currentCycle + 1;
		}},

		{ 0x9F, [&]() {

			/*
				SBC A, A
				1  4
				Z 1 H C
			*/
			sub8(_A, true);

		}},

		{ 0xA0, [&]() {

			/*
				AND B
				1  4
				Z 0 1 0
			*/
			and8(_B);
			
		}},

		{ 0xA1, [&]() {

			/*
				AND C
				1  4
				Z 0 1 0
			*/
			and8(_C);

		}},

		{ 0xA2, [&]() {

			/*
				AND D
				1  4
				Z 0 1 0
			*/
			and8(_D);

		}},

		{ 0xA3, [&]() {

			/*
				AND E
				1  4
				Z 0 1 0
			*/
			and8(_E);

		}},

		{ 0xA4, [&]() {

			/*
				AND H
				1  4
				Z 0 1 0
			*/
			and8(_H);

		}},

		{ 0xA5, [&]() {

			/*
				AND L
				1  4
				Z 0 1 0
			*/
			and8(_L);

		}},

		{ 0xA6, [&]() {

			/*
				AND (HL)
				1  8
				Z 0 1 0
			*/
			arithmic_a(&CPU::and8, _H.join(_L));
		
		}},

		{ 0xA7, [&]() {

			/*
				AND A
				1  4
				Z 0 1 0
			*/
			and8(_A);
			
		}},

		{ 0xA8, [&]() {

			/*
				XOR B
				1  4
				Z 0 0 0
			*/
			xor8(_B);

		}},

		{ 0xA9, [&]() {

			/*
				XOR C
				1  4
				Z 0 0 0
			*/
			xor8(_C);

		}},

		{ 0xAA, [&]() {

			/*
				XOR D
				1  4
				Z 0 0 0
			*/
			xor8(_D);
			
		}},

		{ 0xAB, [&]() {

			/*
				XOR E
				1  4
				Z 0 0 0
			*/
			xor8(_E);

		}},

		{ 0xAC, [&]() {

			/*
				XOR H
				1  4
				Z 0 0 0
			*/
			xor8(_H);

		}},

		{ 0xAD, [&]() {

			/*
				XOR L
				1  4
				Z 0 0 0
			*/
			xor8(_L);

		}},

		{ 0xAE, [&]() {

			/*
				XOR (HL)
				1  8
				Z 0 0 0
			*/
			arithmic_a(&CPU::xor8, _H.join(_L));
		
		}},

		{ 0xAF, [&]() {

			/*
				XOR A
				1  4
				1 0 0 0
			*/
			xor8(_A);
		}},

		{ 0xB0, [&]() {

			/*
				OR B
				1  4
				Z 0 0 0
			*/
			or8(_B);

		}},

		{ 0xB1, [&]() {

			/*
				OR C
				1  4
				Z 0 0 0
			*/
			or8(_C);

		}},

		{ 0xB2, [&]() {

			/*
				OR D
				1  4
				Z 0 0 0
			*/
			or8(_D);
			
		}},

		{ 0xB3, [&]() {

			/*
				OR E
				1  4
				Z 0 0 0
			*/
			or8(_E);
			
		}},

		{ 0xB4, [&]() {

			/*
				OR H
				1  4
				Z 0 0 0
			*/
			or8(_H);
			
		}},

		{ 0xB5, [&]() {

			/*
				OR L
				1  4
				Z 0 0 0
			*/
			or8(_L);

		}},

		{ 0xB6, [&]() {

			/*
				OR (HL)
				1  8
				Z 0 0 0
			*/
			if (_currentCycle == 1)
				auto a = _memory.read(_H.join(_L));

			arithmic_a(&CPU::or8, _H.join(_L));
			
		}},

		{ 0xB7, [&]() {

			/*
				OR A
				1  4
				Z 0 0 0
			*/
			or8(_A);

		}},

		{ 0xB8, [&]() {

			/*
				CP B
				1  4
				Z 1 H C
			*/
			cp8(_B);
			
		}},

		{ 0xB9, [&]() {

			/*
				CP C
				1  4
				Z 1 H C
			*/
			cp8(_C);
			
		}},

		{ 0xBA, [&]() {

			/*
				CP D
				1  4
				Z 1 H C
			*/
			cp8(_D);

		}},

		{ 0xBB, [&]() {

			/*
				CP E
				1  4
				Z 1 H C
			*/
			cp8(_E);
			
		}},

		{ 0xBC, [&]() {

			/*
				CP H
				1  4
				Z 1 H C
			*/
			cp8(_H);

		}},

		{ 0xBD, [&]() {

			/*
				CP L
				1  4
				Z 1 H C
			*/
			cp8(_L);
			
		}},

		{ 0xBE, [&]() {

			/*
				CP (HL)
				1  8
				Z 1 H C
			*/
			arithmic_a(&CPU::cp8, _H.join(_L));

		}},

		{ 0xBF, [&]() {

			/*
				CP A
				1  4
				1 1 0 0
			*/
			cp8(_A);
			
		}},

		{ 0xC0, [&]() {

			/*
				RET NZ
				1  20/8
				- - - -
			*/
			ret_cc(!_F[Flag::ZERO]);

		}},

		{ 0xC1, [&]() {

			/*
				POP BC
				1  12
				- - - -
			*/
			if (_SP == 0xFEF0)
				auto a = 5;

			pop(_B, _C);

		}},

		{ 0xC2, [&]() {

			/*
				JP NZ, a16
				3  16/12
				- - - -
			*/
			jp_cc(!_F[Flag::ZERO]);

		}},

		{ 0xC3, [&]() {

			/*
				JP a16
				3  16
				- - - -
			*/
			jp_cc(true);
		
		}},

		{ 0xC4, [&]() {

			/*
				CALL NZ, a16
				3  24/12	
				- - - -
			*/
			call_cc(!_F[Flag::ZERO]);

		}},

		{ 0xC5, [&]() {

			/*
				PUSH BC
				1  16
				- - - -
			*/
			push(_B.join(_C));

		}},

		{ 0xC6, [&]() {

			/*
				ADD A, d8
				2  8
				Z 0 H C
			*/

			switch (_currentCycle)
			{
			case 1:
				// read Instruction
				break;
			
			case 2:
				add8(_A, _memory.read(_PC++, true));
				break;
			}
			_cycles = 4;
			_currentCycle = _currentCycle == 2 ? 1 : _currentCycle + 1;
		}},

		{ 0xC7, [&]() {

			/*
				RST 00H
				1  16
				- - - -
			*/
			rst(0x00);

		}},

		{ 0xC8, [&]() {

			/*
				RET Z
				1  20/8
				- - - -
			*/
			ret_cc(_F[Flag::ZERO]);
		}},

		{ 0xC9, [&]() {

			/*
				RET
				1  16
				- - - -
			*/

			switch (_currentCycle)
			{
			case 1:
				// read Instruction
				break;

			case 2:
				_container = popStackLow();
				break;

			case 3:
				_container |= popStackHigh() << 8;
				break;

			case 4:
				_PC = _container;
				break;
			}

			_cycles = 4;
			_currentCycle = _currentCycle == 4 ? 1 : _currentCycle + 1;
		}},

		{ 0xCA, [&]() {

			/*
				JP Z, a16
				3  16/12
				- - - -
			*/
			jp_cc(_F[Flag::ZERO]);

		}},

		{ 0xCB, [&]() {

			/*
				PREFIX
				- - - -
			*/	
			_nextPrefix = 1;
			_cycles = 4;
		}},

		{ 0xCC, [&]() {

			/*
				CALL Z, a16
				3  24/12
				- - - -
			*/						
			call_cc(_F[Flag::ZERO]);

		}},

		{ 0xCD, [&]() {

			/*
				CALL a16
				3  24
				- - - -
			*/
			call_cc(true);

		}},

		{ 0xCE, [&]() {

			/*
				ADC A, d8
				2  8
				Z 0 H C
			*/

			switch (_currentCycle)
			{
			case 1:
				// read Instruction
				break;

			case 2:
				add8(_A, _memory.read(_PC++, true), true);
				break;
			}
			_cycles = 4;
			_currentCycle = _currentCycle == 2 ? 1 : _currentCycle + 1;
		}},

		{ 0xCF, [&]() {

			/*
				RST 08H
				1  16
				- - - -
			*/
			rst(0x08);
		
		}},

		{ 0xD0, [&]() {

			/*
				RET NC
				1  20/8
				- - - -
			*/
			ret_cc(!_F[Flag::CARRY]);

		}},

		{ 0xD1, [&]() {

			/*
				POP DE
				1  12
				- - - -
			*/
			pop(_D, _E);

		}},

		{ 0xD2, [&]() {

			/*
				JP NC, a16
				3  16/12
				- - - -
			*/
			jp_cc(!_F[Flag::CARRY]);

		}},

		{ 0xD3, [&]() {
			/* XXX */
			throw InvalidInstructionException();
		}},

		{ 0xD4, [&]() {

			/*
				CALL NC, a16
				3  24/12
				- - - -
			*/
			call_cc(!_F[Flag::CARRY]);

		}},

		{ 0xD5, [&]() {

			/*
				PUSH DE
				1  16
				- - - -
			*/
			push(_D.join(_E));
		}},

		{ 0xD6, [&]() {

			/*
				SUB A, d8
				2  8
				Z 1 H C
			*/

			switch (_currentCycle)
			{
			case 1:
				// read Instruction
				break;

			case 2:
				sub8(_memory.read(_PC++, true));
				break;
			}

			_cycles = 4;
			_currentCycle = _currentCycle == 2 ? 1 : _currentCycle + 1;
		}},

		{ 0xD7, [&]() {

			/*
				RST 10H
				1  16
				- - - -
			*/
			rst(0x10);

		}},

		{ 0xD8, [&]() {

			/*
				RET C
				1  20/8
				- - - -
			*/
			ret_cc(_F[Flag::CARRY]);

		}},

		{ 0xD9, [&]() {

			/*
				RETI
				1  16
				- - - -
			*/

			switch (_currentCycle)
			{
			case 1:
				// read Instruction
				break;

			case 2:
				_container = popStackLow();
				break;

			case 3:
				_container |= popStackHigh() << 8;
				break;

			case 4:
				_PC = _container;
				Interrupts::IME = true;
				break;
			}

			_cycles = 4;
			_currentCycle = _currentCycle == 4 ? 1 : _currentCycle + 1;
		}},

		{ 0xDA, [&]() {

			/*
				JP C, a16
				3  16/12
				- - - -
			*/
			jp_cc(_F[Flag::CARRY]);

		}},

		{ 0xDB, [&]() {
			/* XXX */
			throw InvalidInstructionException();
		}},

		{ 0xDC, [&]() {

			/*
				CALL C, a16
				3  24/12
				- - - -
			*/
			call_cc(_F[Flag::CARRY]);

		}},

		{ 0xDD, [&]() {

			/* XXX */
			throw InvalidInstructionException();
		}},

		{ 0xDE, [&]() {

			/*
				SBC A, d8
				2  8
				Z 1 H C
			*/

			switch (_currentCycle)
			{
			case 1:
				// read Instruction
				break;
			case 2:
				sub8(_memory.read(_PC++, true), true);
				break;
			}

			_cycles = 4;
			_currentCycle = _currentCycle == 2 ? 1 : _currentCycle + 1;
		}},

		{ 0xDF, [&]() {

			/*
				RST 18H
				1  16
				- - - -
			*/
			rst(0x18);

		}},

		{ 0xE0, [&]() {

			/*
				LDH (0xFF00 + a8), A
				2  12
				- - - -
			*/

			switch (_currentCycle)
			{
			case 1:
				// read Instruction 
				break;
			
			case 2:
				_container = _memory.read(_PC, true);
				break;
			
			case 3:
				_memory.write(0xFF00 + _container, _A);
				++_PC;
				break;
			}

			_cycles = 4;
			_currentCycle = _currentCycle == 3 ? 1 : _currentCycle + 1;
		}},

		{ 0xE1, [&]() {

			/*
				POP HL
				1  12
				- - - -
			*/
			pop(_H, _L);

		}},

		{ 0xE2, [&]() {

			/*
				LD (C), A
				1  8
				- - - -
			*/

			switch (_currentCycle)
			{
			case 1:
				// read Instruction 
				break;

			case 2:
				_memory.write(0xFF00 + _C, _A);
				break;
			}
			_cycles = 4;
			_currentCycle = _currentCycle == 2 ? 1 : _currentCycle + 1;
		}},

		{ 0xE3, [&]() {
			/* XXX */
			throw InvalidInstructionException();
		}},

		{ 0xE4, [&]() {
			/* XXX */
			throw InvalidInstructionException();
		}},

		{ 0xE5, [&]() {

			/*
				PUSH HL
				1  16
				- - - -
			*/
			push(_H.join(_L));

		}},

		{ 0xE6, [&]() {

			/*
				AND d8
				2  8
				Z 0 1 0
			*/
			arithmic_a(&CPU::and8, _PC, true); 

		}},

		{ 0xE7, [&]() {

			/*
				RST 20H
				1  16
				- - - -
			*/
			rst(0x20);

		}},

		{ 0xE8, [&]() {

			/*
				ADD SP, r8
				2  16
				0 0 H C
			*/
			_cycles = 4;

			switch (_currentCycle)
			{
			case 1:
				// read Instruction
				break;
				
			case 2:
				_sContainer = static_cast<char>(_memory.read(_PC, true));
				break;

			case 3:
				int fullRes = static_cast<int>(_SP + _sContainer);

				_F[Flag::ZERO] = 0;
				_F[Flag::SUBTRACT] = 0;
				_F[Flag::HALF_CARRY] = ((_SP ^ _sContainer ^ (fullRes & 0xFFFF)) & 0x10) == 0x10;
				_F[Flag::CARRY] = ((_SP ^ _sContainer ^ (fullRes & 0xFFFF)) & 0x100) == 0x100;

				_SP = static_cast<u16>(fullRes);
				
				++_PC;
				_cycles = 8;
				break;
			}

			_currentCycle = _currentCycle == 3 ? 1 : _currentCycle + 1;
		}},

		{ 0xE9, [&]() {

			/*
				JP HL
				1  4
				- - - -
			*/
			_PC = _H.join(_L);

			_cycles = 4;
		}},

		{ 0xEA, [&]() {

			/*
				LD (a16), A
				3  16
				- - - -
			*/

			switch (_currentCycle)
			{
			case 1:
				// read Instruction
				break;

			case 2:
				_container = _memory.read(_PC, true);
				break;

			case 3:
				_container = join(_memory.read(_PC + 1, true), _container);
				break;

			case 4:
				_memory.write(_container, _A);
				_PC += 2;
				break;
			}

			_cycles = 4;
			_currentCycle = _currentCycle == 4 ? 1 : _currentCycle + 1;
		}},

		{ 0xEB, [&]() {
			/* XXX */
			throw InvalidInstructionException();
		}},

		{ 0xEC, [&]() {
			/* XXX */
			throw InvalidInstructionException();
		}},

		{ 0xED, [&]() {
			/* XXX */
			throw InvalidInstructionException();
		}},

		{ 0xEE, [&]() {

			/*
				XOR d8
				2  8
				Z 0 0 0
			*/
			arithmic_a(&CPU::xor8, _PC, true);
		
		}},

		{ 0xEF, [&]() {

			/*
				RST 28H
				1  16
				- - - -
			*/
			rst(0x28);

		}},

		{ 0xF0, [&]() {
			
			/*
				LDH A, (0xFF00 + a8)
				2  12
				- - - -
			*/

			switch (_currentCycle)
			{
			case 1:
				// read _PC
				break;

			case 2:
				_container = _memory.read(_PC, true);
				break;

			case 3:
				_A = _memory.read(0xFF00 + _container);
				++_PC;
				break;
			}

			_cycles = 4;
			_currentCycle = _currentCycle == 3 ? 1 : _currentCycle + 1;
		}},

		{ 0xF1, [&]() {

			/*
				POP AF
				1  12
				Z N H C
			*/
			pop(_A, _F);
			_F &= 0b11110000; // LSByte should not change

		}},

		{ 0xF2, [&]() {

			/*
				LD A, (0xFF00 + C)
				1  8
				- - - -
			*/
			ld_r_d8(_A, 0xFF00 + _C);
		
		}},

		{ 0xF3, [&]() {

			/*
				DI
				1  4
				- - - -
			*/
			Interrupts::IME = false;
			_cycles = 4;
		}},

		{ 0xF4, [&]() {
			/* XXX */
			throw InvalidInstructionException();
		}},

		{ 0xF5, [&]() {

			/*
				PUSH AF
				1  16
				- - - -
			*/
			push(_A.join(_F));

		}},

		{ 0xF6, [&]() {

			/*
				OR d8
				2  8
				Z 0 0 0
			*/ 
			arithmic_a(&CPU::or8, _PC, true);

		}},

		{ 0xF7, [&]() {

			/*
				RST 30H
				1  16
				- - - -
			*/
			rst(0x30);

		}},

		{ 0xF8, [&]() {
			/*
				LD HL, SP + r8
				2  12
				0 0 H C
			*/

			switch (_currentCycle)
			{
			case 1:
				// read Instruction
				break;

			case 2:
				_sContainer = static_cast<char>(_memory.read(_PC, true));
				break;

			case 3:
				int fullRes = static_cast<int>(_SP + _sContainer);

				_F[Flag::HALF_CARRY] = ((_SP ^ _sContainer ^ (fullRes & 0xFFFF)) & 0x10) == 0x10;
				_F[Flag::CARRY] = ((_SP ^ _sContainer ^ (fullRes & 0xFFFF)) & 0x100) == 0x100;
				_F[Flag::ZERO] = 0;
				_F[Flag::SUBTRACT] = 0;

				_H.set16(_L, static_cast<u16>(fullRes));
				++_PC;
				break;
			}

			_cycles = 4;
			_currentCycle = _currentCycle == 3 ? 1 : _currentCycle + 1;
		}},

		{ 0xF9, [&]() {

			/*
				LD SP, HL
				1  8
				- - - -
			*/
			_SP = _H.join(_L);
			_cycles = 8;

		}},

		{ 0xFA, [&]() {

			/*
				LD A, (a16)
				3  16
				- - - -
			*/

			switch (_currentCycle)
			{
			case 1:
				// read Instruction
				break;

			case 2:
				_container = _memory.read(_PC, true);
				break;

			case 3:
				_container = join(_memory.read(_PC + 1), _container);
				break;

			case 4:
				_A = _memory.read(_container);
				_PC += 2;
				break;
			}

			_cycles = 4;
			_currentCycle = _currentCycle == 4 ? 1 : _currentCycle + 1;
		}},

		{ 0xFB, [&]() {

			/*
				EI
				1  4
				- - - -
				enables _IME in the next cycle
			*/
			_enableIME = true;
			_cycles = 4;

		}},

		{ 0xFC, [&]() {
			/* XXX */
			throw InvalidInstructionException();
		}},

		{ 0xFD, [&]() {
			/* XXX */
			throw InvalidInstructionException();
		}},

		{ 0xFE, [&]() {

			/*
				CP d8
				2  8
				Z 1 H C
			*/
			arithmic_a(&CPU::cp8, _PC, true);

		}},

		{ 0xFF, [&]() {

			/*
				RST 38H
				1  16
				- - - -
			*/
			rst(0x38);

		}}
	}};

}