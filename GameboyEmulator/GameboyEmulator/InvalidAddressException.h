#pragma once

#include <exception>

class InvalidAddressException : public std::exception
{
public:
	InvalidAddressException() : std::exception("access to invalid address detected") {};
};