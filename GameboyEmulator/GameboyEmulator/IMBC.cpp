#include "IMBC.h"
#include "Memory.h"

//? Constructors ?//


/* -- C'tor -- */
IMBC::IMBC() :
	_cartridge(Cartridge::getInstance())
{
}

std::ostream& operator<<(std::ostream& os, const std::unique_ptr<IMBC>& MBC)
{
	return MBC->getState(os);
}

std::istream& operator>>(std::istream& is, std::unique_ptr<IMBC>& MBC)
{
	return MBC->setState(is);
}
