#pragma once
#include <array>
#include "Utils.h"
#include "Color.h"

namespace Const
{
	
	inline constexpr const char* GAMEBOY_FILE_EXTENTION = ".gb";
	inline constexpr u32 CPS = 4194304;
	inline constexpr float FPS = 59.73f;

	namespace APU
	{


		inline constexpr u32
			CYCLES_64Hz = CPS / 64,
			AUDIO_FREQUENCY = 44100,
			AUDIO_WAIT_CYCLES = 8192;

		inline constexpr u16
			BITS_15_ON = 0x7FFF,
			CYCLES_512Hz = CPS / 512,
			CYCLES_256Hz = CPS / 256,
			CYCLES_128Hz = CPS / 128,
			BUFFER_FRAMES = 1024,
			AUDIO_WAVE_LENGTH = 256,
			MAX_FREQUENCY = 2047,
			AUDIO_NUM_SAMPLES = 2048;


		inline constexpr double AUDIO_CYCLES_UNTIL_SAMPLE_COLLECTION = (double)CPS / (double)AUDIO_FREQUENCY;

		inline constexpr u8
			POWER_FLAG_SQUARE_CHANNEL_1 = 1,
			POWER_FLAG_SQUARE_CHANNEL_2 = 2,
			POWER_FLAG_WAVE_CHANNEL = 4,
			POWER_FLAG_NOISE_CHANNEL = 8,
			NUMBER_OF_CHANNELS = 4,
			TWO_BITS_ON = 3,
			LAST_TWO_BITS_ON = 192,
			ENVELOP_STEP_NUMBER = 7,
			DAC_POWER_FLAG = 248,
			DAC_POWER_FLAG_WAVE = 128,
			TRIGGER_EVENT_FLAG = 128,
			MIN_FREQUENCY = 0,
			FLAG_OUTPUT_VOLUME = 7,
			FLAG_CHANNEL_DUTY_FLAG = 192,
			FLAG_CHANNEL_LEVEL_FLAG = 96,
			FLAG_CHANNEL_FREQUENCY_MSB_FLAG = 7,
			FLAG_CHANNEL_SWEEP_PERIOD = 112,
			FLAG_CHANNEL_SWEEP_DIRECTION = 8,
			FLAG_CHANNEL_SWEEP_SHIFT = 7,
			FLAG_CHANNEL_ENVELOPE_VOLUME = 240,
			FLAG_CHANNEL_DIVISOR = 7,
			FLAG_CHANNEL_WIDTH_FLAG = 8,
			FLAG_CHANNEL_CLOCK_SHIFT = 240,
			FLAG_CHANNEL_ENVELOPE_AMPLIFY = 8,
			FLAG_CHANNEL_ENVELOPLE_PERIOD = 7,
			FLAG_CHANNEL_LENGTH_DATA = 63,
			FLAG_CHANNEL_LENGTH_FLAG = 64,
			AUDIO_DEFAULT_LENGTH = 64, // default value for channels with envelope channels 1/2/4
			FLAG_WAVE_CHANNEL_LENGTH_DATA = 255, // default value for channel 3
			FLAG_APU_POWER = 128,
			AUDIO_DIVISOR_ARRAY[8] = { 8, 16, 32, 48, 64, 80, 96, 112 },
			AUDIO_OUTPUT_QUANTITY = 2,
			AUDIO_WAVE_PATTERN_ARRAY[4][8] = {
			   { 0, 0, 0, 0, 0, 0, 0, 1 },
			   { 1, 0, 0, 0, 0, 0, 0, 1 },
			   { 1, 0, 0, 0, 0, 1, 1, 1 },
			   { 0, 1, 1, 1, 1, 1, 1, 0 }
			},
			WAVE_RAM_DEFUALT_VALUES[] = {
				0x84, 0x40, 0x43, 0xAA, 0x2D, 0x78, 0x92, 0x3C, 0x60, 0x59, 0x59, 0xB0, 0x34,
				0xB8, 0x2E, 0xDA,
			},
			WAVE_CHANNEL_SHIFT_BY_VOLUME[] = { 4, 0, 1, 2 },
			FLAGS_FOR_OUTPUT[] = {
				1, 16, 2, 32, 4, 64, 8, 128
			};

		enum class AudioLevel : u8 
		{
			MUTE = 0,
			L100 = 1,
			L50 = 2,
			L25 = 3
		};
	}


	namespace LCD
	{

		inline constexpr u8 HEIGHT = 144;
		inline constexpr u8 WIDTH = 160;

		inline constexpr u8 MAX_VISIBLE_SCANLINE = 144;
		inline constexpr u8 MAX_INVISIBLE_SCANLINE = 153;

		inline constexpr u16 CLOCKS_TO_DRAW_LINE = 456;


		namespace Status
		{

			// Bits 0-1 in Status
			namespace Mode
			{
				inline constexpr u8 HBLANK = 0b00;
				inline constexpr u8 VBLANK = 0b01;
				inline constexpr u8 SEARCH_SPRITES = 0b10;
				inline constexpr u8 TRANSFER = 0b11;

				namespace Cycles
				{
					inline constexpr u8 HBLANK = 0;
					inline constexpr u8 VBLANK = 204;
					inline constexpr u8 OAM = 80;
					inline constexpr u8 TRANSFER = 172;
				}
			}

			namespace Bit
			{
				inline constexpr u8 CONICIDENCE_FLAG = 2;
				inline constexpr u8 HBLANK_INT_ENABLED = 3;
				inline constexpr u8 VBLANK_INT_ENABLED = 4;
				inline constexpr u8 OAM_INT_ENABLED = 5;
				inline constexpr u8 CONICIDENCE_INT_ENABLED = 6;
			}
		}


		namespace Control
		{
			namespace Bit
			{
				inline constexpr u8 DISPLAY_ENABLED = 0;
				inline constexpr u8 SPRITES_ENABLED = 1;
				inline constexpr u8 SPRITE_SIZE = 2;	 // 0=8x8, 1=8x16
				inline constexpr u8 BG_TILES_MAP = 3;	 // 0=9800-9BFF, 1=9C00-9FFF
				inline constexpr u8 TILES_DATA_MAP = 4;	 // 0=8800-97FF (unsigned), 1=8000-8FFF (signed)
				inline constexpr u8 WINDOW_ENABLED = 5;
				inline constexpr u8 BG_WINDOW_TILES_MAP = 6; // 0=9800-9BFF, 1=9C00-9FFF
				inline constexpr u8 LCD_ENABLED = 7;
			}
		}

		namespace Tile
		{
			inline constexpr u8 TILES_COUNT = 32;
			inline constexpr u8 TILE_SIZE = 8;
		}

		namespace Sprite
		{
			inline constexpr u8 SPRITES_COUNT = 40;
			inline constexpr u8 SPRITE_WIDTH = 8;
			inline constexpr u8 SPRITE_SIZE = 16;
			inline constexpr u8 SPRITES_ATTR_SIZE = 4;

			namespace AttrBit
			{
				inline constexpr u8	PALLETE_ADDRESS = 4; // 0=0xFF48 1=0xFF49
				inline constexpr u8 X_FLIP = 5;
				inline constexpr u8 Y_FLIP = 6;
				inline constexpr u8 SPRITES_BG_PRIORITY = 7;
			}
		}

		namespace ColorType
		{
			inline constexpr u8	WHITE = 0;
			inline constexpr u8 LIGHT_GRAY = 1;
			inline constexpr u8 DARK_GRAY = 2;
			inline constexpr u8 BLACK = 3;
		}

	}

	namespace Color
	{
		static constexpr inline ::Color WHITE		  = { 255, 255, 255 };
		static constexpr inline ::Color LIGHT_GRAY	  = { 204, 204, 204 };
		static constexpr inline ::Color DARK_GRAY	  = { 119, 119, 119 };
		static constexpr inline ::Color BLACK		  = {   0,   0,   0 };
								
		static constexpr inline ::Color OG_WHITE	  = { 224, 248, 208 };
		static constexpr inline ::Color OG_LIGHT_GRAY = { 136, 192, 112 };
		static constexpr inline ::Color OG_DARK_GRAY  = {  52, 104,  86 };
		static constexpr inline ::Color OG_BLACK	  = {   8,  24,  32 };
	}

	namespace Joypad
	{
		namespace Bit
		{
			inline constexpr u8 DIRECTION_KEY = 4; // 0=select
			inline constexpr u8 BUTTON_KEY = 5; // 0=select
		}
	}

	namespace Cartridge
	{
		inline constexpr u16 MAX_ROM_BANKS_COUNT = 512;
		inline constexpr u16 MIN_ROM_BANKS_COUNT = 2;
		
	}

	namespace Memory
	{
		inline constexpr u32 SIZE = 0x10000;
	}
}
