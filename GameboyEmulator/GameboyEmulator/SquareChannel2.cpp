#include "SquareChannel2.h"
#include "Address.h"

SquareChannel2::SquareChannel2(Memory& memory):
	ISquareChannel(memory,
		Address::APU::SquareChannel2::NRx0,
		Address::APU::SquareChannel2::NRx1,
		Address::APU::SquareChannel2::NRx2,
		Address::APU::SquareChannel2::NRx3,
		Address::APU::SquareChannel2::NRx4
	)
{
}


void SquareChannel2::setEnabled()
{
	_isRunning = _memory[Address::APU::NRx52] & Const::APU::POWER_FLAG_SQUARE_CHANNEL_2;
}

