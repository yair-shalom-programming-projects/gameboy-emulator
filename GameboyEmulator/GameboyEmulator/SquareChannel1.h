#pragma once
#include "ISquareChannel.h"

class Memory;

class SquareChannel1 final : public ISquareChannel
{

public:
	SquareChannel1(Memory& memory);

	virtual void reset() override;
	virtual void triggerEvent(const u8 frameSequencer) override;
	virtual std::istream& setState(std::istream& is) override;
	virtual std::ostream& getState(std::ostream& os) const override;
	virtual void setEnabled() override;

	void sweepTick();
	void setSweep();
	void setEnabledSweepFlag();

	friend std::istream& operator>>(std::istream& is, SquareChannel1& channel1);
	friend std::ostream& operator<<(std::ostream& os, const SquareChannel1& channel1);


private:
	u8 _period;
	u8 _periodTimer;
	u8 _sweepShift;
	u16 _shadow;

	bool _sweepEnabled;
	bool _sweepNegate;
	bool _didSweepNegate;

	void setFrequencySweep();
	u16 calculateFrequency();
	void writeFrequencyToMemory();

};

