#pragma once
#include "Constants.h"

// NRx2 is the register of every channel that the data there has information about the envelope 
// 7-4 bits are the starting volume
// 3 bit it to either amplify the volume or decrease it
// 0-2 bits are the period
class VolumeEnvelope final
{
public:
    explicit VolumeEnvelope(u8 period , u8 startVol , bool amplify);

    void setVolumeEnvelope(u8 period, u8 startVol, bool amplify);
    void clock();
    void triggerEvent(const u8 vol, const u8 frameSequencer);
    u8 getVolume() const;
    bool isNextStepClocksVol(const u8 frameSequencer) const;

    friend std::istream& operator>>(std::istream& is, VolumeEnvelope& volumeEnvelope);
    friend std::ostream& operator<<(std::ostream& os, const VolumeEnvelope& volumeEnvelope);


private:
    u8 _volume;
    u8 _period;
    u8 _periodTimer;
    bool _amplify; // decrease or increase the volume
    bool _disable_env = false;

};

