#include "Timers.h"
#include "Utils.h"
#include "Constants.h"
#include "Address.h"
#include "APU.h"
using namespace Address::Timer;
using namespace Utils;
using Address::Int::REQUEST_REGISTER;



/* -- C'tor -- */
Timers::Timers(Singleton_t) :
    _memory(Memory::getInstance()),
    _interrupts(Interrupts::getInstance())
{
    reset();
}


/* -- update Timers (TIMA & Internal Counter) -- */
void Timers::update(const u8 cycles)
{
    if (_memory[INTERNAL_COUNTER] + cycles > 0xFF) {
        APU::getInstance().checkDivClockFs(_memory[DIVIDER_REGISTER] + 1 , _memory[DIVIDER_REGISTER]);
    }
    const u16 internalCounter = _memory.rawRead16(INTERNAL_COUNTER) + cycles;
    _memory.rawWrite16(INTERNAL_COUNTER, internalCounter);

    releaseOverflow();
    
    handleOverflow();

    updateTIMA(internalCounter);

    _lastTimer = _memory[TIMER];
    _lastModulo = _memory[MODULO];

}

void Timers::reset()
{
    _lastTimer = 0;
    _lastModulo = 0;
    
    _releaseOverflow = false;
    _overflow = false;
    _isTimerCarry = false;
    _lastSignal = false;
    _lastVisiblePulse = false;
}

/*
    --
    handle Timer overflow (if exist) by setting
    Timer value to Modulo value and request Timer interrupt
    --
*/
void Timers::handleOverflow() 
{
    if (!_overflow) return;
    // if Timer written in an overflow cycle, his value won't set to Modulator's value
    if (_memory[TIMER] == _lastTimer)
        _memory[TIMER] = _memory[MODULO];

    _interrupts.request(Interrupt::TIMER);

    _isTimerCarry = false;
    _releaseOverflow = true;
}


/* -- release pending overflow (if exist) -- */
void Timers::releaseOverflow() noexcept
{
    if (!_releaseOverflow) return;
    _overflow = false;
    _releaseOverflow = false;

    // if Modulator written between overflow and release, Timer set to Modulator's value
    if (_lastModulo != _memory[MODULO])
        _memory[TIMER] = _memory[MODULO];
}

/* 
    -- increase TIMA (Timer) if it's time to -- 
    according to the current frequency, a bit from the Internal Counter
    is selected. When this bit is switch from 1 to 0 an increase should trigger.
    Gameboy's TIMA inc architecture can cause glitched increases and weird behaviours.
    For more: https://github.com/geaz/emu-gameboy/blob/master/docs/The%20Cycle-Accurate%20Game%20Boy%20Docs.pdf
*/
void Timers::updateTIMA(const u16 internalCounter) noexcept
{

    const u16 bit = (Const::CPS / getFrequency()) >> 1;
    const bool signal = (internalCounter & bit) == bit && isTimerEnabled();

    if (!signal && _lastSignal)
    {
        ++_memory[TIMER];

        // checks if there's an overflow (0xFF -> 0x00)
        if (!_memory[TIMER] && _isTimerCarry)
            _overflow = true;

        // about to overflow
        else if (_memory[TIMER] == 0xFF)
            _isTimerCarry = true;
    }

    _lastSignal = signal;

}


/*
    -- 
        find if the timer is enabled by
        checking the 2th bit in timer Controller 
    --
    * output: is timer enabled or not
*/
bool Timers::isTimerEnabled() const
{
    return Utils::isBitOn(_memory.read(CONTROL), 2);
}


/*
    -- get timer's frequency found in 0-1 bits of timer Controller --
    * output: timer's frequency
*/
u32 Timers::getFrequency() const
{
    return _FREQUENCIES[_memory.read(CONTROL) & 0b11];
}

std::ostream& operator<<(std::ostream& os, const Timers& timers)
{

    os.put(timers._lastTimer);
    os.put(timers._lastModulo);

    return os
        << timers._releaseOverflow << '\n'
        << timers._overflow << '\n'
        << timers._isTimerCarry << '\n'
        << timers._lastSignal << '\n'
        << timers._lastVisiblePulse;
}

std::istream& operator>>(std::istream& is, Timers& timers)
{
    timers._lastTimer = is.get();
    timers._lastModulo = is.get();

    return is
        >> timers._releaseOverflow
        >> timers._overflow
        >> timers._isTimerCarry
        >> timers._lastSignal
        >> timers._lastVisiblePulse;
}
