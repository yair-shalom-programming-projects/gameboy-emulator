#pragma once
#include <queue>
#include <SDL.h>
#include <string>

namespace SDL
{
    void updateEventQueues();
    void waitForInput();

    extern std::queue<SDL_Event> joypadEvents;
    extern std::queue<SDL_Event> menubarEvents;
    extern std::string fileDropped;
}