#pragma once
#include "Utils.h"


namespace Address
{

	inline constexpr u16 JOYPAD = 0xFF00;
	inline constexpr u16 DMA_TRANSFER = 0xFF46;


	namespace Memory
	{
		// memory map
		inline constexpr u16 ROM_BANK_0_START   = 0x0000, ROM_BANK_0_END   = 0x3FFF;
		inline constexpr u16 ROM_BANK_N_START   = 0x4000, ROM_BANK_N_END   = 0x7FFF;
		inline constexpr u16 RAM_BANK_N_START   = 0xA000, RAM_BANK_N_END   = 0xBFFF;
		inline constexpr u16 ECHO_START		    = 0xE000, ECHO_END		   = 0xFDFF;
		inline constexpr u16 SPRITE_TABLE_START = 0xFE00, SPRITE_TABLE_END = 0xFE9F;

		inline constexpr u16 OAM_BUG_START = 0xFE00, OAM_BUG_END = 0xFEFF;
		inline constexpr u16 ROM_BANK_SIZE = 0x4000;
	};

	namespace Cartridge
	{
		inline constexpr u16 MBC_TYPE = 0x0147;
		inline constexpr u16 ROM_TYPE = 0x0148;
		inline constexpr u16 RAM_TYPE = 0x0149;
	}

	namespace Int
	{
		inline constexpr u16 VBLANK = 0x0040;
		inline constexpr u16 LCD	= 0x0048;
		inline constexpr u16 TIMER  = 0x0050;
		inline constexpr u16 SERIAL = 0x0058;
		inline constexpr u16 JOYPAD = 0x0060;
		

		inline constexpr u16 REQUEST_REGISTER = 0xFF0F; // IF Register
		inline constexpr u16 ENABLED_REGISTER = 0xFFFF; // IE Register
	};


	namespace Timer
	{
		inline constexpr u16 INTERNAL_COUNTER = 0xFF03; // used for internal counter, originaly unmapped to memory 
		inline constexpr u16 DIVIDER_REGISTER = 0xFF04;

		inline constexpr u16 TIMER	 = 0xFF05; // TIMA 
		inline constexpr u16 MODULO  = 0xFF06; // TMA
		inline constexpr u16 CONTROL = 0xFF07; // TAC
	};


	namespace APU
	{

		inline constexpr u16 START = 0xFF10;
		inline constexpr u16 END = 0xFF3F;
		inline constexpr u16 WAVE_RAM_START = 0xFF30;
		inline constexpr u16 WAVE_RAM_END = 0xFF3F;

		// Volume Left/Right
		inline constexpr u16 NRx50 = 0xFF24;
		//  Selection of Sound output terminal
		inline constexpr u16 NRx51 = 0xFF25;
		//  Sound on/off
		inline constexpr u16 NRx52 = 0xFF26;

		inline constexpr u16 REG_CHANNEL_1_DATA = 0xFF19;
		inline constexpr u16 REG_CHANNEL_2_DATA = 0xFF14;
		inline constexpr u16 REG_CHANNEL_3_DATA = 0xFF1E;
		inline constexpr u16 REG_CHANNEL_4_DATA = 0xFF23;



		// in every channel the registers addresses are different 
		namespace SquareChannel1
		{
			inline constexpr u16 NRx0 = 0xFF10;
			inline constexpr u16 NRx1 = 0xFF11;
			inline constexpr u16 NRx2 = 0xFF12;
			inline constexpr u16 NRx3 = 0xFF13;
			inline constexpr u16 NRx4 = 0xFF14;
		}
		namespace SquareChannel2
		{
			inline constexpr u16 NRx0 = 0xFF15;
			inline constexpr u16 NRx1 = 0xFF16;
			inline constexpr u16 NRx2 = 0xFF17;
			inline constexpr u16 NRx3 = 0xFF18;
			inline constexpr u16 NRx4 = 0xFF19;
		}
		namespace WaveChannel
		{
			inline constexpr u16 NRx0 = 0xFF1A;
			inline constexpr u16 NRx1 = 0xFF1B;
			inline constexpr u16 NRx2 = 0xFF1C;
			inline constexpr u16 NRx3 = 0xFF1D;
			inline constexpr u16 NRx4 = 0xFF1E;
		}
		namespace NoiseChannel
		{
			inline constexpr u16 NRx0 = 0xFF1F;
			inline constexpr u16 NRx1 = 0xFF20;
			inline constexpr u16 NRx2 = 0xFF21;
			inline constexpr u16 NRx3 = 0xFF22;
			inline constexpr u16 NRx4 = 0xFF23;
		}
	}


	namespace LCD
	{
		inline constexpr u16 CONTROL			 = 0xFF40;
		inline constexpr u16 STATUS				 = 0xFF41;
		inline constexpr u16 LY					 = 0xFF44; // Current Scanline
		inline constexpr u16 DESIRE_SCANLINE	 = 0xFF45;
		inline constexpr u16 TILES_COLOR_PALLETE = 0xFF47;

		namespace Background
		{
			inline constexpr u16 OFFSET_Y = 0xFF42;
			inline constexpr u16 OFFSET_X = 0xFF43;
		};

		namespace Window
		{
			inline constexpr u16 OFFSET_Y = 0xFF4A;
			inline constexpr u16 OFFSET_X = 0xFF4B;
		};
	};

};