#pragma once
#include "IChannelWithEnvelope.h"


class Memory;

class NoiseChannel final : public IChannelWithEnvelope
{
public:
	explicit NoiseChannel(Memory& memory);
	
	virtual void clock(int cycles) override;
	virtual void triggerEvent(const u8 frameSequencer) override;
	virtual float getSample() override;
	virtual void setEnabled() override;
	virtual std::istream& setState(std::istream& is) override;
	virtual std::ostream& getState(std::ostream& os) const override;
	virtual void reset() override;

	void setNoiseData();
	
	friend std::istream& operator>>(std::istream& is, NoiseChannel& noiseChannel);
	friend std::ostream& operator<<(std::ostream& os, const NoiseChannel& noiseChannel);

private:
	u8 _divisor;
	u8 _widthMode;
	u16 _lfsr; // Linear Feedback Shift Register

};

