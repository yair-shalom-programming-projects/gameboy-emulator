#include "MBC2.h"
#include "Address.h"

using namespace Address::Memory;
using namespace Utils;


//? Constructors ?//

/* -- C'tor -- */
MBC2::MBC2() :
	MBC1()
{
	
}


//? Methods ?//


/* -- handle writing to specific addresses in MBC2 -- */
bool MBC2::write(const u16 address, const u8 data) 
{

	// program writes to ROM Bank 0 memory addresses to modify ROM Bank
	if (address >= ROM_BANK_0_START && address <= ROM_BANK_0_END)
	{
		handleBanking(address, data);
	}

	// write to RAM bank (if RAM enabled)
	else if (address >= RAM_BANK_N_START && address <= RAM_BANK_N_END)
	{
		if (_RAMEnabled)
		{
			// there are only 0x200 addresses in RAM so address should wrap
			u16 newAddress = (address - RAM_BANK_N_START) % 0x200;

			// RAM data is only 4 bits so upper nibble is 0xF
			_RAMBanks[0][newAddress] = (data & 0b1111) | 0xF0; 
		}
	}

	else
	{
		return false;
	}

	return true;
}

/* -- handle reading to specific addresses in MBC2 -- */
bool MBC2::read(const u16 address, u8& data) const
{

	// reading from ROM banks
	if (address >= ROM_BANK_0_START && address <= ROM_BANK_N_END)
	{
		data = _cartridge[ROM_BANK_0_START + (address % ROM_BANK_SIZE) + getROMBank(address) * ROM_BANK_SIZE];
	}
	
	// reading from RAM bank
	else if (address >= RAM_BANK_N_START && address <= RAM_BANK_N_END)
	{
		// there are only 0x200 addresses in RAM so address should wrap
		u16 newAddress = (address - RAM_BANK_N_START) % 0x200;
		data = _RAMEnabled ? _RAMBanks[0][newAddress] : 0xff;
	}

	else
	{
		return false;
	}

	return true;

}



//? Private Methods ?//

/* -- handle banking -- */
void MBC2::handleBanking(const u16 address, const u8 data) noexcept
{
	isBitOn(address, 8) ? setBank1(data) : setRAMEnabled(data);
}

/* -- get bank 1 (0-3 bits of data) if 0, inc to 1 -- */
void MBC2::setBank1(const u8 data) noexcept
{
	if (!(_bank1 = data & 0b1111))
		++_bank1;
}

/* 
	-- get ROM bank depending on the given address -- 
	reading from ROM bank 0 addresses, access bank 0.
	reading from ROM bank n addresses, access bank n.
*/
u8 MBC2::getROMBank(const u16 address) const noexcept
{
	
	// ROM Bank 0 address
	if (address >= ROM_BANK_0_START && address <= ROM_BANK_0_END)
		return 0;

	// ROM Bank n address
	// make sure bank is not bigger than ROM banks count
	return _bank1 & (_ROMBanksCount - 1);

}
