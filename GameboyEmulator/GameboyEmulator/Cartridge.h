#pragma once
#include <vector>
#include <string>
#include "Utils.h"
#include "Singleton.h"
#include "IMBC.h"
#include <memory>

class IMBC;

class Cartridge final : public Singleton<Cartridge>
{

public:
	
	// Constructors
	explicit Cartridge(Singleton_t);


	// Methods
	void loadCartridge(const std::string_view path);
	u16 getROMBanksCount() const;
	u16 getRAMBanksCount() const;
	void getMBC(std::unique_ptr<IMBC>& MBC) const;
	void reset();

	// Operators Overloads
	u8& operator[](const size_t& address);
	u8 operator[](const size_t& address) const;
	friend std::ostream& operator<<(std::ostream& os, const Cartridge& cartridge);
	friend std::istream& operator>>(std::istream& is, Cartridge& cartridge);

private:

	// Fields
	std::vector<u8> _cartridge;
};

