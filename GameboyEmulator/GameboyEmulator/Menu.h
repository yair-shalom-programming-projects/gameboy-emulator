#pragma once
#include <array>
#include <functional>
#include "Singleton.h"
#include <windows.h>
#include "mySDL.h"
#include "Color.h"
#include <fstream>

class Menu final : public Singleton<Menu>
{
public:
	
	// C'tor
	explicit Menu(Singleton_t, SDL_Window* window);

	// Methods
	static Menu& getInstance(SDL_Window* window = nullptr)
	{
		static Menu i{ Singleton_t(), window };
		return i;
	}

	// Methods
	operator HMENU() const noexcept { return _menu; };
	void handlePendingEvents();
	
private:
	
	static bool isStateFileSignatureValid(std::ifstream& file);
	static void writeStateFileSignature(std::ofstream& file);

	enum Option
	{
		SAVE_STATE, LOAD_STATE,
		SCALE_1X1, SCALE_2X2, SCALE_3X3, SCALE_4X4,
		PAUSE, RESET, LOAD_GAME, SCREENSHOT,
		REGULAR_COLORS, ORIGINAL_COLORS, Count 
	};

	using MenuHandlers = std::array<std::function<void(Menu*)>, Option::Count>;

	// Methods
	void construct();
	void initWindow();
	void addToWindow();

	void handleSaveState();
	void handleLoadState();
	void handleChangeScale(const size_t& scale);
	void handleLoadGame();
	void handlePause();
	void handleReset();
	void handleScreenShot();
	void handleChangeColorsType(const ColorsType& type, const Option& checkOptionId);

	// Fields
	static const MenuHandlers OPTION_HANDLERS;
	static const char* STATE_SIGNATURE;

	SDL_Window* _window;
	HWND _winapiWindow;
	HMENU _menu;

};

