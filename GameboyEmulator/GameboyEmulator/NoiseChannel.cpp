#include "NoiseChannel.h"
#include "Address.h"
NoiseChannel::NoiseChannel(Memory& memory):
    IChannelWithEnvelope(
        memory,
        Address::APU::NoiseChannel::NRx0,
        Address::APU::NoiseChannel::NRx1,
        Address::APU::NoiseChannel::NRx2,
        Address::APU::NoiseChannel::NRx3,
        Address::APU::NoiseChannel::NRx4
    ),
    _divisor(0), _lfsr(Const::APU::BITS_15_ON), _widthMode(0)
{
}

void NoiseChannel::clock(int cycles)
{

    _cyclesCount += cycles;
    if (_cyclesCount < _cyclesSampleUpdate) return;
    
    // The linear feedback shift register (LFSR)generates a pseudo - random bit sequence.
    // It has a 15 - bit shift register with feedback.
    // When clocked by the frequency timer, the low two bits(0 and 1) are XORed, all bits are shifted right by one, 
    // and the result of the XOR is put into the now - empty high bit.
    // If width mode is 1 (NR43), the XOR result is ALSO put into bit 6 AFTER the shift, resulting in a 7 - bit LFSR.
    const u8 bit0 = _lfsr & 0x1;
    const u8 bit1 = (_lfsr & 0x2) >> 1;
    const u8 xored = bit0 ^ bit1;

    _lfsr = (xored << 14) | (_lfsr >> 1);
    if (_widthMode)
        _lfsr = (xored << 6) | (_lfsr & 0x7FBF);
    
    
    _cyclesCount -= _cyclesSampleUpdate;
}

void NoiseChannel::triggerEvent(const u8 frameSequencer)
{
    // on trigger we reload lfsr to all 15 bits on
    _lfsr = Const::APU::BITS_15_ON;
    IChannelWithEnvelope::triggerEvent(frameSequencer);
}

float NoiseChannel::getSample()
{
    if (!_isDacOn) {
        return 0;
    }

    // if lfsr first bit is on then 
    return (_lfsr & 0x1) ? +_envelope.getVolume() : -_envelope.getVolume();
}

void NoiseChannel::setEnabled()
{
    _isRunning = _memory[Address::APU::NRx52] & Const::APU::POWER_FLAG_NOISE_CHANNEL;
}

std::istream& NoiseChannel::setState(std::istream& is)
{
    _divisor = is.get();
    _widthMode = is.get();
    is >> _lfsr;
    return IChannelWithEnvelope::setState(is);
}

std::ostream& NoiseChannel::getState(std::ostream& os) const 
{
    os.put(_divisor);
    os.put(_widthMode);
    os << _lfsr << '\n';
    return IChannelWithEnvelope::getState(os);
}

void NoiseChannel::reset()
{
    _divisor = 0;
    _lfsr = Const::APU::BITS_15_ON;
    _widthMode = 0;
    IChannelWithEnvelope::reset();
}



void NoiseChannel::setNoiseData()
{
    _divisor = Const::APU::AUDIO_DIVISOR_ARRAY[_memory[_NRx3] & Const::APU::FLAG_CHANNEL_DIVISOR];// the divisor code is in bits 1-3 of NRx3
    _widthMode = _memory[_NRx3] & Const::APU::FLAG_CHANNEL_WIDTH_FLAG; // the width flag is found in the NRx3 4th bit

    // The noise channel's frequency timer period is set by a base divisor shifted left some number of bits.
    _cyclesSampleUpdate = _divisor << ((_memory[_NRx3] & Const::APU::FLAG_CHANNEL_CLOCK_SHIFT) >> 4); 
}

std::istream& operator>>(std::istream& is, NoiseChannel& noiseChannel)
{
    return noiseChannel.setState(is);
}

std::ostream& operator<<(std::ostream& os, const NoiseChannel& noiseChannel)
{
    return noiseChannel.getState(os);
}
