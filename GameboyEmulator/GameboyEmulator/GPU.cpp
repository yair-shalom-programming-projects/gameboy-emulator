#include "GPU.h"
#include <SDL.h>
#include "CannotCreateWindowException.h"
#include <iostream>


using Address::Memory::SPRITE_TABLE_START;
using namespace Address::LCD;
using namespace Const::LCD;
using namespace Utils;


Mode GPU::_mode = Mode::HBLANK;
u8   GPU::_currentRow = 0;


//? Constructors ?//


/* -- C'tor -- */
GPU::GPU(Singleton_t) :
    _memory(Memory::getInstance()),
    _interrupts(Interrupts::getInstance()),
    _gui(GUI::getInstance()),

    _shouldUseOGColors(false)
{
    reset();
}

//? Methods ?//

void GPU::reset()
{
    resetScreen();

    _mode = Mode::HBLANK;
    _currentRow = 0;

    _clocks = 0;
    _scanline = 0;
    _firstFrame = true;
    _searchingSpritesFinished = false;
    _increasedLY = true;
}

std::string GPU::getGameFileFromUser()
{
    return _gui.getGameFileFromUser();
}

void GPU::renderHelloScreen()
{
    _gui.renderHelloScreen();
}



/*
    -- updates GPU --
    * input: number of clocks made at the current M-Cycle
*/
void GPU::update(u8 clocks)
{

    setLCDStatus();

    if (!isLCDEnabled()) return;
   
    _clocks += clocks;

    // LY register is increased 6 clocks before the line is drawn
    if (_clocks >= CLOCKS_TO_DRAW_LINE - 6 && _increasedLY)
    {
        ++_memory[LY];
        _increasedLY = false;
    }

    if (_clocks >= getClocksToDrawLine())
    {

        _clocks -= getClocksToDrawLine();
        _scanline = _memory[LY];
        _increasedLY = true;


        // move to VBLANK
        if (_scanline == MAX_VISIBLE_SCANLINE)
        {
            _firstFrame = false;
            _interrupts.request(Interrupt::VBLANK);
        }

        if (_scanline > MAX_INVISIBLE_SCANLINE)
        {
            _scanline =_memory[LY] = 0;
        }

        // first LCD first is not drawn
        if (_scanline < MAX_VISIBLE_SCANLINE && !_firstFrame)
        {       
            drawScanline();
        }

        setLCDStatus();
        clocks = _clocks;

    }

    if (isMode(Mode::OAM) && !_searchingSpritesFinished)
    {
        _currentRow += clocks / 4;

        if (_currentRow == 0xff)
            _currentRow = clocks / 4;

        else if (_currentRow >= 20)
        {
            _searchingSpritesFinished = true;
            _currentRow = 0xff;
        }
    }
    else
    {
        _currentRow = 0xff;
        _searchingSpritesFinished = false;
    }

}

void GPU::handleInput()
{
    _gui.update();
}

/*
    LCD Status Register
    Bit 0-1: Mode - 00 H-Blank
                    01 V-Blank
                    10 Searching Sprites Atts
                    11 Transfering Data to LCD Driver
    Bit 2: Conicidence flag
    Bit 3: Mode 0 Interrupt Enabled
    Bit 4: Mode 1 Interrupt Enabled
    Bit 5: Mode 2 Interrupt Enabled
*/
void GPU::setLCDStatus()
{

    if (!isLCDEnabled())
    {
        _firstFrame = true;
        _increasedLY = true;
        _currentRow = 0xff;
        
        // reset _clocks & scanline
        _clocks = 0;
        _memory[LY] = 0;
        _scanline = 0;

        // set mode to HBLANK
        setMode(Mode::HBLANK);

        return;
    }

    // in the first scanline of the first frame, OAM mode is shorter by 4 clocks
    const u16 oamModeEnd = Status::Mode::Cycles::OAM;
    const u16 transferModeEnd = oamModeEnd + Status::Mode::Cycles::TRANSFER;

    const Mode lastMode = _mode;

    u8& status = _memory[STATUS];
    bool requestInterrupt = false;


    // time to change to VBlank mode
    if (_scanline >= MAX_VISIBLE_SCANLINE)
    {
        setMode(Mode::VBLANK);
        requestInterrupt = isBitOn(status, Status::Bit::VBLANK_INT_ENABLED);
    }

    // time to change to OAM Search mode
    else if (_clocks <= oamModeEnd)
    {
        setMode(Mode::OAM);
        requestInterrupt = isBitOn(status, Status::Bit::OAM_INT_ENABLED);
    }

    // time to change to Transfer To LCD mode
    else if (_clocks <= transferModeEnd)
    {
        setMode(Mode::TRANSFER_TO_LCD);
    }
    
    // time to change to HBlank mode
    else
    {
        setMode(Mode::HBLANK);
        requestInterrupt = isBitOn(status, Status::Bit::HBLANK_INT_ENABLED);
    }


    // just entered a new mode so request interupt
    if (requestInterrupt && lastMode != _mode)
        _interrupts.request(Interrupt::LCD);


    // if the current scanline is equal to the game's desired scanline, Conicidence Flag is set
    // and if the Conicidence Interrupt bit in status is enabled, request LCD Interrupt. 
    // else reset Conicidence Flag
    if (_scanline == _memory[DESIRE_SCANLINE])
    {
        status = setBit(status, Status::Bit::CONICIDENCE_FLAG);

        if (isBitOn(status, Status::Bit::CONICIDENCE_INT_ENABLED))
            _interrupts.request(Interrupt::LCD);
    }
    else
    {
        status = resetBit(status, Status::Bit::CONICIDENCE_FLAG);
    }


}

/*
    -- return if lcd is enabled by returning the 7th bit in LCD Control Register --
*/
bool GPU::isLCDEnabled() const
{
    return isBitOn(_memory[CONTROL], Control::Bit::LCD_ENABLED);
}

/* -- setting new mode and update memory -- */
void GPU::setMode(const Mode mode)
{
    _mode = mode;
    _memory[STATUS] = (_memory[STATUS] & 0b11111100) | static_cast<u8>(_mode);
}


/*
    -- get the number of clocks each scanline takes --
    Note: if it is the first scanline in the first frame, 
    clocks count is shorter by 1 M-cycle
*/
u16 GPU::getClocksToDrawLine() noexcept
{
    return CLOCKS_TO_DRAW_LINE - (!_scanline && _firstFrame ? 4 : 0);
}


/*
    -- render tiles and sprites (if enabled) --
*/
void GPU::drawScanline()
{

    const u8 control = _memory[CONTROL];
    
    if (isBitOn(control, Control::Bit::DISPLAY_ENABLED))
        renderTiles(control);
    
    if (isBitOn(control, Control::Bit::SPRITES_ENABLED))
        renderSprites(control);

}


/*
    -- get the color type of the colorID in the given palette --
    each two bits in the palette represent a color.
    colorID is the "index" in the palette of the desired color.
    palettes aren't fixed so the programer can create cool efects.
    * input: color's ID in the palette, palette
    * output: color
*/
u8 GPU::getColorType(const u8 colorID, const u8 palette) const 
{

    u8 high = 0, low = 0;

    // which bits of the color palette does the color id map to?
    switch (colorID)
    {
        case 0: high = 1; low = 0; break;
        case 1: high = 3; low = 2; break;
        case 2: high = 5; low = 4; break;
        case 3: high = 7; low = 6; break;
    }

    // use the palette to get the color
    return (isBitOn(palette, high) << 1) | isBitOn(palette, low);

}


/// <summary>
/// 
/// Tiles (8x8) - tiles are what form the background and are not interactive.
///               tiles ID's are stored in backgroundMemory. With the tileID 
///               we look up to tileDataLocation to find tile's data.
///               each pixel in the tile represented by his color and each color is 
///               2 bits (4 color option) so the size in memory of 1 line in tile is 8x2 (2 bytes).
///               pixel 0 in the tileData is the 7th bit of data1 and data2. Pixel 1 is bit 6 etc..
/// 
/// Background (256x256) (32x32 tiles) - the visual display can show any 160x144 pixels of the 256x256 
///                                      background, this allows for scrolling the viewing area over the background.
/// 
/// Window - this is a layer on the background but behind the sprites. enables fixed panel over the background that does not scroll.
/// 
/// </summary>
void GPU::renderTiles(const u8 control)
{

    // where to draw the visual area and the window
    const u8 scrollY = _memory[Background::OFFSET_Y];
    const u8 scrollX = _memory[Background::OFFSET_X];
    const u8 windowY = _memory[Window::OFFSET_Y];  
    const u8 windowX = _memory[Window::OFFSET_X] - 7; // -7 is necessary

    // is the window enabled? is the current scanline within the windows Y pos?
    const bool usingWindow = isBitOn(control, Control::Bit::WINDOW_ENABLED) && windowY <= _scanline;


    // which tile data are we using?
    const u16 tileData = isBitOn(control, Control::Bit::TILES_DATA_MAP) ? 0x8000 : 0x8800;

    // 0x8000 memory region uses unsigned bytes as tile identifiers
    const bool isUnsigned = tileData == 0x8000;


    // which background memory? 
    const u16 backgroundMemory = isBitOn(control, usingWindow ? Control::Bit::BG_WINDOW_TILES_MAP : Control::Bit::BG_TILES_MAP) ? 0x9C00 : 0x9800;


    // yPos is used to calculate which of the vertical tiles the current scanline is drawing
    // line is reduced when usingWindow becuase it is reading from WINDOWS_TILES_MAP    
    const u8 yPos = usingWindow ? (_scanline - windowY) : (scrollY + _scanline);

    // which of the TILE_SIZE vertical pixels of the current
    // tile is the scanline on?
    const u16 tileRow = ((u8)(yPos / Tile::TILE_SIZE)) * Tile::TILES_COUNT;


    // time to start drawing the width horizontal pixels for this scanline
    for (u8 pixel = 0; pixel < WIDTH; ++pixel)
    {

        const u8 xPos = (usingWindow && pixel >= windowX) ? (pixel - windowX) : (pixel + scrollX);


        // which of NUM_OF_TILES horizontal tiles does this xPos fall within?
        const u16 tileColumn = xPos / Tile::TILE_SIZE;

        // get the tile identity number. Remember it can be signed
        // or unsigned
        const u16 tileIDAddress = backgroundMemory + tileRow + tileColumn;

        const short tileID = isUnsigned ? _memory[tileIDAddress] : (char)_memory[tileIDAddress];

        // deduce where this tile identifier is in memory
        const u16 tileDataLocation = tileData + (isUnsigned ? (tileID * 16) : ((tileID + 128) * 16));

        // find the correct vertical line. each line is 2 bytes
        const u8 line = (yPos % 8) * 2;

        const u8 data1 = _memory[tileDataLocation + line];
        const u8 data2 = _memory[tileDataLocation + line + 1];

        // pixel 0 in the tile is the 7th bit in data1 and data2.
        // Pixel 1 is the 6th etc..
        const u8 colorBit = ((xPos % 8) - 7) * -1;

        // combine data 2 and data 1 to get the color id for this pixel in the tile
        const u8 colorID = (isBitOn(data2, colorBit) << 1) | isBitOn(data1, colorBit); 

        _frame[_scanline][pixel] = getColorType(colorID, _memory[TILES_COLOR_PALLETE]);

    }

}


/// <summary>
/// Sprites (8x8 or 8x16) - sprites are what forms the interactive GPU on the display.
///                         There are 40 of them, and each one got an ID found in Sprites Table address and 
///                         with their ID we find the their data addresses. 
///                         Sprites pixels are stored the same way as tiles pixels.
/// </summary>
void GPU::renderSprites(const u8 control)
{

    const u8 ySize = isBitOn(control, Control::Bit::SPRITE_SIZE) ? 16 : 8;

    for (u8 sprite = 0; sprite < Sprite::SPRITES_COUNT; ++sprite)
    {

        // sprite occupies SPRITES_ATTR_SIZE bytes in the sprite attributes table
        const u8 offset = sprite * Sprite::SPRITES_ATTR_SIZE;

        const u8 yPos       = _memory[SPRITE_TABLE_START + offset + 0] - 16; // -16 is necessary
        const u8 xPos       = _memory[SPRITE_TABLE_START + offset + 1] - 8;  // -8  is necessary
        const u8 spriteID   = _memory[SPRITE_TABLE_START + offset + 2];
        const u8 attributes = _memory[SPRITE_TABLE_START + offset + 3];
          

        // does this sprite intercept with the scanline?
        if (_scanline >= yPos && _scanline < yPos + ySize)
        {

            const bool yFlip = isBitOn(attributes, Sprite::AttrBit::Y_FLIP);
            const bool xFlip = isBitOn(attributes, Sprite::AttrBit::X_FLIP);

            int line = _scanline - yPos;

            // flip sprite backwards in y axis
            if (yFlip)
            {
                //line = ysize - 1 - line;
                line -= ySize;
                line *= -1;
            }


            line *= 2; // each line 2 bytes same as for tiles
            const u16 dataAddress = 0x8000 + (spriteID * Sprite::SPRITE_SIZE) + line;
            const u8 data1 = _memory[dataAddress];
            const u8 data2 = _memory[dataAddress + 1];



            for (u8 spritePixel = 0; spritePixel < Sprite::SPRITE_WIDTH; ++spritePixel)
            {

                // pixel 0 in the sprite is it 7 of data 1 and data2.
                // Pixel 1 is bit 6 etc..
                const u8 colorbit = xFlip ? spritePixel : (7 - spritePixel);

                // combine data 2 and data 1 to get the color id for this pixel in the sprite
                const u16 paletteAddress = isBitOn(attributes, Sprite::AttrBit::PALLETE_ADDRESS) ? 0xFF49 : 0xFF48;
                const u8 colorID = (isBitOn(data2, colorbit) << 1) | isBitOn(data1, colorbit);

                // colorID 0 is transparent
                if (!colorID) continue;

                const u8 pixel = xPos + spritePixel;

                if (pixel >= Const::LCD::WIDTH)
                    continue;

                // if the pixel is not behind background or the background is transparent, print sprite
                if (!isBitOn(attributes, Sprite::AttrBit::SPRITES_BG_PRIORITY) ||
                    _frame[_scanline][pixel] == 0)
                {
                    _frame[_scanline][pixel] = getColorType(colorID, _memory[paletteAddress]);
                }

            }
        }
    }
}


/* -- render screen -- */
void GPU::renderFrame()
{
    _gui.renderFrame(_frame);
}


/* -- reset screen to 0 color index -- */
void GPU::resetScreen()
{
    for (auto& row : _frame)
        row.fill(0);
}

std::ostream& operator<<(std::ostream& os, const GPU& gpu)
{
    for (const auto& y : gpu._frame)
        for (const auto& colorIndex : y)
            os.put(colorIndex);
    
    os.put(static_cast<u8>(gpu._mode));
    os.put(gpu._currentRow);

    return os
        << gpu._clocks << '\n'
        << gpu._scanline << '\n'
        << gpu._firstFrame << '\n'
        << gpu._searchingSpritesFinished << '\n'
        << gpu._increasedLY;
}

std::istream& operator>>(std::istream& is, GPU& gpu)
{
    for (auto& y : gpu._frame)
        for (auto& colorIndex : y)
            colorIndex = is.get();
    
    gpu._mode = static_cast<Mode>(is.get());
    gpu._currentRow = is.get();

    return is  
        >> gpu._clocks
        >> gpu._scanline
        >> gpu._firstFrame
        >> gpu._searchingSpritesFinished
        >> gpu._increasedLY;
}
