#include "SquareChannel1.h"
#include "Address.h"
#include <iostream>

SquareChannel1::SquareChannel1(Memory& memory):
	ISquareChannel(
		memory,
		Address::APU::SquareChannel1::NRx0,
		Address::APU::SquareChannel1::NRx1,
		Address::APU::SquareChannel1::NRx2,
		Address::APU::SquareChannel1::NRx3,
		Address::APU::SquareChannel1::NRx4
	)
{
}

void SquareChannel1::reset()
{
	setSweep();
	_shadow = 0;
	_sweepEnabled = true;
	ISquareChannel::reset();
}

void SquareChannel1::triggerEvent(const u8 frameSequencer)
{
	// set the new frequency
	ISquareChannel::triggerEvent(frameSequencer);
	_shadow = getFrequency(); // reload frequency from the memory
	_didSweepNegate = false; // set the flag of did the sweep negate to false
	_periodTimer = _period ? _period : 8;// reload the period
	
	setEnabledSweepFlag(); // set the sweep enabled flag
	if (_sweepShift) { // if the sweep shift is enabled again we do an overflow check
		calculateFrequency();
	}
}

std::istream& SquareChannel1::setState(std::istream& is)
{
	is >> _shadow 
	   >> _didSweepNegate;

	_period = is.get();
	_periodTimer = is.get();
	_sweepShift = is.get();

	is >> _shadow;
	bool _sweepEnabled;
	bool _sweepNegate;
	bool _didSweepNegate;

	return ISquareChannel::setState(is);
}

std::ostream& SquareChannel1::getState(std::ostream& os) const
{
	os << _shadow << '\n'
	   << _didSweepNegate << '\n';
	return ISquareChannel::getState(os);
}


void SquareChannel1::setEnabled()
{
	_isRunning = _memory[Address::APU::NRx52] & Const::APU::POWER_FLAG_SQUARE_CHANNEL_1;
}

void SquareChannel1::sweepTick()
{
	if (!(--_periodTimer)) { 
		// reload period
		if (!_period) {
			_periodTimer = 8;
			return;
		}
		_periodTimer = _period;

		if (!_sweepEnabled) return; // if sweep is not enabled do nothing
		setFrequencySweep();
	}
}


void SquareChannel1::setSweep() {
	// if at least 1 sweep negate has happened since last trigger,
	// and negate is now cleared, then disable ch1.
	if (
		_didSweepNegate &&
		_sweepNegate &&
		!(_memory[_NRx0] & Const::APU::FLAG_CHANNEL_SWEEP_DIRECTION)
		) 
	{
		_isRunning = false;
	}
	// setting sweep data
	_period = (_memory[_NRx0] & Const::APU::FLAG_CHANNEL_SWEEP_PERIOD) >> 4; // bits 6-8 of NRx0 are the value of the period
	_sweepNegate = _memory[_NRx0] & Const::APU::FLAG_CHANNEL_SWEEP_DIRECTION; // bit 5th of NRx0 is the sweep direction
	_sweepShift = _memory[_NRx0] & Const::APU::FLAG_CHANNEL_SWEEP_SHIFT;// bits 1-4 of NRx0 are for the value of the shift
}

void SquareChannel1::setEnabledSweepFlag()
{
	_sweepEnabled = _period || _sweepShift; // if either of period or sweep shift is not zero then the sweep is enabled
}

void SquareChannel1::writeFrequencyToMemory()
{


	// AND 0xF8 deletes old frequency from the data and 0x700 takes the upper 3 bits of the frequency
	_memory[_NRx4] = (_memory[_NRx4] & 0xF8) | ((_frequency & 0x700) >> 8);
	_memory[_NRx3] =  _frequency & 0xFF;
}


void SquareChannel1::setFrequencySweep()
{
	u16 newFrequency = calculateFrequency(); // get new frequency
	if (newFrequency > Const::APU::MAX_FREQUENCY || !_sweepShift) { // if the sweep shift is zero or the new frequency is not valid then do not update current frequency
		return;
	}
	_frequency = newFrequency;
	_shadow = newFrequency;
	writeFrequencyToMemory();

	// overflow check
	calculateFrequency(); 
}

u16 SquareChannel1::calculateFrequency()
{
	u16 change = (_shadow >> _sweepShift);
	u16 helper = 0;
	
	if (_sweepNegate) {
		helper = _shadow - change;
		_didSweepNegate = true;
	}
	else {
		helper = _shadow + change;
	}
	
	// if the new frequency is over the max then there is an overflow and the channel is disabled
	if (helper > Const::APU::MAX_FREQUENCY) {
		_isRunning = false;
	}

	return helper;
}

std::istream& operator>>(std::istream& is, SquareChannel1& channel1)
{
	return channel1.setState(is);
}

std::ostream& operator<<(std::ostream& os, const SquareChannel1& channel1)
{
	return channel1.getState(os);
}
