#include "CPU.h"
#include <bitset>
#include <iostream>
#include <exception>
#include <fstream>
#include "Interrupts.h"
#include "Utils.h"
#include "GPU.h"

using std::exception;
using namespace Utils;


//? CONSTRUCTORS ?//

/* -- c'tor -- */	
CPU::CPU(Singleton_t) :
	_memory(Memory::getInstance()),
	_interrupts(Interrupts::getInstance(this))
{
	reset();
	initInstructions();
	initPrefixInstructions();
}


//? PUBLIC METHODS ?//

/* -- execute the next instruction -- */
u8 CPU::execute()
{

	// interrupt cannot be in the middle of M-Cycle or prefix instruction
	if (_currentCycle == 1 && !_nextPrefix)
		_interrupts.doInterrupts();
	
	if (_interrupts.isServing())
		return _interrupts.serve();


	if (_enableIME)
	{
		Interrupts::IME = true;
		_enableIME = false;
	}

	if (!_halted)
	{
		if (_currentCycle == 1)
		{

			_instruction = _memory.read(_doHaltBug ? _PC : _PC++, !_doHaltBug);

			// only two prefix instructions (CB XX)
			if (_nextPrefix && ++_nextPrefix > 2)
				_nextPrefix = 0;

			_doHaltBug = false;
		}
			
		_nextPrefix ? _prefixInstructions[_instruction]() : _instructions[_instruction]();
	}


	return _cycles;

}

void CPU::status()
{
	std::fstream file("logg.txt", std::ios_base::app);

	file << std::hex
		<< "\naf= " << _A.join(_F)
		<< "\nbc= " << _B.join(_C)
		<< "\nde= " << _D.join(_E)	
		<< "\nhl= " << _H.join(_L)
		<< "\nsp= " << _SP
		<< "\npc= " << _PC
		<< "\ninst= " << (int)_memory.read(_PC) << "\n"
		<< "\currentCycle= " << (int)_currentCycle << "\n\n";

}

void CPU::reset()
{
	_halted = false;
	_nextPrefix = 0;

	_cycles = 0;
	_currentCycle = 1;
	_instruction = 0;

	_A = 0x01; _F = 0xB0;
	_B = 0x00; _C = 0x13;
	_D = 0x00; _E = 0xD8;
	_H = 0x01; _L = 0x4d;
	_SP = 0xFFFE; _PC = 0x100;

	_enableIME = false;
	_doHaltBug = false;
}

void CPU::setPC(const u16 newPC) noexcept
{
	_PC = newPC;
	_currentCycle = 1;
}




/* -- push value to stack -- */
void CPU::pushStack(const u16 value)
{
	// push trigger write memory corruption
	_memory.doOAMBug(_SP, OAM_BUG_TYPE::WRITE);
	
	_memory.write(--_SP, value >> 8);
	_memory.write(--_SP, value);
}

void CPU::pushStack(const u8 value)
{
	_memory.write(--_SP, value);
}

/* -- pop value from stack -- */
u16 CPU::popStack()
{
	
	// pop will trigger OAM bug only 3 times (read, glitched write, read) not 4!
	u16 value = _memory.read(_SP++, true);
	value |= _memory.read(_SP++) << 8;
	
	return value;
}


//? PRIVATE METHODS ?//


u8 CPU::popStackHigh()
{
	return _memory.read(_SP++);
}

u8 CPU::popStackLow()
{
	// pop low trigger oam inc read corruption
	return _memory.read(_SP++, true);
}


/* -- inc the given 8bit regiser -- */
void CPU::inc8(Register& reg)
{

	++reg;

	_F[Flag::ZERO] = !reg;
	_F[Flag::HALF_CARRY] = !(reg & 0x0F);
	_F[Flag::SUBTRACT] = 0;

	_cycles = 4;

}

/*
	-- inc 16 bit registers and checks for the OAM Bug --
	* input: address, reg1 (high), reg2 (low)
*/
u16 CPU::inc16(Register& reg1, Register& reg2)
{
	// increase trigger write memory corruption
	_memory.doOAMBug(reg1.join(reg2), OAM_BUG_TYPE::WRITE);

	return reg1.add16(reg2);
}

u16 CPU::inc16(u16& value)
{
	// increase trigger write memory corruption
	_memory.doOAMBug(value, OAM_BUG_TYPE::WRITE);

	return ++value;
}


/* -- dec the given 8bit regiser -- */
void CPU::dec8(Register& reg)
{

	--reg;

	_F[Flag::ZERO] = !reg;
	_F[Flag::HALF_CARRY] = (reg & 0x0F) == 0x0F;
	_F[Flag::SUBTRACT] = 1;

	_cycles = 4;

}

u16 CPU::dec16(Register& high, Register& low)
{
	// decrease trigger write memory corruption
	_memory.doOAMBug(high.join(low), OAM_BUG_TYPE::WRITE);

	return high.subtract16(low);
}

u16 CPU::dec16(u16& value)
{
	// decrease trigger write memory corruption
	_memory.doOAMBug(value, OAM_BUG_TYPE::WRITE);

	return --value;
}


/* -- sub the given value from _A register -- */
void CPU::sub8(u8 value, const bool subCarry)
{

	u8 helper = (subCarry ? _F[Flag::CARRY] : 0);
	int32_t signedRes = static_cast<int32_t>(_A - value - helper);
	u8 res = static_cast<u8>(signedRes);

	_F[Flag::ZERO] = !res;
	_F[Flag::CARRY] = signedRes < 0;
	_F[Flag::HALF_CARRY] = ((_A & 0xF) - (value & 0xF) - helper) < 0;
	_F[Flag::SUBTRACT] = 1;

	_A = res;

	_cycles = 4;

}


/* -- add 8bit value to the given register -- */
void CPU::add8(Register& reg, u8 value, const bool addCarry)
{

	u8 helper = addCarry ? _F[Flag::CARRY] : 0;
	u16 fullRes = reg + value + helper;
	u8 res = static_cast<u8>(fullRes);

	_F[Flag::ZERO] = !res;
	_F[Flag::SUBTRACT] = 0;
	_F[Flag::HALF_CARRY] = ((reg & 0xF) + (value & 0xF) + helper) > 0xF;
	_F[Flag::CARRY] = fullRes > 0xFF;

	reg = res;

	_cycles = 4;

}

/* -- add 16bit value to the given registers -- */
void CPU::add16(Register& reg1, Register& reg2, const u16 value)
{
	u32 fullResult = reg1.join(reg2) + value;
	u16 result = fullResult;

	_F[Flag::SUBTRACT] = 0;
	_F[Flag::HALF_CARRY] = (reg1.join(reg2) ^ value ^ (fullResult & 0xFFFF)) & 0x1000;
	_F[Flag::CARRY] = fullResult > 0xFFFF;

	reg1.set16(reg2, result);
	_cycles = 8;

}

/* -- execute AND operator on the given value and register _A (result saved in _A) -- */
void CPU::and8(const u8 value)
{

	_A &= value;

	_F[Flag::ZERO] = !_A;
	_F[Flag::SUBTRACT] = 0;
	_F[Flag::HALF_CARRY] = 1;
	_F[Flag::CARRY] = 0;

	_cycles = 4;

}

/* -- execute XOR operator on the given value and register _A (result saved in _A) -- */
void CPU::xor8(const u8 value)
{
	_A ^= value;

	_F[Flag::ZERO] = !_A;
	_F[Flag::SUBTRACT] = 0;
	_F[Flag::HALF_CARRY] = 0;
	_F[Flag::CARRY] = 0;

	_cycles = 4;
}

/* -- execute OR operator on the given value and register _A (result saved in _A) -- */
void CPU::or8(const u8 value)
{

	_A |= value;

	_F[Flag::ZERO] = !_A;
	_F[Flag::SUBTRACT] = 0;
	_F[Flag::HALF_CARRY] = 0;
	_F[Flag::CARRY] = 0;

	_cycles = 4;

}

/*
	-- compare register _A to the given value --
	Special flags values:
	* Flag Zero is on if they're equal
	* Flag Carry is on if the value is bigger than _A
*/
void CPU::cp8(const u8 value)
{

	_F[Flag::ZERO] = _A == value;
	_F[Flag::SUBTRACT] = 1;
	_F[Flag::HALF_CARRY] = (_A & 0xF) - (value & 0xF) < 0;
	_F[Flag::CARRY] = _A < value;

	_cycles = 4;

}



//? PREFIX INSTRUCTIONS METHODS ?//

/*
	-- Rotate Left Carry --
	* input: register to rotate
*/
void CPU::rlc(Register& reg)
{

	_F[Flag::CARRY] = reg[7];

	reg = (reg << 1) | reg[7];


	_F[Flag::ZERO] = !reg;
	_F[Flag::HALF_CARRY] = 0;
	_F[Flag::SUBTRACT] = 0;

	_cycles = 4;
}


/*
	-- Rotate Right Carry --
	* input: register to rotate
*/
void CPU::rrc(Register& reg)
{

	_F[Flag::CARRY] = reg[0];
	reg = (reg[0] << 7) | (reg >> 1);

	_F[Flag::ZERO] = !reg;
	_F[Flag::HALF_CARRY] = 0;
	_F[Flag::SUBTRACT] = 0;


	_cycles = 4;

}



/*
	-- Rotate Left --
	* input: register to rotate
*/
void CPU::rl(Register& reg)
{

	const bool reg0 = reg[7];
	reg = (reg << 1) | _F[Flag::CARRY];

	_F[Flag::ZERO] = !reg;
	_F[Flag::HALF_CARRY] = 0;
	_F[Flag::SUBTRACT] = 0;
	_F[Flag::CARRY] = reg0;

	_cycles = 4;

}


/*
	-- Rotate Right --
	* input: register to rotate
*/
void CPU::rr(Register& reg)
{

	const bool reg0 = reg[0];
	reg >>= 1;
	reg[7] = _F[Flag::CARRY];

	_F[Flag::ZERO] = !reg;
	_F[Flag::SUBTRACT] = 0;
	_F[Flag::HALF_CARRY] = 0;
	_F[Flag::CARRY] = reg0;

	_cycles = 4;
}



/*
	-- Shift Left the given register into Carry --
	* input: register to shift
*/
void CPU::sla(Register& reg)
{

	_F[Flag::CARRY] = reg[7];
	reg <<= 1;
	_F[Flag::ZERO] = !reg;
	_F[Flag::SUBTRACT] = 0;
	_F[Flag::HALF_CARRY] = 0;

	_cycles = 4;
}


/*
	-- Shift Right the given register into Carry (MSB doesn't change)--
	* input: register to shift
*/
void CPU::sra(Register& reg)
{

	_F[Flag::CARRY] = reg[0];

	reg = (reg[7] << 7) | (reg >> 1);

	_F[Flag::ZERO] = !reg;
	_F[Flag::SUBTRACT] = 0;
	_F[Flag::HALF_CARRY] = 0;

	_cycles = 4;
}


/*
	-- Shift Right the given register into Carry --
	* input: register to shift
*/
void CPU::srl(Register& reg)
{

	_F[Flag::CARRY] = reg[0];
	reg >>= 1;
	_F[Flag::ZERO] = !reg;
	_F[Flag::SUBTRACT] = 0;
	_F[Flag::HALF_CARRY] = 0;

	_cycles = 4;
}


/*
	-- Swap the given register's lower & upper nibbles
	   (first and last 4 bit) --
	^ example: 0010 1000 -> 1000 0010
	* input: register to shift
*/
void CPU::swap(Register& reg)
{

	reg = (reg & 0xF) << 4 | (reg & 0xF0) >> 4;

	_F[Flag::ZERO] = !reg;
	_F[Flag::SUBTRACT] = 0;
	_F[Flag::HALF_CARRY] = 0;
	_F[Flag::CARRY] = 0;

	_cycles = 4;
}



/*
	-- implement bit instruction --
	* input: bit, register
*/
void CPU::bit(const u8 bit, const Register& reg)
{
	_F[Flag::ZERO] = !reg[bit];
	_F[Flag::SUBTRACT] = 0;
	_F[Flag::HALF_CARRY] = 1;

	_cycles = 4;
}

/*
	--
	impementation of bit instruction of HL.
	In >4 cycles instructions, each read() should execute (update timer, Interrupts...)
	in a seperate cycle (4 cycles)
	--
*/
void CPU::bitHL(const u8 bit)
{

	switch (_currentCycle)
	{
	case 1:
		// read Instruction
		break;

	case 2:
		CPU::bit(bit, _memory.read(_H.join(_L)));
		break;
	}

	_cycles = 4;
	_currentCycle = _currentCycle == 2 ? 1 : _currentCycle + 1;
}

/*
	-- reset the given bit of the given register --
	* input: bit, register
*/
void CPU::res(const u8 bit, Register& reg)
{
	reg[bit] = 0;
	_cycles = 4;
}

/*
	-- set the given bit of the given register --
	* input: bit, register
*/
void CPU::set(const u8 bit, Register& reg)
{
	reg[bit] = 1;
	_cycles = 4;
}

/*
	-- do an arithmic method on HL register --
	* input: method
*/
void CPU::doHL(void(CPU::* method)(Register&))
{

	switch (_currentCycle)
	{
	case 1:
		// read Instruction
		break;

	case 2:
	{
		Register helper(_memory.read(_H.join(_L)));
		(this->*method)(helper);
		_container = helper;
		break;
	}
	case 3:
		_memory.write(_H.join(_L), _container);		
		break;
	}

	_cycles = 4;
	_currentCycle = _currentCycle == 3 ? 1 : _currentCycle + 1;
}

/*
	-- set/reset bit in HL register --
	* input: bit to modify, turnOn - turn the bit on or off
*/
void CPU::setHL(const u8 bit, const bool turnOn)
{

	switch (_currentCycle)
	{
	case 1:
		// read Instruction
		break;

	case 2:
		_container = turnOn ? Utils::setBit(_memory.read(_H.join(_L)), bit) :
					    Utils::resetBit(_memory.read(_H.join(_L)), bit);
		break;

	case 3:
		_memory.write(_H.join(_L), _container);
		break;
	}

	_cycles = 4;
	_currentCycle = _currentCycle == 3 ? 1 : _currentCycle + 1;

}


void CPU::ld_rr_d16(Register& high, Register& low)
{

	switch (_currentCycle)
	{
	case 1:
		// read Instruction
		break;

	case 2:
		_container = _memory.read(_PC, true);
		break;

	case 3:
		high = _memory.read(++_PC, true);
		low = _container;
		++_PC;
		break;
	}

	_cycles = 4;
	_currentCycle = _currentCycle == 3 ? 1 : _currentCycle + 1;
}

void CPU::ld_rr_d16(u16& registers)
{
	switch (_currentCycle)
	{
	case 1:
		// read Instruction
		break;

	case 2:
		_container = _memory.read(_PC, true);
		break;

	case 3:
		registers = join(_memory.read(++_PC, true), _container);
		++_PC;
		break;
	}

	_cycles = 4;
	_currentCycle = _currentCycle == 3 ? 1 : _currentCycle + 1;
}

void CPU::ld_d16_r(const u16 address, const Register& reg)
{

	switch (_currentCycle)
	{
	case 1:
		// read Instruction
		break;

	case 2:
		_memory.write(address, reg);
		break;
	}

	_cycles = 4;
	_currentCycle = _currentCycle == 2 ? 1 : _currentCycle + 1;
}

void CPU::ld_r_d8(Register& reg, const u16 address, const bool isPC)
{

	switch (_currentCycle)
	{
	case 1:
		// read Instruction
		break;

	case 2:
		reg = _memory.read(address, isPC);
		if (isPC) ++_PC;
		break;
	}

	_cycles = 4;
	_currentCycle = _currentCycle == 2 ? 1 : _currentCycle + 1;
}

void CPU::jr_cc(const bool condition)
{

	switch (_currentCycle)
	{
	case 1:
		// read Instruction
		break;

	case 2:
	{
		_sContainer = (char)_memory.read(_PC, true);

		if (!condition)
		{
			++_PC;
			_currentCycle = 3;
		}

		break;
	}
	case 3:
		_PC += _sContainer + 1;
		break;
	}

	_cycles = 4;
	_currentCycle = _currentCycle == 3 ? 1 : _currentCycle + 1;
}

void CPU::ret_cc(const bool condition)
{

	switch (_currentCycle)
	{
	case 1:
		// read Instruction
		break;

	case 2:
		if (!condition)
			_currentCycle = 5;
		break;

	case 3:
		_container = popStackLow();
		break;

	case 4:
		_container |= popStackHigh() << 8;
		break;

	case 5:
		_PC = _container;
		break;
	}

	_cycles = 4;
	_currentCycle = _currentCycle == 5 ? 1 : _currentCycle + 1;
}

void CPU::jp_cc(const bool condition)
{

	switch (_currentCycle)
	{
	case 1:
		// read Instruction
		_currentCycle = 2;
		break;
	
	case 2:
		_container = _memory.read(_PC, true);
		_currentCycle = 3;
		break;

	case 3:
		_container = join(_memory.read(_PC + 1, true), _container);
		_currentCycle = 4;

		if (!condition) 
		{
			_PC += 2;
			_currentCycle = 1;
		}
		break;

	case 4:
		_PC = _container;
		_currentCycle = 1;
		break;
	}

	_cycles = 4;
}

void CPU::call_cc(const bool condition)
{

	switch (_currentCycle)
	{
	case 1:
		// read Instruction
		break;

	case 2:
		_container = _memory.read(_PC, true);
		break;

	case 3:
		_container = join(_memory.read(_PC + 1, true), _container);
	
		if (!condition)
		{
			_PC += 2;
			_currentCycle = 6;
		}
		break;

	case 4:
		_memory.doOAMBug(_SP, OAM_BUG_TYPE::WRITE);
		break;

	case 5:
		_memory.write(--_SP, (_PC + 2) >> 8);
		break;

	case 6:
		_memory.write(--_SP, _PC + 2);
		_PC = _container;
		break;
	}

	_cycles = 4;
	_currentCycle = _currentCycle == 6 ? 1 : _currentCycle + 1;
}

/*
	-- ld _A address to HL --
	* input: is increase or decrease
*/
void CPU::ld_a_hl(const bool isInc)
{
	switch (_currentCycle)
	{
	case 1:
		// read Instruction
		break;

	case 2:
		_A = _memory.read(_H.join(_L), true);
		isInc ? inc16(_H, _L) : dec16(_H, _L);
		break;	
	}

	_cycles = 4;
	_currentCycle = _currentCycle == 2 ? 1 : _currentCycle + 1;
}

/* 
	-- ld HL to _A address -- 
	* input: is increase or decrease	
*/
void CPU::ld_hl_a(const bool isInc)
{
	switch (_currentCycle)
	{
	case 1:
		// read Instruction
		break;

	case 2:
		_memory.write(_H.join(_L), _A);
		isInc ? _H.add16(_L) : _H.subtract16(_L);
		break;
	}

	_cycles = 4;
	_currentCycle = _currentCycle == 2 ? 1 : _currentCycle + 1;
}


void CPU::inc_dec_rr(Register& high, Register& low, const bool isInc)
{

	switch (_currentCycle)
	{
	case 1:
		// read Instruction
		break;

	case 2:
		isInc ? inc16(high, low) : dec16(high, low);
		break;
	}

	_cycles = 4;
	_currentCycle = _currentCycle == 2 ? 1 : _currentCycle + 1;

}

void CPU::inc_dec_rr(u16& registers, const bool isInc)
{
	switch (_currentCycle)
	{
	case 1:
		// read Instruction
		break;

	case 2:
		isInc ? inc16(registers) : dec16(registers);
		break;
	}

	_cycles = 4;
	_currentCycle = _currentCycle == 2 ? 1 : _currentCycle + 1;
}


void CPU::pop(Register& high, Register& low)
{

	switch (_currentCycle)
	{
	case 1:
		// read Instruction
		break;

	case 2:
		low = popStackLow();
		break;
	
	case 3:
		high = popStackHigh();
		break;
	}

	_cycles = 4;
	_currentCycle = _currentCycle == 3 ? 1 : _currentCycle + 1;
}

void CPU::push(const u16 value)
{
	_cycles = 4;

	switch (_currentCycle)
	{
	case 1:
		// read Instruction
		break;

	case 2:
		_memory.doOAMBug(_SP, OAM_BUG_TYPE::WRITE);
		break;
	
	case 3:
		_memory.write(--_SP, value >> 8);
		break;
	
	case 4:
		_memory.write(--_SP, value);
		break;
	}

	_currentCycle = _currentCycle == 4 ? 1 : _currentCycle + 1;
}

void CPU::rst(const u16 newPC)
{
	_cycles = 4;

	switch (_currentCycle)
	{
	case 1:
		// read Instruction
		break;

	case 2:
		_memory.doOAMBug(_SP, OAM_BUG_TYPE::WRITE);
		break;
	case 3:
		_memory.write(--_SP, _PC >> 8);
		break;
	
	case 4:
		_memory.write(--_SP, _PC);
		_PC = newPC;
		break;
	}

	_currentCycle = _currentCycle == 4 ? 1 : _currentCycle + 1;
}

void CPU::arithmic_a(void(CPU::* method)(const u8), const u16 address, const bool isPC)
{

	switch (_currentCycle)
	{
	case 1:
		// read Instruction
		break;

	case 2:
		(this->*method)(_memory.read(address, isPC));
		if (isPC) ++_PC;
		break;
	}

	_cycles = 4;
	_currentCycle = _currentCycle == 2 ? 1 : _currentCycle + 1;
}

std::ostream& operator<<(std::ostream& os, const CPU& CPU)
{
	os.put(CPU._sContainer);
	os.put(CPU._cycles);
	os.put(CPU._currentCycle);
	os.put(CPU._instruction);
	os.put(CPU._nextPrefix);
	os.put(CPU._A);
	os.put(CPU._F);
	os.put(CPU._D);
	os.put(CPU._E);
	os.put(CPU._H);
	os.put(CPU._L);

	return os << CPU._interrupts
			  << CPU._halted << '\n'
			  << CPU._SP << '\n'
			  << CPU._PC << '\n'
			  << CPU._container << '\n'
			  << CPU._enableIME << '\n'
			  << CPU._doHaltBug;
}

std::istream& operator>>(std::istream& is, CPU& CPU)
{
	CPU._sContainer = is.get();
	CPU._cycles = is.get();
	CPU._currentCycle = is.get();
	CPU._instruction = is.get();
	CPU._nextPrefix = is.get();
	CPU._A = is.get();
	CPU._F = is.get();
	CPU._D = is.get();
	CPU._E = is.get();
	CPU._H = is.get();
	CPU._L = is.get();

	return is >> CPU._interrupts
			  >> CPU._halted
			  >> CPU._SP
			  >> CPU._PC
			  >> CPU._container
			  >> CPU._enableIME
			  >> CPU._doHaltBug;
}
