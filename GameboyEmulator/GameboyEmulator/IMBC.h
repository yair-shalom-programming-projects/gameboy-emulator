#pragma once
#include "Utils.h"
#include "Cartridge.h"
#include <memory>

class Cartridge;

class IMBC
{

public:

	// Constructors
	IMBC();
	virtual ~IMBC() noexcept = default;

	// Methods
	virtual bool write(const u16 address, const u8 data) = 0;
	virtual bool read(const u16 address, u8& data) const = 0;

	// operator overload
	friend std::ostream& operator<<(std::ostream& os, const std::unique_ptr<IMBC>& MBC);
	friend std::istream& operator>>(std::istream& is, std::unique_ptr<IMBC>& MBC);

	virtual std::istream& setState(std::istream& is) = 0;
	virtual std::ostream& getState(std::ostream& os) const = 0;

	virtual void reset() = 0;

protected:

	// Fields
	Cartridge& _cartridge;

};
