#include "MBC1.h"
#include "Address.h"

using namespace Address::Memory;
using namespace Utils;


//? Constructors ?//

/* -- C'tor -- */
MBC1::MBC1() :
	IMBC(),
	_ROMBanksCount(0),
	_RAMBanksCount(0)
{
	reset();
}


//? Methods ?//

/* -- handle writing to specific addresses in MBC1 -- */
bool MBC1::write(const u16 address, const u8 data) 
{

	// program writes to ROM banks memory addresses to modify ROM/RAM bank
	if (address >= ROM_BANK_0_START && address <= ROM_BANK_N_END)
	{
		handleBanking(address, data);
	}

	// writing to RAM bank (if RAM is enabled) 
	else if (address >= RAM_BANK_N_START && address <= RAM_BANK_N_END)
	{
		if (_RAMEnabled)
		{
			_RAMBanks[getRAMBank()][address - RAM_BANK_N_START] = data;
		}
	}

	else
	{
		return false;
	}

	return true;

}

/* -- handle reading to specific addresses in MBC1 -- */
bool MBC1::read(const u16 address, u8& data) const
{
	
	// reading from ROM bank
	if (address >= ROM_BANK_0_START && address <= ROM_BANK_N_END)
	{
		u32 newAddress = ROM_BANK_0_START + (address % ROM_BANK_SIZE) + getROMBank(address) * ROM_BANK_SIZE;
		data = _cartridge[newAddress];
	}

	// reading from RAM bank. if RAM disabled, return 0xFF
	else if (address >= RAM_BANK_N_START && address <= RAM_BANK_N_END)
	{
		data = _RAMEnabled ? _RAMBanks[getRAMBank()][address - RAM_BANK_N_START] : 0xFF;
	}

	else
	{
		return false;
	}

	return true;

}

std::ostream& MBC1::getState(std::ostream& os) const 
{
	os << _ROMBanksCount << '\n'
	   << _RAMBanksCount;

	for (const auto& RAMbank : _RAMBanks)
		for (const auto& byte : RAMbank)
			os.put(byte);
	
	os.put(static_cast<u8>(_mode));
	os.put(_bank1);
	os.put(_bank2);

	return os << _RAMEnabled;
}

std::istream& MBC1::setState(std::istream& is)
{
	
	is >> const_cast<u16&>(_ROMBanksCount) 
	   >> const_cast<u16&>(_RAMBanksCount);

	for (auto& RAMbank : _RAMBanks)
		for (auto& byte : RAMbank)
			byte = is.get();

	_mode = static_cast<Mode>(is.get());
	_bank1 = is.get();
	_bank2 = is.get();

	return is >> _RAMEnabled;
}



//? Protected Methods ?//

/*
	--
	handle banking when writing to ROM.
	writing to 0x0000-0x2000 to enable/disable RAM (when lower nibble is 0b1010, RAM enabled. else disabled).
	writing to 0x2000-0x4000 to modify _bank1 (bits 0-4 of data).
	writing to 0x4000-0x6000 to modify _bank2 (bits 0-1 of data).
	writing to 0x6000-0x8000 to set _mode (bit 0 of data).
	--
*/
void MBC1::handleBanking(const u16 address, const u8 data) noexcept
{

	if (address < 0x2000)
	{
		setRAMEnabled(data);
	}

	else if (address >= 0x2000 && address < 0x4000)
	{
		setBank1(data);
	}

	else if (address >= 0x4000 && address < 0x6000)
	{
		setBank2(data);
	}

	else if (address >= 0x6000 && address < 0x8000)
	{
		_mode = static_cast<Mode>(isBitOn(data, 0));
	}


}

/* -- if the lower nibble in data is 0b1010, _RAMEnabled is true. else false -- */
void MBC1::setRAMEnabled(const u8 data) noexcept
{
	_RAMEnabled = (data & 0b1111) == 0b1010;
}


/* -- set _bank 1 (0-4 bits of data) -- */
void MBC1::setBank1(const u8 data) noexcept
{
	
	_bank1 = data & 0b11111;
	
	// _bank1 can't contain 0
	if (!_bank1) ++_bank1;
	
}

/* -- set _bank2 (bits 0-1 of data) -- */
void MBC1::setBank2(const u8 data) noexcept
{
	_bank2 = data & 0b11;
}

/*
	-- get ROM bank depending on the given address and on the current _mode --
	ROM Bank 0 addresses - in Mode::ROM, 0. in Mode::RAM depends on _bank2
	ROM Bank n addresses - ROM bank is the combination of _bank2 and _bank1 
*/
u8 MBC1::getROMBank(const u16 address) const noexcept
{
	u8 bank = 0;

	// ROM Bank 0 address
	if (address >= ROM_BANK_0_START && address <= ROM_BANK_0_END)
	{
		bank = isMode(Mode::ROM) ? 0 : _bank2 << 5;
	}

	// ROM Bank n address
	else
	{
		bank = (_bank2 << 5) | _bank1;
	}

	// make sure bank is not bigger than ROM banks count
	return bank & (_ROMBanksCount - 1);
}


/* 
	-- get RAM bank depending on _mode --
	if Mode::RAM, RAM bank is _bank2. else 0
*/
u8 MBC1::getRAMBank() const noexcept
{
	const u8 bank = isMode(Mode::RAM) ? _bank2 : 0;
	
	// make sure bank is not bigger than RAM banks count
	return bank & (_RAMBanksCount - 1);
}

void MBC1::reset()
{
	const_cast<u16&>(_ROMBanksCount) = _cartridge.getROMBanksCount();
	const_cast<u16&>(_RAMBanksCount) = _cartridge.getRAMBanksCount();

	for (auto& RAMBank : _RAMBanks)
		RAMBank.fill(0);

	_mode = Mode::ROM;

	_bank1 = 1; // cannot contain 0
	_bank2 = 0;

	_RAMEnabled = false;
}
