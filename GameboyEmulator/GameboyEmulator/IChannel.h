#pragma once
#include "LengthCounter.h"
#include <string>
#include <memory>
#include <iostream>
#include "Memory.h"

class Memory;

// Every channel has his NRx0-NRx4 registers which he uses
// not all channels use all registers 
// '-' is used to mark empty value 
// for example NRx0 of Square 1 is NR10 (0xFF10) and for Square 2 its NR20 (0xFF15)
/*
        Square 1
NR10 FF10 -PPP NSSS Sweep period, negate, shift
NR11 FF11 DDLL LLLL Duty, Length load (64-L)
NR12 FF12 VVVV APPP Starting volume, Envelope add mode, period
NR13 FF13 FFFF FFFF Frequency LSB
NR14 FF14 TL-- -FFF Trigger, Length enable, Frequency MSB

       Square 2
NR20 FF15 ---- ---- Not used
NR21 FF16 DDLL LLLL Duty, Length load (64-L)
NR22 FF17 VVVV APPP Starting volume, Envelope add mode, period
NR23 FF18 FFFF FFFF Frequency LSB
NR24 FF19 TL-- -FFF Trigger, Length enable, Frequency MSB

       Wave
NR30 FF1A E--- ---- DAC power
NR31 FF1B LLLL LLLL Length load (256-L)
NR32 FF1C -VV- ---- Volume code (00=0%, 01=100%, 10=50%, 11=25%)
NR33 FF1D FFFF FFFF Frequency LSB
NR34 FF1E TL-- -FFF Trigger, Length enable, Frequency MSB

       Noise
     FF1F ---- ---- Not used
NR41 FF20 --LL LLLL Length load (64-L)
NR42 FF21 VVVV APPP Starting volume, Envelope add mode, period
NR43 FF22 SSSS WDDD Clock shift, Width mode of LFSR, Divisor code
NR44 FF23 TL-- ---- Trigger, Length enable

*/
class IChannel
{
public:

    explicit IChannel(Memory& memory, u16 NRx0, u16 NRx1, u16 NRx2, u16 NRx3, u16 NRx4, const LengthCounter& lengthCounter);
    virtual ~IChannel() = default;

    virtual float getSample() = 0;
    virtual void reset();
	virtual void clockLengthCounter();
    virtual void checkObscureLengthClock(const u8 value, const u8 frameSequencer);
    virtual void setEnabledFlagLengthCounter();
    virtual bool isEnabledLengthCounter() const { return _lengthCounter.isEnabled(); };
    virtual void triggerEvent();
    virtual bool isRunning() const { return _isRunning; }
    virtual void start() { _isRunning = true; }
    virtual void stop() { _isRunning = false;  _isDacOn = false;}
    virtual void startDac() { _isDacOn = true; }
    virtual void clock(int cycles) = 0;
    virtual void setLengthCounter() = 0;
    virtual void setDacPower(const int value) = 0;

    virtual std::istream& setState(std::istream& is);
    virtual std::ostream& getState(std::ostream& os) const;

    friend std::istream& operator>>(std::istream& is, std::shared_ptr<IChannel>& channel);
    friend std::ostream& operator<<(std::ostream& os, const std::shared_ptr<IChannel>& channel);

protected:
    virtual void setEnabled() = 0;
    virtual u16 getFrequency();
    
	
    Memory& _memory;
    
    LengthCounter _lengthCounter; 
    
    // addresses of the registers
    u16 _NRx0;
    u16 _NRx1;
    u16 _NRx2;
    u16 _NRx3;
    u16 _NRx4;

    u16 _cyclesCount = 0;
    u16 _cyclesSampleUpdate = 0;

    bool _isRunning = false;
    bool _isDacOn = false;
};

