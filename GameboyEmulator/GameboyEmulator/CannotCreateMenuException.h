#pragma once
#include <exception>
#include <string>
#include <minwindef.h>

class CannotCreateMenuException : public std::exception
{
public:
	CannotCreateMenuException(const DWORD& e) : 
		std::exception((std::string("cannot create menu: ") + std::to_string(e)).c_str()) {};

	CannotCreateMenuException(const std::string& e) :
		std::exception(("cannot create menu: " + e).c_str()) {};
};