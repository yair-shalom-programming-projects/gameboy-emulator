#pragma once
#include <cstdint>

namespace DAC
{
	float convert(const float input) 
	{
		return ((input / 7) - 1) * 0x7FFF;
	}
};

