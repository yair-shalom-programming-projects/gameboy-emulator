#pragma once
#include "GPU.h"
#include "Timers.h"
#include "Joypad.h"
#include "APU.h"
#include <iostream>
#include <string>

class Gameboy final : public Singleton<Gameboy>
{

public:
	
	// Constructors
	explicit Gameboy(Singleton_t);

	// Methods
	void emulate();
	void reset();
	void loadGame(const std::string_view filePath);

	// operator overloads
	friend std::ostream& operator<<(std::ostream& os, const Gameboy& gameboy);
	friend std::istream& operator>>(std::istream& is, Gameboy& gameboy);
	

private:

	// Methods
	void update();
	std::string getGameFileFromUser();

	// Fields
	Memory& _memory;
	CPU& _cpu;
	GPU& _gpu;
	Timers& _timers;
	Joypad& _joypad;
	APU& _apu;

};

