#pragma once
#include "Constants.h"


class LengthCounter final
{
public:
	explicit LengthCounter(const u16 length, const bool isEnabled);

	void setLengthCounter(const u16 length, const bool isEnabled);
	void setEnabled(bool isEnabled);
	bool clock();
	bool isEnabled() const { return _isEnabled; };
	void triggerEvent(const u16 length, const u8 frameSequencer);
	bool checkObscureLengthClock(const u8 value, const u8 frameSequencer);

	friend std::istream& operator>>(std::istream& is, LengthCounter& lengthCounter);
	friend std::ostream& operator<<(std::ostream& os, const LengthCounter& lengthCounter);

private:
	u16 _lengthTimer;
	bool _isEnabled;
	
	void decrementTimer();
	bool isNextFrameClockLengthClock(const u8 frameSequencer);

};

