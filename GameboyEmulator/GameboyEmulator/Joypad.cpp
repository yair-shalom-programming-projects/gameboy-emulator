#include "Joypad.h"
#include "Constants.h"
#include "GameOverException.h"
#include <stdexcept>

using namespace Utils;
using namespace Const::Joypad;
using Address::JOYPAD;

using SDL::joypadEvents;

/* -- c'tor -- */
Joypad::Joypad(Singleton_t) :
    _memory(Memory::getInstance()),
    _interrupts(Interrupts::getInstance())
{
    reset();
}


/* -- handle all pending inputs -- */
void Joypad::handleInput()
{
    SDL::updateEventQueues();

    while (joypadEvents.size())
    {
        if (auto keycode = joypadEvents.front().key.keysym.sym; KEYS.find(keycode) != KEYS.end())
        {
            Key key = KEYS.at(keycode);

            isPendingEventKeyPressed() ? keyPressed(key) : keyReleased(key);
        }

        joypadEvents.pop();
    }
}

Joypad::operator u8() const noexcept
{
    return getCurrentState();
}



/*
    -- 
    get the desired joypad state (buttons or directions)
    found in JOYPAD register address in bits DIRECTION_KEY, BUTTON_KEY
    note - bits 6-7 unused, always on
    --
    * output: desired Joypad state 
*/
u8 Joypad::getCurrentState() const noexcept
{

    const u8 currentJoypad = (_memory[JOYPAD] & 0xF0);

    // handle direction keys request (0=requested)
    if (isDirectionsRequested())
    {   
        return currentJoypad | (_joypad & 0x0F);
    }

    // handle buttons keys request (0=requested)
    else if (isButtonsRequested())
    {
        return currentJoypad | ((_joypad & 0xF0) >> 4);
    }

    else
    {
        return _memory[JOYPAD];
    }

}

void Joypad::reset()
{
    _joypad = 0xFF;
}


/*
    --
    press the given key by reseting his _joypad bit
    and request interrupt if the bit was set and it is in the requested keys
    --
    * input: key pressed
*/
void Joypad::keyPressed(const Key key)
{
    
    const bool wasSet = isBitOn(_joypad, key.id);

    _joypad = resetBit(_joypad, key.id);

    const bool isRequested = (key.type == Type::BUTTON && isButtonsRequested()) ||
                             (key.type == Type::DIRECTION && isDirectionsRequested());
    
    // request interrupt if the bit was set and requested 
    if (wasSet && isRequested)
        _interrupts.request(Interrupt::JOYPAD);

}

/*
    -- release the given key by setting his _joypad bit --
    * input: key released
*/
void Joypad::keyReleased(const Key key) noexcept
{
    _joypad = setBit(_joypad, key.id);
}

/*
    -- check if the pending joypad event is key pressed type -- 
    * output: bool: is key pressed or released
*/
bool Joypad::isPendingEventKeyPressed() const
{
    return joypadEvents.front().type == SDL_KEYDOWN;
}

/* -- checks if game requested buttons pressed (0=requested) -- */
bool Joypad::isButtonsRequested() const noexcept
{
    return !isBitOn(_memory[JOYPAD], Bit::BUTTON_KEY);
}

/* -- checks if game requested directions pressed (0=requested) -- */
bool Joypad::isDirectionsRequested() const noexcept
{
    return !isBitOn(_memory[JOYPAD], Bit::DIRECTION_KEY);
}

std::ostream& operator<<(std::ostream& os, const Joypad& joypad)
{
    return os.put(joypad._joypad);
}

std::istream& operator>>(std::istream& is, Joypad& joypad)
{
    joypad._joypad = is.get();
    return is;
}
