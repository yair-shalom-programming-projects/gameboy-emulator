#pragma once

#include <exception>

class GameOverException : public std::exception
{
public:
	GameOverException() noexcept : std::exception("game over") {};
};
