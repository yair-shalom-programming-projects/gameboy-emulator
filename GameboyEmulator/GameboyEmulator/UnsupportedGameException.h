#pragma once

#include <exception>

class UnsupportedGameException : public std::exception
{
public:
	UnsupportedGameException() : std::exception("game's type (MBC) is not supported") {};
};
