#include "MBC5.h"
#include "Address.h"

using namespace Address::Memory;
using namespace Utils;

/* -- c'tor -- */
MBC5::MBC5() :
	MBC1()
{
	reset();
}

bool MBC5::write(const u16 address, const u8 data)
{

	// program writes to ROM banks memory addresses to modify ROM/RAM bank
	if (address >= ROM_BANK_0_START && address <= 0x5FFF)
	{
		handleBanking(address, data);
	}

	// writing to RAM bank (if RAM is enabled) 
	else if (address >= RAM_BANK_N_START && address <= RAM_BANK_N_END)
	{
		if (_RAMEnabled)
		{
			_RAMBanks[_RAMBank][address - RAM_BANK_N_START] = data;
		}
	}

	else
	{
		return false;
	}

	return true;
}


bool MBC5::read(const u16 address, u8& data) const
{

	// reading from ROM bank 0
	if (address >= ROM_BANK_0_START && address <= ROM_BANK_0_END)
	{
		data = _cartridge[address];
	}

	// reading from ROM bank n
	else if (address >= ROM_BANK_N_START && address <= ROM_BANK_N_END)
	{
		u32 newAddress = _ROMBank.val * ROM_BANK_SIZE + (address % ROM_BANK_SIZE);
		data = _cartridge[newAddress];
	}

	// reading from RAM bank. if RAM disabled, return 0xFF
	else if (address >= RAM_BANK_N_START && address <= RAM_BANK_N_END)
	{
		data = _RAMEnabled ? _RAMBanks[_RAMBank][address - RAM_BANK_N_START] : 0xFF;
	}

	else
	{
		return false;
	}

	return true;
}

std::ostream& MBC5::getState(std::ostream& os) const
{
	os.put(_RAMBank);
	return this->MBC1::getState(os) << '\n' << _ROMBank.val;
}

std::istream& MBC5::setState(std::istream& is)
{
	_RAMBank = is.get();
	return this->MBC1::setState(is) >> _ROMBank.val;
}


void MBC5::handleBanking(const u16 address, const u8 data) noexcept
{

	if (address < 0x2000)
	{
		_RAMEnabled = data == 0b00001010;
	}
	else if (address < 0x3000)
	{
		_ROMBank.low = data;
		_ROMBank.val &= _ROMBanksCount - 1;
	}
	else if (address < 0x4000)
	{
		_ROMBank.high = data & 1;
		_ROMBank.val &= _ROMBanksCount - 1;
	}
	else if (address < 0x6000)
	{
		_RAMBank = data & 0xF;
		_RAMBank &= _RAMBanksCount - 1;
	}

}

void MBC5::reset()
{
	this->MBC1::reset();

	for (auto& RAMBank : _RAMBanks)
		RAMBank.fill(0);

	_RAMBank = 0;
	_ROMBank.val = 1;
}
