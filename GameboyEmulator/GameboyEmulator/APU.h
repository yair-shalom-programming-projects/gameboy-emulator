#pragma once
#include "Constants.h"
#include "WaveChannel.h"
#include "NoiseChannel.h"
#include "SquareChannel1.h"
#include "SquareChannel2.h"
#include <SDL_audio.h>

class Memory;

class APU final: public Singleton<APU>
{
public:
    explicit APU(Singleton_t);
    
    
    void update(int cycles);
    void reset();
    bool write(const u16 address, u8 value);
    bool read(const u16 address, u8& data);
    void checkDivClockFs(const u8 oldDiv, const u8 newDiv);

    friend std::ostream& operator<<(std::ostream& os, const APU& APU);
    friend std::istream& operator>>(std::istream& is, APU& APU);


private:
    void resetDuties();
    void resetChannels();
    void cycleFrameSequencer(int cycles);
    void clockFrameSequencer();
    void clockLength();
    void clockEnvelope();
    void clockSweep();
    void cycleChannels(int cycles);
    void cycleSamples(int cycles);
    bool isAddressDivWrite(const u16 address);
    bool isAddressAPU(const u16 address);
    bool isAddressLengthCounter(const u16 address);
    bool isAddressLengthCounterSquare(const u16 address);
    bool isAddressWaveRam(const u16 address);

    SDL_AudioDeviceID audioDevice;
    float _audioBuffer[Const::APU::AUDIO_NUM_SAMPLES * 2];
    SquareChannel1 _squareChannel1;
    SquareChannel2 _squareChannel2;
    WaveChannel _waveChannel;
    NoiseChannel _noiseChannel;
    Memory& _memory;
    u32 _cyclesCount = 0;
    u32 _cyclesFrameSequencerCount = 0;
    u16 _sampleCounter = 0;
    u8 _frameSequencerCounter = 0;
    u32 _sampleIndex = 0;
    u8 _volumeLeft = 1;
    u8 _volumeRight = 1;
    bool channelsOutput[8];
    bool _isEnabled = true;
};