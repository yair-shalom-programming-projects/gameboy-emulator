# Gameboy Emulator

Final Project For Magshimim 2022. Written Fully In C++20 Using SDL & WINAPI Libraries.


## Features

- Cycle-accurate (mostly), passes ALL DMG Blargg's tests.
- Written fully in modern C++20.
- Supports MBC1, MBC2 & MBC5.
- Passes most Mooneye's tests.
